<?php

namespace App\Http\Middleware;
use Illuminate\Support\Facades\Auth;

use Closure;

class Checktypeadmin
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
      $session = session('adminlogin');
       if($request->session()->has('adminlogin')){
           if($session->is_admin == 'admin'){
             return $next($request);
           }
       }
        return redirect('admin/products')->with('msg','คุณไม่สิทธิ์เข้าใช้งาน');
    }
}