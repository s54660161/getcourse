<?php

namespace App\Http\Middleware;
use Illuminate\Support\Facades\Auth;

use Closure;

class Checkadmin
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
      $session = session('adminlogin');
    // dd($session);
       if($request->session()->has('adminlogin')){
           if($session->is_admin == 'admin' || $session->is_admin == 'partner'){
             return $next($request);
           }
       }
        return redirect('/admin');
    }
}