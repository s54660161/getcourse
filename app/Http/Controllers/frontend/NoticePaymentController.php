<?php

namespace App\Http\Controllers\frontend;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\NoticePaymentModel;
use App\BankModel;
use App\OrderListModel;
use App\User;
use Auth;


use App\Http\Controllers\CenterController as CenterController;

class NoticePaymentController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    function index($order_id){
      $userid = Auth::id();

      $data['account_pay'] = BankModel::all();
      $data['order_id'] = $order_id;
       
      if($userid != null){
         $data['member'] = User::find($userid);
      }
      return view('/frontend/confirmpayment',$data);
    }


    function createNewNotice(Request $request){
      $db = new NoticePaymentModel;
      $centerControll = new CenterController;

      $image = $request->file('chooseFile');
      $name =  $centerControll->generateRandomString().'.'.$image->getClientOriginalExtension();
      $destinationPath = 'upload/slip';
      $getDir = $centerControll->createDirectory($destinationPath);
      $image->move(public_path($getDir),$name);

      //dd(strtoupper($request->order_id));die;
      $db->bank_name = $request->bank_name;
      $db->order_id = strtoupper($request->order_id);
      $db->payment_datetime = $request->date_pay." ".$request->time_pay;
      $db->amount = $request->amount;
      $db->picture_slip = $getDir.$name;
      $db->description = $request->description;
      $db->member_name = $request->member_name;
      $db->email = $request->email;
      $db->tel = $request->tel;
      $db->save();

      OrderListModel::where('order_id', $request->order_id)->update(['order_status' => "pending"],['payment_method'=>'Payment by bank account']);

      return redirect('/myorder')->with('msg','ได้รับแจ้งยืนยันการชำระเงินเรียบร้อย รอเจ้าหน้าที่ติดต่อกลับภายใน 24 ชม.');
    }
 

}