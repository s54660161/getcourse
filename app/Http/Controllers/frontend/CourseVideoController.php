<?php

namespace App\Http\Controllers\frontend;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Products;
use App\Category;
use App\Teacher;
use App\Section;
use App\Lecture;
class CourseVideoController extends Controller
{
    // public function __construct()
    // {
    //     $this->middleware('auth');
    // }
    function seeVideo($product_id,$id=null){

        if($id == ""){
            $section_first = Section::where('sec_product_id',$product_id)->orderBy('sec_sequence', 'asc')->first();
            $data['lecture_find'] = Lecture::where('lecture_section_id',$section_first->id)->orderBy('lecture_sequence', 'asc')->first();
          
        }else{
            $data['lecture_find'] = Lecture::find($id);
        }

        $data['sections'] = Section::where('sec_product_id',$product_id)->orderBy('sec_sequence', 'asc')->get();

        $product = new Products;
        $data['product']= $product->ProductsById($product_id);
     
        return view('frontend.course_video.show_video',$data);
    }
}