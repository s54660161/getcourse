<?php

namespace App\Http\Controllers\frontend;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Products;
use App\OrderListModel;
use App\User;
use App\Teacher;
use App\MemberCourse;
use Illuminate\Support\Facades\Hash;

use App\Http\Controllers\frontend\AddCourseController as AddCourseController;
use App\Http\Controllers\MailController as MailController;

class CallBackApiController extends Controller
{

    
    function callbackApiPayment(Request $request){
        $AddCourseController = new AddCourseController;
        if(($request->deal['people']['email']) != ""){

            $user = User::where('email',$request->deal['people']['email'])->first();
            if($user){
                    $product = Products::where('tc_woo_product_id',$request->product['woo_id'])->first();
                    if($product){
                        $MemberCourse = MemberCourse::where([['member_id',$user->id],['product_id',$product->id]])->first();
                        if($MemberCourse){
                            $MemberCourse->status_buy_course = 1;  
                            $MemberCourse->save();
            
                            $orderlist = OrderListModel::where('id',$MemberCourse->order_id)->first();
                            $orderlist->order_status = "success";  
                            $orderlist->payment_method = "Payment by Payment Gateway";  
                            $orderlist->save();
                          // dd($MemberCourse->id);
                            //แจ้งยืนยันการชำเงินเสร็จสิ้น
                            $this->sendMailSuccessBuyCourse($user->id,$orderlist->id);
                            //แจ้งเตือน LineNotify ไปยังแอดมิน
                            $this->LineNotify($orderlist->order_id,$request->deal['people']['first_name'],$request->deal['people']['email']);
                            $response= array(
                                'status' => 'success',
                            );
                            
                            return \Response::json($response);
                        }else{
                            
                            $create_order_success = $AddCourseController->addToOrder($product,$user->id,$status_order="success");
                            
                            //แจ้งยืนยันการชำเงินเสร็จสิ้น
                            $this->sendMailSuccessBuyCourse($user->id,$create_order_success);

                            $orderlist = OrderListModel::find($create_order_success);
                             //แจ้งเตือน LineNotify ไปยังแอดมิน
                            $this->LineNotify($orderlist->order_id,$request->deal['people']['first_name'],$request->deal['people']['email']);
                            $response= array(
                                'status' => 'success',
                            );
                            return \Response::json($response);
                        }//MemberCourse
                        
                    }else {
                        return  abort(404);
                    }//product
            }else{

                $product = Products::where('tc_woo_product_id',$request->product['woo_id'])->first();
                if($product){
                    // เพิ่มผู้ใช้งานใหม่
                   $userid =  $this->createNewuser($request);
                   $create_order_success = $AddCourseController->addToOrder($product,$userid,$status_order="success");
                            
                   //แจ้งยืนยันการชำเงินเสร็จสิ้น
                   $this->sendMailSuccessBuyCourse($userid,$create_order_success);
                   $orderlist = OrderListModel::find($create_order_success);
                    //แจ้งเตือน LineNotify ไปยังแอดมิน
                    $this->LineNotify($orderlist->order_id,$request->deal['people']['first_name'],$request->deal['people']['email']);
                   // dd($create_order_success);
                   $response= array(
                    'status' => 'success create usernname and create order',
                    );
                
                    return \Response::json($response);
                }else{
                    return  abort(404);
                }

            }//user
        }else{
            return  abort(404);
        }//email
      }

      function LineNotify($orderid,$name,$email){
        $token = "4YMaugPvR0GZ0kH9wx4G55J0OpY4u2jG9Jzl3R7Xwpm"; 
        $message ="\nรหัสการสั่งซื้อ : ".$orderid."\nชื่อ  : ".$name."\nอีเมล์ : ".$email."\nชำระเงินผ่าน :" .'Payment by Payment Gateway'. "";
    
        $chOne = curl_init(); 
        curl_setopt( $chOne, CURLOPT_URL, "https://notify-api.line.me/api/notify"); 
        curl_setopt( $chOne, CURLOPT_SSL_VERIFYHOST, 0); 
        curl_setopt( $chOne, CURLOPT_SSL_VERIFYPEER, 0); 
        curl_setopt( $chOne, CURLOPT_POST, 1); 

        curl_setopt( $chOne, CURLOPT_POSTFIELDS, "message=$message"); 	

        curl_setopt( $chOne, CURLOPT_FOLLOWLOCATION, 1); 
        $headers = array( 'Content-type: application/x-www-form-urlencoded', 'Authorization: Bearer '.$token.'', );
        curl_setopt($chOne, CURLOPT_HTTPHEADER, $headers); 
        curl_setopt( $chOne, CURLOPT_RETURNTRANSFER, 1); 
        $result = curl_exec( $chOne ); 
        //   if(curl_error($chOne)) { echo 'error:' . curl_error($chOne); } 
        //   else { $result_ = json_decode($result, true); 
        //   echo "status : ".$result_['status']; echo "message : ". $result_['message']; 
        //   } 
        curl_close( $chOne );  
      }
    function createNewuser($request){

        $password_create_rand = $request->product['woo_id'] + time();

         $db = User::create([
            'name' => $request->deal['people']['first_name'],
            'email' => $request->deal['people']['email'],
            'password' => Hash::make($password_create_rand),
        ]);

        $insert_id = $db->id;

        $data_mail = array(
            'username' => $request->deal['people']['email'],
            'password' => $password_create_rand,
            'name' => $request->deal['people']['first_name'],
        );
        
        $sendusernametomail = new MailController;
        $sendusernametomail->SendUserNameForNewUser($data_mail);
        
        return $insert_id;
    }

    function sendMailSuccessBuyCourse($userid,$order_id){
        $sendusernametomail = new MailController;
        $sendusernametomail->mailSuccessBuyCourse($userid,$order_id);
    }




    function guzzleGetCourses(){
        $count = new Products;
        $products = $count->Products();
        //dd('ddd');
        return json_encode($products);
    }
    
    function guzzleGetCoursesPromotion(){
        $count = new Products;
        $products = $count->ProductsPromotion();
       
        return json_encode($products);
    }
    function guzzleGetCoursesFree(){
        $count = new Products;
        $products = $count->ProductsFree();
        //dd('ddd');
        return json_encode($products);
    }
    
    function guzzleGetCoursesReccomment(){
        $count = new Products;
        $products = $count->ProductsReccomment();
        //dd('ddd');
        return json_encode($products);
    }
    
    function guzzleGetTeachers(){
        $teachers = Teacher::all();
        //dd('ddd');
        return json_encode($teachers);

    }
}