<?php

namespace App\Http\Controllers\frontend;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class AuthController extends Controller
{
    public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }

    function register(){
        return view('frontend.auth.register');
    }

    function login(){
        return view('frontend.auth.login');
    }


}