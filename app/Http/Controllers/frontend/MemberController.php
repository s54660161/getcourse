<?php

namespace App\Http\Controllers\frontend;

use Illuminate\Http\Request;
use Illuminate\Validation\Rule;

use App\Http\Controllers\Controller;
use Auth;
use App\MemberCourse;
use App\User;
use App\Products;
use App\OrderListModel;
use App\Teacher;
use App\BankModel;

class MemberController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    function profile(){
        $userid = Auth::id();
       
        if($userid != null){
           $data['member'] = User::find($userid);
        }

        return view('frontend.member.profile',$data);

    }

    function editProfile(Request $request){

        $this->validate($request, [
            'email' => [
            'required',
                  Rule::unique('users')->ignore(Auth::user()->id),
              ],
        ]);
        $userid = Auth::id();
       // Save profile member
        $member = User::find($userid);
        $member->name = $request->name;
        $member->email = $request->email;
        $member->tel = $request->tel;
        $member->save();
        return redirect(url()->previous())->with('msg','แก้ประวัติส่วนตัวเรียบร้อย');
    }

    function courseme(){
        $userid = Auth::id();
        $count = new Products;
        $data['countSection'] = $count->countSection();
        
        $data['orderlists'] = OrderListModel::where('member_id',$userid)->get();
        $MemberCourse = MemberCourse::where([['member_id',$userid],['status_buy_course','=', '1']])->get();
        $courseme = array();
        $data['teachers'] = Teacher::all();
            foreach($MemberCourse as $value){
               // $courseme[] = $count->ProductsById($value->product_id);
               $courseme[] = Products::find($value->product_id);
            }
       
        $data['course'] = array_filter($courseme);
       
       return view('frontend.member.coursemember',$data);

    }

    function myorder(){
        $userid = Auth::id();
        $data['account_pay'] = BankModel::all();
        $data['products'] = Products::all();
        $data['orderlists'] = OrderListModel::where('member_id',$userid)->get();
        return view('frontend.member.myorder',$data);
    }
}