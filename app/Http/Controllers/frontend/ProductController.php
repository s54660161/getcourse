<?php

namespace App\Http\Controllers\frontend;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Products;
use App\Category;
use App\Teacher;
use App\Section;
use App\Lecture;
use App\User;
use App\MemberCourse;

use Auth;


class ProductController extends Controller
{
    
    function detail(Request $request,$slug){
        //dd($userid );
        
        //  รายละเอียดคอร์ส
        $data['detail'] = Products::where('tc_slug_name','like','%'.$slug.'%')->firstOrFail();

        // คอร์สแนะนำ 5 คอร์ส
        $count = new Products;
        $data['products'] = $count->ProductsReccomment();
        
        // หมวดหมู่ และ ผู้สอนทั้งหมด
        $data['category'] = Category::all();
        $data['teachers'] = Teacher::all();

        // แปลง json หมวดหมู่ และ ผู้สอน จากรายบะเอียดคอร์ส
        $data['category_array'] = json_decode($data['detail']->tc_category);
        $data['teacher_array'] = json_decode($data['detail']->tc_nameteacher);

        
        $data['tags'] = unserialize($data['detail']->tc_tags);

        $data['video_preview'] = $this->iframeCheckTypePlatform($data['detail']->tc_video_platform);

        $data['sections'] = Section::where('sec_product_id',$data['detail']->id)->orderBy('sec_sequence', 'asc')->get();
        
        $count = 0;
        foreach($data['sections'] as $val ){
            $lecture_find = Lecture::where('lecture_section_id',$val->id)->count();
            $count = $lecture_find +$count;
        }
       $data['count_lecture'] = $count;

        $userid = Auth::id();
       
        if($userid != null){
           $data['member'] = User::find($userid);
        }
        
        return view('frontend.products.product_detail',$data);
      }


 

      function iframeCheckTypePlatform($Platform){

        if($Platform == 0){ 
            $data = 'https://player.vimeo.com/video/';
        }else{
            $data = 'https://www.youtube.com/embed/';
        }

        return $data;
      }
      

      function courseAll(){
        
        $count = new Products;
        $data['teachers'] = Teacher::all();
        //$data['products'] = Products::orderByRaw('id', 'DESC')->where('tc_status', 0)->paginate(30);
        $data['products'] = $count->Products();

        return view('frontend.products.products',$data);
      }
}