<?php

namespace App\Http\Controllers\frontend;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Auth;
use App\MemberCourse;

class CheckCourseController extends Controller
{
    function checkCourseMember(Request $request){
        //dd(Auth::id());
        $MemberCourse = new MemberCourse;
        $id = $request->data;
        $user = Auth::id();
        $getCheckCourseMember = $MemberCourse->checkCourseMember($user,$id);
       // dd($getCheckCourseMember);

        if($getCheckCourseMember == null){
            $response= array(
                'status' => 'failed',
              );
            return \Response::json($response);
        }else{
            if($getCheckCourseMember->status_buy_course == 1){
                $response= array(
                    'status' => 'success',
                );
                return \Response::json($response);
            }else{
                $response= array(
                    'status' => 'pending',
                );
                return \Response::json($response);
            }
        }
        
    }

    
}