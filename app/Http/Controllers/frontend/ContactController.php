<?php

namespace App\Http\Controllers\frontend;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use App\ContactModel;
use App\WebSetting;

class ContactController extends Controller
{
    function index(){
      $data['setting'] = WebSetting::get()->first();

      return view('frontend.contact',$data);
    }


    function messageFormto_admin(Request $request){
      
        $db = new ContactModel;
        $db->name = $request->name;
        $db->email = $request->email;
        $db->tel = $request->tel;
        $db->title = $request->title;
        $db->message = $request->message;
        $db->save();

        return redirect('/contact')->with('msg','ส่งข้อความเรียบร้อย');
    }
}