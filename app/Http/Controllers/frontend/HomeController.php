<?php

namespace App\Http\Controllers\frontend;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Slideshow;
use App\Teacher;
use App\Category;
use App\Products;
use Cohensive\Embed\Embed;
use Illuminate\Support\Facades\DB;

class HomeController extends Controller
{
    function index(){

        $count = new Products;
        $data['slideshows'] = Slideshow::all();
        $data['teachers'] = Teacher::all();
        $data['categorys'] = Category::all();
      //  $data['products'] = Products::orderByRaw('id', 'DESC')->where('tc_status', 0)->get();
        $data['products'] = $count->ProductsPromotion();

        return view('frontend.home',$data);
    }

    function policy(){
        echo 'policy';
      }


  
}