<?php

namespace App\Http\Controllers\frontend;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\MemberCourse;
use Auth;
use Anam\Phpcart\Cart;
use App\Products;
use App\OrderListModel;
use App\User;
use Illuminate\Support\Facades\DB;
use Illuminate\Validation\Rule;
use App\Http\Controllers\MailController as MailController;


class AddCourseController extends Controller
{
    function addCourseFree(Request $request){
        $userid = Auth::id();
        
        $checkHaveCourseMember = MemberCourse::where('member_id', $userid) ->where('product_id', $request->course_id)->first();

        if($checkHaveCourseMember == null){
       // dd($checkHaveCourseMember);
        $MemberCourse = new MemberCourse;
        $MemberCourse->member_id = $userid;
        $MemberCourse->product_id = $request->course_id;
        $MemberCourse->status_buy_course = 1;
        $MemberCourse->seq = 1;
        $MemberCourse->save();
        $response= array(
            'status' => 'success',
          );
        return \Response::json($response);
        
        }
    }

    function buyCourse(Request $request){
        $userid = Auth::id();
        $product = Products::find($request->product_id);
      

       // Save profile member
       $this->validate($request, [
        'email' => [
        'required',
              Rule::unique('users')->ignore(Auth::user()->id),
          ],
       ]);

       $member = User::find($userid);
       $member->name = $request->name;
       $member->email = $request->email;
       $member->tel = $request->tel;
       $member->save();

    //     // add to cart
    //     $cart = new Cart();
    //     $cart->setCart('course');
    //     $cart->add([
    //         'id'       => $product->id,
    //         'name'     => $product->tc_namecourse,
    //         'quantity' => 1,
    //         'price'    => $product->tc_discount
    //     ]);

    //   $db = new OrderListModel;
    //   $db->member_id =  $userid;
    //   $db->order_id = "ORD_".time();
    //   $db->order_item = serialize($cart->getItems());
    //   $db->order_totalqty = $cart->totalQuantity();
    //   $db->order_totalall = $cart->getTotal();
    //   $db->order_status = "waiting";
    //   $db->save();
    //   $insert_id = $db->id;
    //   $i= 1;
    //   foreach ($cart->getItems() as $key => $value) {

    //     DB::table('member_course')->insert([
    //       'member_id' => Auth::user()->id,
    //       'order_id' => $insert_id,
    //       'product_id' => $value->id,
    //       'seq' => $i,
    //       "created_at" =>  \Carbon\Carbon::now(),
    //       "updated_at" => \Carbon\Carbon::now(),
    //     ]);
    //     $i++;
    //   }
    //   $cart->clear();

    $insert_id_orderlist = $this->addToOrder($product,$userid,$status_order="waiting");
    
    $sendDataToMail = new MailController;
    $sendDataToMail->sendMailByCourse($insert_id_orderlist);
    
    // dd($cart->getItems());

    return redirect('/myorder');
    
    }

    function addToOrder($product,$userid,$status_order){
            // add to cart
        $cart = new Cart();
        $cart->setCart('course');
        $cart->add([
            'id'       => $product->id,
            'name'     => $product->tc_namecourse,
            'quantity' => 1,
            'price'    => $product->tc_discount
        ]);
  
        $db = new OrderListModel;
        $db->member_id =  $userid;
        $db->order_id = "ORD_".time();
        $db->order_item = serialize($cart->getItems());
        $db->order_totalqty = $cart->totalQuantity();
        $db->order_totalall = $cart->getTotal();
        $db->order_status = $status_order;
        $db->save();
        $insert_id = $db->id;
        $i= 1;

        foreach ($cart->getItems() as $key => $value) {
  
          DB::table('member_course')->insert([
            'member_id' => $userid,
            'order_id' => $insert_id,
            'product_id' => $value->id,
            'seq' => $i,
            'status_buy_course' => ($status_order == "waiting" ? 0 : 1),
            "created_at" =>  \Carbon\Carbon::now(),
            "updated_at" => \Carbon\Carbon::now(),
          ]);
          $i++;
        }
        $cart->clear();


        return $insert_id;
    }
}