<?php
namespace App\Http\Controllers;
 
use App\Http\Controllers\Controller;
use App\Mail\DemoEmail;
use App\Mail\MailUserName;
use App\Mail\MailSuccessBuyCourse;
use Illuminate\Support\Facades\Mail;
use App\OrderListModel;
use App\User;
use App\AddressShippingModel;

use Auth;

class MailController extends Controller
{
    public function sendMailByCourse($dataOrderID)
    {
        
        $data=  OrderListModel::find($dataOrderID);
     
        $user = User::find(Auth::id());

        try {
            Mail::to(Auth::user()->email)->cc('cs@getbiz.co')->send(new DemoEmail($data,$user));
        }
        catch(\Exception $e){
           $data['error'] = $e;
        }
     
       // return view('frontend.mails.demo',$data);
    }


    public function SendUserNameForNewUser($data_mail){
        
        try {
            Mail::to($data_mail['username'])->cc('cs@getbiz.co')->send(new MailUserName($data_mail));
        }
        catch(\Exception $e){
           $data['error'] = $e;
          // dd($data['error']);
        }
    }

    public function mailSuccessBuyCourse($userid,$dataOrderID){
        
        $data=  OrderListModel::find($dataOrderID);
     
        $user = User::find($userid);
        
        try {
            Mail::to($user->email)->cc('cs@getbiz.co')->send(new MailSuccessBuyCourse($data,$user));
        }
        catch(\Exception $e){
           $data['error'] = $e;
           //dd($data['error']);
        }
        
    }
}