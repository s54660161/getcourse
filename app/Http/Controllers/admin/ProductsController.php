<?php

namespace App\Http\Controllers\admin;

use App\Products;
use App\MemberCourse;
use App\Category;
use App\Teacher;
use App\User;

use App\ImageShowProduct;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Controllers\CenterController as CenterController;

class ProductsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $countMember = new MemberCourse;
        $data['products'] = $countMember->countMemberCourse();
     
        $data['category'] = Category::all();
       // $data['products'] = Products::orderBy('id', 'DESC')->get();
        return view('admin.products.products',$data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {        
        $data['category'] = Category::all();
        $data['teachers'] = Teacher::all();
       
        return  view('admin.products.addproduct',$data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        //dd($request->all());
        $centerControll = new CenterController;
        
        $image = $request->file('chooseFile');
        $name =  $centerControll->generateRandomString().'.'.$image->getClientOriginalExtension();
        $destinationPath = 'upload/Products/ImgTitle';
        $getDir = $centerControll->createDirectory($destinationPath);
        $image->move(public_path($getDir),$name);

         //ค่าของ tags dynamic input array
        if(!empty(array_filter($request->input('tc_tags')))){           
            $new_array_input = array_filter($request->input('tc_tags'));
            $dynamic_input_tags = serialize($new_array_input);
        }else{
            $dynamic_input_tags = "";
        }
        //dd(json_encode($request['tc_category']));
        $slug = $centerControll->convertToSlug($request->input('tc_slug_name'));
        $product = new Products;
        
        $product->tc_namecourse = $request['tc_namecourse'];
        $product->tc_title = $request['tc_title'];
        $product->tc_nameteacher = json_encode($request['tc_nameteacher']);
        $product->tc_category = json_encode($request['tc_category']);
        $product->tc_slug_name = $slug;
        $product->tc_woo_product_id = $request['tc_woo_product_id'];
        $product->tc_landingpage_code = $request['tc_landingpage_code'];

        $product->tc_free = $request['tc_free'];
        $product->tc_price = $request['tc_price'];
        $product->tc_discount = $request['tc_discount'];
     
        $product->tc_status = $request['tc_status'];
        $product->tc_promotion = $request['tc_promotion'];
        $product->tc_recommend = $request['tc_recommend'];
        $product->tc_description = $request['tc_description'];
        $product->tc_tags = $dynamic_input_tags;
        $product->tc_video_platform = $request['tc_video_platform'];
        $product->tc_video_preview = ($request['tc_video_platform']== 0 ? $request['tc_video_preview_vimeo'] : $request['tc_video_preview_youtube']);
        $product->tc_picture =  $getDir.$name;
        $product->save();


        return redirect()->route('admin.curriculum',$product->id)->with('msg','เพิ่มรายละเอียดคอร์สสำเร็จ');
       //return  redirect('admin/products')->with('msg','เพิ่มรายละเอียดคอร์สสำเร็จ');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Products  $products
     * @return \Illuminate\Http\Response
     */
    public function show(Products $products)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Products  $products
     * @return \Illuminate\Http\Response 
     */
    public function edit(Products $products,$id)
    {
        //dd($id);
       $data['EditProduct'] = $products::find($id);
       $data['category'] = Category::all();
       $data['teachers'] = Teacher::all();
       $data['teacher_array'] = json_decode($data['EditProduct']->tc_nameteacher);
       $data['category_array'] = json_decode($data['EditProduct']->tc_category);
       //dd($data['teacher']);
       $data['tags_all'] =  ($data['EditProduct']->tc_tags === "" ? "" : ($data['EditProduct']->tc_tags === NULL ? "" : unserialize($data['EditProduct']->tc_tags)));

       return view('admin.products.updateproduct',$data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Products  $products
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Products $products)
    {
       // dd($request->all());
        $centerControll = new CenterController;
        $image = $request->file('chooseFile');
        $imageold = $request->input('imageold');

        if ($image != null) {
          if (file_exists($imageold)) {
            unlink($imageold);
          }
          $name =  $centerControll->generateRandomString().'.'.$image->getClientOriginalExtension();
          $destinationPath = 'upload/Products/ImgTitle';
          $getDir = $centerControll->createDirectory($destinationPath);
          $image->move(public_path($getDir),$name);
        }

        
        //ค่าของ tags dynamic input array
        $get_old_tags = $products::find($request->id);
        if(!empty(array_filter($request->input('tc_tags')))){
            $unserialize_array = unserialize($get_old_tags->tc_tags);
            $chk_is_array = array_filter((is_array($unserialize_array) ? $unserialize_array : array($unserialize_array) ));                    
            $new_array_input = array_filter($request->input('tc_tags'));
            $array_merge = array_merge($chk_is_array,$new_array_input);
            $dynamic_input_tags = serialize($array_merge);

        }else{
            $dynamic_input_tags = $get_old_tags->tc_tags;
        }
        
        $slug = $centerControll->convertToSlug($request->input('tc_slug_name'));
        $product = $products::find($request['id']);
        $product->tc_namecourse = $request['tc_namecourse'];
        $product->tc_title = $request['tc_title'];
        $product->tc_nameteacher = json_encode($request['tc_nameteacher']);
        $product->tc_category = json_encode($request['tc_category']);
        $product->tc_slug_name = $slug;
        $product->tc_woo_product_id = $request['tc_woo_product_id'];
        $product->tc_landingpage_code = $request['tc_landingpage_code'];
        
        $product->tc_free = $request['tc_free'];
        $product->tc_price = $request['tc_price'];
        $product->tc_discount = $request['tc_discount'];
     
        $product->tc_status = $request['tc_status'];
        $product->tc_promotion = $request['tc_promotion'];
        $product->tc_recommend = $request['tc_recommend'];
        $product->tc_description = $request['tc_description'];
        $product->tc_tags = $dynamic_input_tags;
        $product->tc_video_platform = $request['tc_video_platform'];
        $product->tc_video_preview = ($request['tc_video_platform']== 0 ? $request['tc_video_preview_vimeo'] : $request['tc_video_preview_youtube']);
        $product->tc_picture =   ($image != null ? $getDir.$name : $imageold);
        $product->save();

        //return redirect()->route('admin.curriculum',$request['id'])->with('msg','เพิ่มรายละเอียดคอร์สสำเร็จ');
        return  redirect(url()->previous())->with('msg','แก้ไขรายละเอียดคอร์สสำเร็จ');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Products  $products
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request,Products $products)
    {
            //dd($request->id);
            $db = $products::find($request->id);

            if ($db != null) {
             if (file_exists($db->tc_picture)) {
                 unlink($db->tc_picture);
             }
             $products::destroy($request->id);
           }
           
           
           $response= array(
              'status' => 'success',
            );
            return \Response::json($response);
    }

    public function deletetags(Products $products,Request $request){

    
        $db =  $products::find($request->id_product);
        
        $unserialize = unserialize($db->tc_tags);

        unset($unserialize[$request->id_index]); //remove array by index array

        $re_array = array_values($unserialize);// reindex array after remove index array
        if(!empty(array_filter($re_array))){
            $db->tc_tags =  serialize(array_values($unserialize));
            $db->save();
        }else{
            $db->tc_tags = "";
            $db->save();
        }
        

        $response= array(
            'status' => 'success',
          );
        return \Response::json($response);

    }

    function seeMemberCourse($id){
      
        $data['memberCourse'] = MemberCourse::where([['product_id',$id],['status_buy_course',1]])->get();
       
        foreach($data['memberCourse'] as $val){
            $member[] = User::find($val->member_id);
        }
        $data['member'] =$member;
       return view('admin.products.memberProduct',$data);

    }

    function seeMemberCourseAll($id){
        $data['memberCourse'] = MemberCourse::where([['product_id',$id]])->get();
       
        foreach($data['memberCourse'] as $val){
            $member[] = User::find($val->member_id);
        }
        $data['member'] =$member;
       return view('admin.products.memberProduct',$data);

    }
}