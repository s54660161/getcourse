<?php

namespace App\Http\Controllers\admin;

use App\Category;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Controllers\CenterController as CenterController;

class CategoryProductController extends Controller
{

    function categoryProductAll(){
        $data['category'] = Category::all();
        return view('admin.category.category_product',$data);
    }

    function createCategory(Request $request,Category $category){
       
        $global_fn = new CenterController;
        
        $str_replace_to_slug = $global_fn->convertToSlug($request['category_name']);
        $category->category_name = $request['category_name'];
        $category->category_slug =$str_replace_to_slug;
        $category->save();

        return redirect('admin/categoryproductall')->with('msg-category','เพิ่มหมวดหมู่หลักของสินค้าเรียบร้อย');
    }

    
    function editCategory(Request $request){
       
        $global_fn = new CenterController;
        
        $str_replace_to_slug = $global_fn->convertToSlug($request['category_name']);
        $db = Category::find($request['id']);
        
        $db->category_name = $request['category_name'];
        $db->category_slug =$str_replace_to_slug;
        $db->save();

        return redirect('admin/categoryproductall')->with('msg-category','แก้ไขหมวดหมู่หลักของสินค้าเรียบร้อย');
    }

    function deleteCategory(Request $request){
        
      if ($request->id != null) {
        Category::where('id', '=', $request->id)->delete();

        $response= array(
           'status' => 'success',
        );
      }
       return \Response::json($response);
    }

 
}