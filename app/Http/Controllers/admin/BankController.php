<?php

namespace App\Http\Controllers\admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\BankModel;
use App\Http\Controllers\CenterController as CenterController;

class BankController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data['bank'] = BankModel::all();

        return view('admin.bank',$data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $db = new BankModel;
        $centerControll = new CenterController;

        $image = $request->file('chooseFile');
        $name =  $centerControll->generateRandomString().'.'.$image->getClientOriginalExtension();
        $destinationPath = 'upload/bank';
        $getDir = $centerControll->createDirectory($destinationPath);
        $image->move(public_path($getDir),$name);


        $db->picture = $getDir.$name;
        $db->name_owner = $request->input('name_owner');
        $db->bank_name = $request->input('bank_name');
        $db->bank_branch = $request->input('bank_branch');
        $db->account_number = $request->input('account_number');
        $db->save();

        return redirect('admin/bank')->with('msg','เพิ่มข้อมูลเรียบร้อย');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
          $centerControll = new CenterController;
          $image = $request->file('chooseFile');
          $imageold = $request->input('imageold');

          if ($image != null) {
            //dd($imageold);die;
            if (file_exists($imageold)) {
              unlink($imageold);
            }
            $name =  $centerControll->generateRandomString().'.'.$image->getClientOriginalExtension();
            $destinationPath = 'upload/bank';
            $getDir = $centerControll->createDirectory($destinationPath);
            $image->move(public_path($getDir),$name);
          }

          $db = BankModel::find($request->id);
          $db->picture = $image != null ? $getDir.$name : $imageold;
          $db->name_owner = $request->input('name_owner');
          $db->bank_name = $request->input('bank_name');
          $db->bank_branch = $request->input('bank_branch');
          $db->account_number = $request->input('account_number');
          $db->save();

          return redirect('admin/bank')->with('msg','แก้ไขข้อมูลเรียบร้อย');

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request)
    {
      //dd($request->id);
      $db = BankModel::find($request->bank_id);
      if ($db != null) {
        if (file_exists($db->picture)) {
            unlink($db->picture);
        }
        BankModel::destroy($request->bank_id);
      }
      $response= array(
         'status' => 'success',
       );
       return \Response::json($response);
    }
}