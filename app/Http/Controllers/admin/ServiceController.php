<?php

namespace App\Http\Controllers\admin;

use App\Service;
use App\CategoryService;
use App\SubCategoryService;
use App\ImageShowService;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Controllers\CenterController as CenterController;
class ServiceController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        
        $data['category'] = CategoryService::all();
        $data['subcategory'] = SubCategoryService::all();
        $data['service'] = Service::orderBy('id', 'DESC')->get();
        return view('admin.service.service',$data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $data['category'] = CategoryService::all();
        $data['subcategory'] = SubCategoryService::all();
        return view('admin.service.addservice',$data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request,Service $service)
    {
        //dd($request->all());
        $centerControll = new CenterController;
        
        $image = $request->file('chooseFile');
        $name =  $centerControll->generateRandomString().'.'.$image->getClientOriginalExtension();
        $destinationPath = 'upload/Service';
        $getDir = $centerControll->createDirectory($destinationPath);
        $image->move(public_path($getDir),$name);

        $slug = $centerControll->convertToSlug($request->input('slug'));
        
        $service->title =$request['title'];
        $service->slug = $slug;
        $service->category_id =$request['category'];
        $service->subcategory_id =$request['subcategory'];
        $service->description =$request['description'];
        $service->picture = $getDir.$name;
        $service->save();


        $review_image = $request->input('document');
        if($review_image){
            for($i = 0; $i < count($review_image); $i++) {
            $review_db = new ImageShowService;
            $review_db->image = 'upload/ImageShowService/' . $review_image[$i];
            $review_db->service_id =  $service->id;
            $review_db->save();
            }
        }

        
        return redirect('admin/services')->with('msg','เพิ่มข้อมูลบรการเรียบร้อย');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Service  $service
     * @return \Illuminate\Http\Response
     */
    public function show(Service $service)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Service  $service
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data['category'] = CategoryService::all();
        $data['subcategory'] = SubCategoryService::all();
        $data['EditService']= Service::find($id);
        $data['imageShow'] = ImageShowService::where('service_id', $id)->get();
        return view('admin.service.updateservice',$data);

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Service  $service
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Service $service)
    {
        
        $centerControll = new CenterController;
        $image = $request->file('chooseFile');
        $imageold = $request->input('imageold');

        if ($image != null) {
          if (file_exists($imageold)) {
            unlink($imageold);
          }
        //  dd($image);
          $name =  $centerControll->generateRandomString().'.'.$image->getClientOriginalExtension();
          $destinationPath = 'upload/Service';
          $getDir = $centerControll->createDirectory($destinationPath);
          $image->move(public_path($getDir),$name);
        }
        $slug = $centerControll->convertToSlug($request->input('slug'));

        $db = $service::find($request['id']);
        $db->title =$request['title'];
        $db->slug = $slug;
        $db->category_id =$request['category'];
        $db->subcategory_id =$request['subcategory'];
        $db->description =$request['description'];
        $db->picture =($image != null ? $getDir.$name : $imageold);
        $db->save();

        
        $review_image = $request->input('document');
        if($review_image){
            for($i = 0; $i < count($review_image); $i++) {
            $review_db = new ImageShowService;
            $review_db->image = 'upload/ImageShowService/' . $review_image[$i];
            $review_db->service_id =  $request['id'];
            $review_db->save();
            }
        }

        
        return redirect('admin/services')->with('msg','แก้ไขข้อมูลบรการเรียบร้อย');

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Service  $service
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request,Service $service)
    {
        $db = Service::find($request->id);
       
        if ($db != null) {
         if (file_exists($db->picture)) {
             unlink($db->picture);
         }
         Service::destroy($request->id);
       }

       $imageShow = ImageShowService::where('service_id', $request->id)->get();
     
       if ($imageShow != null) {
           foreach($imageShow as $img){
             if (file_exists($img->image)) {
                 unlink($img->image);
             }
           }
           ImageShowService::where('service_id', $request->id)->delete();
       }
       $response= array(
          'status' => 'success',
        );
        
        return \Response::json($response);
    }


    public function imageShowService(Request $request)
    {
        $global_fn = new CenterController;

        $destinationPath = 'upload/ImageShowService';
                    
        $file = $request->file('file');

        $name = uniqid() . '_' . trim($file->getClientOriginalName());

        $file->move($destinationPath, $name);
        

        return response()->json([
            'name'          => $name,
            'original_name' => $file->getClientOriginalName(),
        ]);
    }
    public function deleteImageShowService(Request $request){
       
        if($request->imageShow_id){
            $id = $request->imageShow_id;
            $image = $request->name;
            if (file_exists($image)) {
                unlink($image);
            }
            ImageShowService::destroy($id);
        }else{
            $image = 'upload/ImageShowService/' . $request->name;
            if (file_exists($image)) {
                unlink($image);
            }
        }

        $response= array(
            'status' => 'success',
          );
        return \Response::json($response);

    }
}