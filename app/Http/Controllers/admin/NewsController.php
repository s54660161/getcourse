<?php

namespace App\Http\Controllers\admin;

use App\News;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Controllers\CenterController as CenterController;

class NewsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data['newsall'] = News::orderBy('id', 'DESC')->get();
        return view('admin.news.news',$data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.news.addnews');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $db = new News;
        $global_fn = new CenterController;
        
        $str_replace_to_slug = $global_fn->convertToSlug($request['title']);
        
        //image add to folder
        $image = $request->file('chooseFile');
        $name =  $global_fn->generateRandomString().'.'.$image->getClientOriginalExtension();
        $destinationPath = 'upload/news';
        $getDir = $global_fn->createDirectory($destinationPath);
        $image->move(public_path($getDir),$name);
        

        $db->title = $request['title'];
        $db->abstract = $request['abstract'];
        $db->slug = $str_replace_to_slug;
        $db->description = $request['description'];
        $db->picture = $getDir.$name;
        $db->save();
        
        return redirect('admin/news')->with('msg','เพิ่มข่าวสารเรียบร้อย');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\News  $news
     * @return \Illuminate\Http\Response
     */
    public function show(News $news)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\News  $news
     * @return \Illuminate\Http\Response
     */
    public function edit(News $news)
    {
       // $data['getNews'] = News::find($id);

        return view('admin.news.updatenews',compact('news'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\News  $news
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, News $news)
    {
        //dd($request['title']);
        $global_fn = new CenterController;
        $image = $request->file('chooseFile');
        $imageold = $request->input('imageold');
        $db = News::find($request['id']);
        
        $str_replace_to_slug = $global_fn->convertToSlug($request['title']);
        
        if ($image != null) {
        if (file_exists($imageold)) {
            unlink($imageold);
        }
        //  dd($image);
        $name =  $global_fn->generateRandomString().'.'.$image->getClientOriginalExtension();
        $destinationPath = 'upload/news';
        $getDir = $global_fn->createDirectory($destinationPath);
        $image->move(public_path($getDir),$name);
        }

        $db->title = $request['title'];
        $db->abstract = $request['abstract'];
        $db->slug = $str_replace_to_slug;
        $db->description = $request['description'];
        $db->picture = ($image != null ? $getDir.$name : $imageold) ;
        $db->save();

        return redirect('admin/news')->with('msg','แก้ไขข่าวสารเรียบร้อย');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\News  $news
     * @return \Illuminate\Http\Response
     */
     function destroy(Request $request)
    {
        $db = News::find($request->id);
       
        if ($db != null) {
         if (file_exists($db->picture)) {
             unlink($db->picture);
         }
         News::destroy($request->id);
       }
       $response= array(
          'status' => 'success',
        );
        
        return \Response::json($response);
    }
}