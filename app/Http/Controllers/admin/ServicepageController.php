<?php

namespace App\Http\Controllers\admin;
use App\Servicepage;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Controllers\CenterController as CenterController;

class ServicepageController extends Controller
{
    function index(){
        $data['servicepage'] = Servicepage::all();
        return view('admin.servicepage.servicepage',$data);
    }
    function addservicepage(Request $request){

        //dd($request->all());
        //image add to folder
        $global_fn = new CenterController;
        $db = new Servicepage;
        //dd($request->all());
        $image = $request->file('chooseFile');
        $name =  $global_fn->generateRandomString().'.'.$image->getClientOriginalExtension();
        $destinationPath = 'upload/servicepage';
        $getDir = $global_fn->createDirectory($destinationPath);
        $image->move(public_path($getDir),$name);

        $db->title = $request['title'];
        $db->picture = $getDir.$name;
        $db->save();

        return redirect('admin/servicepage')->with('msg','เพิ่มบริการเรียบร้อย');
    }

    function updateServicePage(Request $request){
        $global_fn = new CenterController;
        $db = Servicepage::find($request['id']);
       //dd($request->all());

        $image = $request->file('chooseFileOld');
        $imageold = $request['imageold'];
        
        if($image != null){
            if (file_exists($imageold)) {
                unlink($imageold);
            }
            $name =  $global_fn->generateRandomString().'.'.$image->getClientOriginalExtension();
            $destinationPath = 'upload/servicepage';
            $getDir = $global_fn->createDirectory($destinationPath);
            $image->move(public_path($getDir),$name);
        }
       
        $db->title = $request['title'];
        $db->picture = ($image != null ? $getDir.$name : $imageold);
        $db->save();
        
        return redirect('admin/servicepage')->with('msg','แก้ไขหน้าบริการเรียบร้อย');
    }
    function deleteServicePage(Request $request){
        // dd($request->all());
         $db = Servicepage::find($request['id']);
       if($db != null){
         if (file_exists($db->picture)) {
             unlink($db->picture);
         }
         Servicepage::destroy($request->id);
         $response= array(
             'status' => 'success',
           );
       }else{
         $response= array(
             'status' => 'error',
           );
       }
     
       return \Response::json($response);
     }
}