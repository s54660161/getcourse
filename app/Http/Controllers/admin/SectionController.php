<?php

namespace App\Http\Controllers\admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Section;
use App\Lecture;
use App\Products;

class SectionController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
    
        return view('admin.curriculum.section_form');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request,Section $section)
    {
        //dd($request->all());
        $check = $section::where('sec_product_id',$request['sec_product_id'])->orderBy('sec_sequence', 'desc')->first();
        //dd($check);
        $section->sec_title = $request['sec_title'];
        $section->sec_sequence = ($check == null ? 1 : $check->sec_sequence +1);
        $section->sec_product_id = $request['sec_product_id'];
        // $section->sec_video_platform = $request['sec_video_platform'];
        $section->sec_status = $request['sec_status'];
        $section->save();

        return redirect()->route('admin.curriculum',$request['sec_product_id'])->with('msg','สร้างบทเรียนเรียบร้อย');

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $data['sec_product_id'] = $id;
        return view('admin.sections.add_section',$data);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data['EditSection']= Section::find($id);
        $data['sec_product_id'] = $data['EditSection']->sec_product_id;
        return view('admin.sections.edit_section',$data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $section = Section::find($id);
        $section->sec_title = $request['sec_title'];
        $section->sec_product_id = $request['sec_product_id'];
        // $section->sec_video_platform = $request['sec_video_platform'];
        $section->sec_status = $request['sec_status'];
        $section->save();
        
        return redirect()->route('admin.curriculum',$request['sec_product_id'])->with('msg','แก้ไขบทเรียนเรียบร้อย');

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request)
    {
        if($request->id){
            Section::destroy($request->id);
            Lecture::where('lecture_section_id',$request->id)->delete();;
        }
        $response= array(
            'status' => 'success',
          );
          
        return \Response::json($response);
    }


    function curriculum($id){


        $data['sections'] = Section::where('sec_product_id',$id)->orderBy('sec_sequence', 'asc')->get();
        $data['lectures'] = Lecture::orderBy('lecture_sequence', 'asc')->get();
        $data['product'] =  Products::find($id);
      //dd($data['lectures']);
        $data['product_id'] = $id;
        return view('admin.curriculum.curriculum',$data);
    }


    function sortSection(Request $request){
      
        parse_str($request->data, $output);
     
        foreach($output['section'] as $key => $val){
        // echo ($output['section'][$key]);
            try{
                $flight = Section::findOrFail($output['section'][$key]);
                $flight->sec_sequence = $key+1;
                $flight->save();
            }catch(\Illuminate\Database\QueryException $e){
                dd($e);
                return \Response::json($e);
            }
        }
        
        $response= array(
            'status' => 'OK',
        
        );
        
        return \Response::json($response);
    }
    
}