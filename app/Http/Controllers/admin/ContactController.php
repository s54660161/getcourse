<?php

namespace App\Http\Controllers\admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\ContactModel;
class ContactController extends Controller
{
    function index(){
        
        $data['all_msg'] = ContactModel::all();
        return view('admin.msgcontact',$data);
    }

}