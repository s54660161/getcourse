<?php

namespace App\Http\Controllers\admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\OrderListModel;
use App\User;
use App\NoticePaymentModel;
use App\MemberCourse;

class OrderController extends Controller
{
  function index(){
    $dborder = new OrderListModel;
      //$data['orderall'] =  OrderListModel::orderBy('id', 'DESC')->get();
      $data['orderall'] =   $dborder->orderall();
      
   // dd($data['orderall']);die;
    return view('admin/order/orderall',$data);
  }

  function showorderlist($id){
    $dbUser = new User;

    $data['orderlist'] = OrderListModel::find($id);//ออเดอร์การสั่งซื้อ
    $data['unserializeItem'] = unserialize($data['orderlist']->order_item);//รายละเอียดสินค้าที่สั่งซื้อ
    $data['users_account'] = User::find($data['orderlist']->member_id);//ข้อมมูลผู้สั่งซื้อ
   
    $data['notice_payment'] = NoticePaymentModel::where('order_id',$data['orderlist']->order_id)->first();//เช็คการยืนยันการจ่ายเงิน


    return view('admin/order/showorderlist',$data);
  }

  function confirmpayment($status,$id){
    
      if ($status === "success") {
        $db =   OrderListModel::find($id);
        $db->order_status = $status;
        $db->save();
      //dd($status);

        MemberCourse::where('order_id', $id)
                  ->update(['status_buy_course' => 1]);
       return redirect(url()->previous());

      }else {
        
        $db =   OrderListModel::find($id);
        $notice = NoticePaymentModel::where('order_id',$db->order_id)->first();
     
        if($notice == null){
          $db->order_status = "waiting";
          $db->save();
        }else{
          $db->order_status = "pending";
          $db->save();
        }
      
        MemberCourse::where('order_id', $id)
        ->update(['status_buy_course' => 0]);
        return redirect(url()->previous());
      }
      
  }


  function printOrder($id){
    $dbUser = new User;
    $dbShipping = new AddressShippingModel;

    $data['orderlist'] = OrderListModel::find($id);//ออเดอร์การสั่งซื้อ
    $data['unserializeItem'] = unserialize($data['orderlist']->order_item);//รายละเอียดสินค้าที่สั่งซื้อ
    $data['users_account'] = User::find($data['orderlist']->member_id);//ข้อมมูลผู้สั่งซื้อ
    $data['address_shipping'] = $dbShipping->getAddress($data['orderlist']->member_id);//ข้อมมูลผู้สั่งซื้อ
    $data['notice_payment'] = NoticePaymentModel::where('order_id',$data['orderlist']->order_id)->first();//เช็คการยืนยันการจ่ายเงิน


    return view('admin/order/printorderlist',$data);
  }


  function delete(Request $request){
    //dd($request->id);
    //dd($request->id);
    $db = OrderListModel::find($request->id);
    if ($db != null) {
      OrderListModel::destroy($request->id);
      MemberCourse::where('order_id', $request->id)->delete();
      $response= array(
         'status' => 'success',
      );
    }

     return \Response::json($response);

  }
}