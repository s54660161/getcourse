<?php

namespace App\Http\Controllers\admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use session;
use Illuminate\Support\Facades\Hash;
use App\Admin;

class AdminController extends Controller
{
    function index(){
        return view('admin.login');
    }
    function blank(){
      return view('admin.blank');
  }
    function checkLogin(Request $request){

        $request->session()->forget('adminlogin');
  
        $username = $request->input('username');
        $password = $request->input('password');
  
        $admin =  Admin::where('username',$username)->first();
       if ($admin && Hash::check($password, $admin->password)) {
      //  dd($admin);
  
           $request->session()->put('adminlogin', $admin);
           $session = session('adminlogin')->username;
           return redirect('admin/products');
        }
  
        return redirect('admin')->with('msg','เข้าสู่ระบบไม่สำเร็จ');
      }
  
  
      function logout(Request $request){
        if ($request->session()->has('adminlogin')) {
            $request->session()->forget('adminlogin');
            return view('admin/login');
        }else{
          return view('admin/login');
        }
      }


      function alladmin(){
        $data['alladmin']  = DB::table('admin')->get();
        //dd($data['alladmin']);
        return view('admin.alladmin.alladmin',$data);
      }
  
      function addusername(Request $request){
        //dd($request);
  
        DB::table('admin')->insert([
            [
              'username' => $request->username,
              'password' =>  Hash::make($request->password),
              'is_admin' => $request->is_admin,
              'status' => "active"

            ]
        ]);
        return redirect('admin/alladmin')->with('msg','เพิื่มข้อมูลสำเร็จ');
      }
  
      function editusername(Request $request){
        if ($request->password != "") {
          DB::table('admin')
              ->where('id', $request->id)
              ->update([
                'username' => $request->username,
                'password' =>  Hash::make($request->password),
                'is_admin' => $request->is_admin,
                'status' => "active"
              ]);
        }else {
          DB::table('admin')
              ->where('id', $request->id)
              ->update([
                'username' => $request->username,
                'is_admin' => $request->is_admin,
                'status' => "active"

          ]);
        }
        return redirect('admin/alladmin')->with('msg','แก้ไขข้อมูลสำเร็จ');
  
      }
  
      function deleteusername(Request $request){
        if ($request->id != null) {
          DB::table('admin')->where('id', '=', $request->id)->delete();
          $response= array(
             'status' => 'success',
          );
        }
         return \Response::json($response);
      }
}