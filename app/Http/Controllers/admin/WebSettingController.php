<?php

namespace App\Http\Controllers\admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Controllers\CenterController as CenterController;

use App\WebSetting;
class WebSettingController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data['setting'] = WebSetting::first();
        return view('admin.setting',$data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
      //dd($request->all());
      $global_fn = new CenterController;
      $destinationPath = 'upload/images/';
      
      $id = $request->input('id') != "" ? $request->input('id') : "";
      if ($id != "") {
        $db = WebSetting::find($id);
      }else {
        $db = new WebSetting;
      }
      
      //input form image
      $chooseFilelogo = $request->file('chooseFilelogo');
      $chooseFilefavicon = $request->file('chooseFilefavicon');
      $lineqrcode = $request->file('lineqrcode');
      //image old
      $imageold_chooseFilelogo = $request->input('imageold_chooseFilelogo');
      $imageold_chooseFilefavicon = $request->input('imageold_chooseFilefavicon');
      $imageold_lineqrcode = $request->input('imageold_lineqrcode');

      //logo image
        if ($chooseFilelogo != null) {
            //dd($chooseFilelogo);
            if (file_exists($imageold_chooseFilelogo)) {
            unlink($imageold_chooseFilelogo);
            }
            $name_chooseFilelogo =  $global_fn->generateRandomString().'.'.$chooseFilelogo->getClientOriginalExtension();
            $chooseFilelogo->move(public_path($destinationPath),$name_chooseFilelogo);
        }
      //favicon image
        if ($chooseFilefavicon != null) {
            //dd($chooseFilelogo);
            if (file_exists($imageold_chooseFilefavicon)) {
              unlink($imageold_chooseFilefavicon);
            }
            $name_chooseFilefavicon =  $global_fn->generateRandomString().'.'.$chooseFilefavicon->getClientOriginalExtension();
            $chooseFilefavicon->move(public_path($destinationPath),$name_chooseFilefavicon);
        }
        
      //line qrcode image
      if ($lineqrcode != null) {
          if (file_exists($imageold_lineqrcode)) {
            unlink($imageold_lineqrcode);
          }
          $name_lineqrcode =  $global_fn->generateRandomString().'.'.$lineqrcode->getClientOriginalExtension();
          $lineqrcode->move(public_path($destinationPath),$name_lineqrcode);
      }

      $db->tabheader_text = $request['tabheader_text'];
      $db->text_footer_left = $request['text_footer_left'];
      $db->text_footer_right = $request['text_footer_right'];
      $db->title = $request['title'];
      $db->name_office = $request['name_office'];
      $db->keyword = $request['keyword'];
      $db->description = $request['description'];
      $db->address = $request['address'];
      $db->tel = $request['tel'];
      $db->fax = $request['fax'];
      $db->website = $request['website'];
      $db->facebook_page = $request['facebook_page'];
      $db->ig_page = $request['ig_page'];
      $db->line_id = $request['line_id'];
      $db->email = $request['email'];
      $db->facebook_pixel = $request['facebook_pixel'];
      $db->google_analytics = $request['google_analytics'];
      $db->hitstat = $request['hitstat'];
      $db->logo = ($chooseFilelogo != null ? $destinationPath.$name_chooseFilelogo : $imageold_chooseFilelogo);
      $db->favicon = ($chooseFilefavicon != null ? $destinationPath.$name_chooseFilefavicon : $imageold_chooseFilefavicon);
      $db->line_qrcode = ($lineqrcode != null ? $destinationPath.$name_lineqrcode : $imageold_lineqrcode);
      $db->save();

      return redirect('admin/setting')->with('msg','อัพเดตการตั้งค่าสำเร็จ');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}