<?php

namespace App\Http\Controllers\admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Lecture;
use App\Section;

class LectureController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request,Lecture $lecture)
    {
        $check = $lecture::where('lecture_section_id',$request['lecture_section_id'])->orderBy('lecture_sequence', 'desc')->first();
        $id_product = Section::find($request['lecture_section_id']);
       // dd($request->all());
        $lecture->lecture_video_link = $request['lecture_video_link'];
        $lecture->lecture_title = $request['lecture_title'];
        $lecture->lecture_free_video = $request['lecture_free_video'];
        $lecture->lecture_sequence = ($check == null ? 1 : $check->lecture_sequence +1);
        $lecture->lecture_section_id = $request['lecture_section_id'];
        $lecture->save();
        
      return redirect()->route('admin.curriculum',$id_product['sec_product_id'])->with('msg','สร้างบทเรียนเรียบร้อย');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $data['lecture_section_id'] = $id;
        
        return view('admin.lectures.add_lecture',$data);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
     $data['EditLecture'] = Lecture::find($id);
     $data['lecture_section_id'] = $data['EditLecture']->lecture_section_id;

     return view('admin.lectures.edit_lecture',$data);

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $id_product = Section::find($request['lecture_section_id']);

        $lecture = Lecture::find($id);
        $lecture->lecture_video_link = $request['lecture_video_link'];
        $lecture->lecture_free_video = $request['lecture_free_video'];
        $lecture->lecture_title = $request['lecture_title'];
        $lecture->lecture_section_id = $request['lecture_section_id'];
        $lecture->save();

        return redirect()->route('admin.curriculum',$id_product['sec_product_id'])->with('msg','แก้ไขบทเรียนเรียบร้อย');

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request)
    {
      

        if($request->id){
            Lecture::destroy($request->id);
        }
        $response= array(
            'status' => 'success',
          );
          
        return \Response::json($response);
    }

    public function sortLecture(Request $request)
    {
        parse_str($request->data, $output);

        foreach($output['lecture_video'] as $key => $val){
          
                try{
                    $flight = Lecture::findOrFail($output['lecture_video'][$key]);
                    $flight->lecture_sequence = $key+1;
                    $flight->save();
                }catch(\Illuminate\Database\QueryException $e){
                    dd($e);
                    return \Response::json($e);
                }
            }
            
            $response= array(
                'status' => 'OK',
            );
            
            return \Response::json($response);
    }
}