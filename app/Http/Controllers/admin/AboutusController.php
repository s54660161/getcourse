<?php

namespace App\Http\Controllers\admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Aboutus;
use App\Http\Controllers\CenterController as CenterController;

class AboutusController extends Controller
{
    function index(){
        $data['about_us'] = Aboutus::first();
        return view('admin.about_us',$data);
    }

    function updateaboutus(Request $request){
      
        $id = $request->input('id') != "" ? $request->input('id') : "";
        if ($id != "") {
          $db = Aboutus::find($id);
        }else {
          $db = new Aboutus;
        }
  
        $db->description = $request['description'];

        $db->save();

        return redirect('admin/aboutus')->with('msg','แก้ไขข้อมูลเรียบร้อย');
    }

}