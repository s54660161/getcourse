<?php

namespace App\Http\Controllers\admin;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use App\Teacher;
use App\Http\Controllers\CenterController as CenterController;

class TeacherController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data['teacher'] = Teacher::orderBy('id', 'DESC')->get();
        return view('admin.teachers.teacher',$data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return  view('admin.teachers.addteacher');
        
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $centerControll = new CenterController;
        

        //ค่าของ experience dynamic input array
        if(!empty(array_filter($request->input('experience')))){           
            $new_array_input = array_filter($request->input('experience'));
            $dynamic_input_experience = serialize($new_array_input);
        }else{
            $dynamic_input_experience = "";
        }
        
         //ค่าของ portfolio dynamic input array
         if(!empty(array_filter($request->input('portfolio')))){           
            $new_array_input = array_filter($request->input('portfolio'));
            $dynamic_input_portfolio = serialize($new_array_input);
        }else{
            $dynamic_input_portfolio = "";
        }
        
        $image = $request->file('chooseFile');
        $name =  $centerControll->generateRandomString().'.'.$image->getClientOriginalExtension();
        $destinationPath = 'upload/Teachers';
        $getDir = $centerControll->createDirectory($destinationPath);
        $image->move(public_path($getDir),$name);
       
        $Teacher =   new Teacher;
        $Teacher->name = $request->name;
        $Teacher->title = $request->title;
        $Teacher->course = $request->course;
        $Teacher->experience = $dynamic_input_experience;
        $Teacher->portfolio = $dynamic_input_portfolio;
        $Teacher->code_video = $request->code_video;
        $Teacher->picture = $getDir.$name;
        $Teacher->save();


        return  redirect('admin/teacher')->with('msg','เพิ่มรายละเอียดผู้สอนเรียบร้อย');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
       
        $data['EditTeacher'] = Teacher::find($id);
        $data['experience_all'] =  ($data['EditTeacher']->experience === "" ? "" : ($data['EditTeacher']->experience === NULL ? "" : unserialize($data['EditTeacher']->experience)));
        $data['portfolio_all'] =  ($data['EditTeacher']->portfolio === "" ? "" : ($data['EditTeacher']->portfolio === NULL ? "" : unserialize($data['EditTeacher']->portfolio)));

       // dd($EditTeacher->id);
       return view('admin.teachers.updateteacher',$data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $centerControll = new CenterController;
        
        $image = $request->file('chooseFile');
        $imageold = $request->input('imageold');

        
        if ($image != null) {
            if (file_exists($imageold)) {
              unlink($imageold);
            }
          //  dd($image);
            $name =  $centerControll->generateRandomString().'.'.$image->getClientOriginalExtension();
            $destinationPath = 'upload/Teachers';
            $getDir = $centerControll->createDirectory($destinationPath);
            $image->move(public_path($getDir),$name);
          }

            //ค่าของ experience dynamic input array
            $get_old_data = Teacher::find($id);
            if(!empty(array_filter($request->input('experience')))){
                $unserialize_array = unserialize($get_old_data->experience);
                $chk_is_array = array_filter((is_array($unserialize_array) ? $unserialize_array : array($unserialize_array) ));                    
                $new_array_input = array_filter($request->input('experience'));
                $array_merge = array_merge($chk_is_array,$new_array_input);
                $dynamic_input_experience = serialize($array_merge);

            }else{
                $dynamic_input_experience = $get_old_data->experience;
            }

            if(!empty(array_filter($request->input('portfolio')))){
                $unserialize_portfolio = unserialize($get_old_data->portfolio);
                $chk_portfolio_array = array_filter((is_array($unserialize_portfolio) ? $unserialize_portfolio : array($unserialize_portfolio) ));                    
                $new_array_portfolio = array_filter($request->input('portfolio'));
                $array_merge_portfolio = array_merge($chk_portfolio_array,$new_array_portfolio);
                $dynamic_input_portfolio = serialize($array_merge_portfolio);

            }else{
                $dynamic_input_portfolio = $get_old_data->portfolio;
            }

          $Teacher =    Teacher::find($id);
          $Teacher->name = $request->name;
          $Teacher->title = $request->title;
          $Teacher->course = $request->course;
          $Teacher->experience = $dynamic_input_experience;
          $Teacher->portfolio = $dynamic_input_portfolio;
          $Teacher->code_video = $request->code_video;
          $Teacher->picture = ($image != null ? $getDir.$name : $imageold);
          $Teacher->save();
  
  
          return  redirect('admin/teacher')->with('msg','แก้ไขรายละเอียดผู้สอนเรียบร้อย');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request)
    {
       // dd($request->id);
        $db = Teacher::find($request->id);
       
        if ($db != null) {
         if (file_exists($db->picture)) {
             unlink($db->picture);
         }
         Teacher::destroy($request->id);
       }
       $response= array(
          'status' => 'success',
        );
        
        return \Response::json($response);
    }


    public function deleteExperience(Teacher $teacher,Request $request){    
        $db =  $teacher::find($request->id_product);
        
        $unserialize = unserialize($db->experience);

        unset($unserialize[$request->id_index]); //remove array by index array

        $re_array = array_values($unserialize);// reindex array after remove index array
        if(!empty(array_filter($re_array))){
            $db->experience =  serialize(array_values($unserialize));
            $db->save();
        }else{
            $db->experience = "";
            $db->save();
        }
        

        $response= array(
            'status' => 'success',
          );
        return \Response::json($response);

    }

    public function deletePortfolio(Teacher $teacher,Request $request){    
        $db =  $teacher::find($request->id_product);
        
        $unserialize = unserialize($db->portfolio);

        unset($unserialize[$request->id_index]); //remove array by index array

        $re_array = array_values($unserialize);// reindex array after remove index array
        if(!empty(array_filter($re_array))){
            $db->portfolio =  serialize(array_values($unserialize));
            $db->save();
        }else{
            $db->portfolio = "";
            $db->save();
        }
        
        $response= array(
            'status' => 'success',
          );
        return \Response::json($response);

    }
}