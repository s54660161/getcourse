<?php

namespace App\Http\Controllers\admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Slideshow;
use App\Banner;
use App\Products;
use App\Http\Controllers\CenterController as CenterController;

class SlidebannerController extends Controller
{   
    function slideshow(){
        $data['slideshows'] = Slideshow::all();
        return view('admin.slidebanner.slideshow',$data);
    }

    
   
    function addSlideshow(Request $request){
        //image add to folder
        $global_fn = new CenterController;
        //dd($request->all());
        $image = $request->file('chooseFile');
        $name =  $global_fn->generateRandomString().'.'.$image->getClientOriginalExtension();
        $destinationPath = 'upload/slidebanner';
        $getDir = $global_fn->createDirectory($destinationPath);
        $image->move(public_path($getDir),$name);

        //** slide Mobile
        $imageMB = $request->file('chooseFileMb');
        $nameMB =  $global_fn->generateRandomString().'.'.$imageMB->getClientOriginalExtension();
        $destinationPathMB = 'upload/slidebanner';
        $getDirMB = $global_fn->createDirectory($destinationPathMB);
        $imageMB->move(public_path($getDirMB),$nameMB);

        $db = new Slideshow;
        $db->picture = $getDir.$name;
        $db->picture_mb = $getDirMB.$nameMB;
        $db->save();

        return redirect('admin/slideshow');
    }

    function updateSlideshow(Request $request){
        $global_fn = new CenterController;
        $db = Slideshow::find($request['id']);
       //dd($request->all());

        $image = $request->file('chooseFileOld');
        $image_mobile = $request->file('editchooseFileMb');
        $imageold = $request['imageold'];
        $picture_mb_old = $request['picture_mb_old'];

        if($image != null){
            if (file_exists($imageold)) {
                unlink($imageold);
            }
            $name =  $global_fn->generateRandomString().'.'.$image->getClientOriginalExtension();
            $destinationPath = 'upload/slidebanner';
            $getDir = $global_fn->createDirectory($destinationPath);
            $image->move(public_path($getDir),$name);
        }
        
        if($image_mobile  != null){
            if(file_exists($picture_mb_old)) {
              unlink($picture_mb_old);
            }
            $imageMB = $request->file('editchooseFileMb');
            $nameMB =  $global_fn->generateRandomString().'.'.$imageMB->getClientOriginalExtension();
            $destinationPathMB = 'upload/slidebanner';
            $getDirMB = $global_fn->createDirectory($destinationPathMB);
            $imageMB->move(public_path($getDirMB),$nameMB);
          }

        $db->picture = ($image != null ? $getDir.$name : $imageold);
        $db->picture_mb = ($image_mobile != null ? $getDirMB.$nameMB : $picture_mb_old);
        $db->save();

        return redirect('admin/slideshow');
    }

    
    function deleteSlideshow(Request $request){
        // dd($request->all());
         $db = Slideshow::find($request['id']);
       if($db != null){
         if (file_exists($db->picture)) {
             unlink($db->picture);
         }
         if (file_exists($db->picture_mb)) {
            unlink($db->picture_mb);
        }
         Slideshow::destroy($request->id);
         $response= array(
             'status' => 'success',
           );
       }else{
         $response= array(
             'status' => 'error',
           );
       }
     
       return \Response::json($response);
     }

     
    /// BANNER
    
    function banner(){
        $data['banners'] = Banner::all();
        $data['products'] = Products::all();
        return view('admin.slidebanner.banner',$data);
    }
    
    function addBanner(Request $request){
        //image add to folder
        $global_fn = new CenterController;
        $db = new Banner;
        $slug = Products::find($request['product_id']);

        //dd($request->all());
        $image = $request->file('chooseFile');
        $name =  $global_fn->generateRandomString().'.'.$image->getClientOriginalExtension();
        $destinationPath = 'upload/slidebanner';
        $getDir = $global_fn->createDirectory($destinationPath);
        $image->move(public_path($getDir),$name);

        $db->picture = $getDir.$name;
        $db->product_id = $request['product_id'];
        $db->link = $slug->pd_slug_name;
        $db->save();

        return redirect('admin/banner');
    }

    function updateBanner(Request $request){
        $global_fn = new CenterController;
        $db = Banner::find($request['id']);
       // dd($request->all());
       $slug = Products::find($request['product_id']);

        $image = $request->file('chooseFileOld');
        $imageold = $request['imageold'];
        
        if($image != null){
            
            if (file_exists($imageold)) {
                unlink($imageold);
            }
            
            $name =  $global_fn->generateRandomString().'.'.$image->getClientOriginalExtension();
            $destinationPath = 'upload/slidebanner';
            $getDir = $global_fn->createDirectory($destinationPath);
            $image->move(public_path($getDir),$name);
        }
       

        $db->picture = ($image != null ? $getDir.$name : $imageold);
        $db->product_id = $request['product_id'];
        $db->link = $slug->pd_slug_name;

        $db->save();

        return redirect('admin/banner');
    }


    function deleteBanner(Request $request){
       // dd($request->all());
        $db = Banner::find($request['id']);
      if($db != null){
        if (file_exists($db->picture)) {
            unlink($db->picture);
        }
        Banner::destroy($request->id);
        $response= array(
            'status' => 'success',
          );
      }else{
        $response= array(
            'status' => 'error',
          );
      }
    
      return \Response::json($response);
    }
}