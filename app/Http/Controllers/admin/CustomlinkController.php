<?php

namespace App\Http\Controllers\admin;

use App\Customlink;
use App\ImageShowCustomLink;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Controllers\CenterController as CenterController;

class CustomlinkController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
    //dd(url('/'));

    $data['customlink'] = Customlink::orderBy('id', 'DESC')->get();
    return view('admin.customlink.customlink',$data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.customlink.addcustomlink');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request,Customlink $customlink)
    {
        $centerControll = new CenterController;
        
        $image = $request->file('chooseFile');
        $name =  $centerControll->generateRandomString().'.'.$image->getClientOriginalExtension();
        $destinationPath = 'upload/customlink';
        $getDir = $centerControll->createDirectory($destinationPath);
        $image->move(public_path($getDir),$name);
        
        $slug = $centerControll->convertToSlug($request->input('slug'));
        
        $customlink->slug =  $slug;
        $customlink->name = $request['name'];
        $customlink->title = $request['title'];
        $customlink->description = $request['description'];
        $customlink->picture = $getDir.$name;
        $customlink->save();
        
        $review_image = $request->input('document');
        if($review_image){
            for($i = 0; $i < count($review_image); $i++) {
            $review_db = new ImageShowCustomLink;
            $review_db->image = 'upload/ImageShowCustomLink/' . $review_image[$i];
            $review_db->customlink_id =  $customlink->id;
            $review_db->save();
            }
        }
        
       return  redirect('admin/customlink')->with('msg','เพิ่มรายละเอียดข้อมูลสำเร็จ');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Customlink  $customlink
     * @return \Illuminate\Http\Response
     */
    public function show(Customlink $customlink)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Customlink  $customlink
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data['imageShow'] = ImageShowCustomLink::where('customlink_id', $id)->get();
        $data['customlink'] = Customlink::find($id);
       // dd($data['imageShow']);
        return view('admin.customlink.updatecustomlink',$data);
        
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Customlink  $customlink
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Customlink $customlink)
    {
        $centerControll = new CenterController;
        
        $image = $request->file('chooseFile');
        $imageold = $request->input('imageold');

        if ($image != null) {
          if (file_exists($imageold)) {
            unlink($imageold);
          }
          $name =  $centerControll->generateRandomString().'.'.$image->getClientOriginalExtension();
          $destinationPath = 'upload/customlink';
          $getDir = $centerControll->createDirectory($destinationPath);
          $image->move(public_path($getDir),$name);
        }

        
        $slug = $centerControll->convertToSlug($request->input('slug'));
        $db = $customlink::find($request['id']);
        $db->slug =  $slug;
        $db->name = $request['name'];
        $db->title = $request['title'];
        $db->description = $request['description'];
        $db->picture = ($image != null ? $getDir.$name : $imageold) ;
        $db->save();

        $review_image = $request->input('document');
        if($review_image){
            for($i = 0; $i < count($review_image); $i++) {
            $review_db = new ImageShowCustomLink;
            $review_db->image = 'upload/ImageShowCustomLink/' . $review_image[$i];
            $review_db->customlink_id =  $request['id'];
            $review_db->save();
            }
        }

        
       return  redirect('admin/customlink')->with('msg','แก้ไขรายละเอียดข้อมูลสำเร็จ');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Customlink  $customlink
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request,Customlink $customlink)
    {
        $db = $customlink::find($request->id);
       
        if ($db != null) {
         if (file_exists($db->picture)) {
             unlink($db->picture);
         }
         $customlink::destroy($request->id);
       }


       $imageShow = ImageShowCustomLink::where('customlink_id', $request->id)->get();
     
       if ($imageShow != null) {
           foreach($imageShow as $img){
             if (file_exists($img->image)) {
                 unlink($img->image);
             }
           }
           ImageShowCustomLink::where('customlink_id', $request->id)->delete();
       }
       $response= array(
          'status' => 'success',
        );
        
        return \Response::json($response);
    }


    public function imageShowCustomLink(Request $request)
    {
        $global_fn = new CenterController;

        $destinationPath = 'upload/ImageShowCustomLink';
                    
        $file = $request->file('file');

        $name = uniqid() . '_' . trim($file->getClientOriginalName());

        $file->move($destinationPath, $name);
        

        return response()->json([
            'name'          => $name,
            'original_name' => $file->getClientOriginalName(),
        ]);
    }
    public function deleteImageCustomLink(Request $request){
       
        if($request->imageShow_id){
            $id = $request->imageShow_id;
            $image = $request->name;
            if (file_exists($image)) {
                unlink($image);
            }
            ImageShowCustomLink::destroy($id);
        }else{
            $image = 'upload/ImageShowCustomLink/' . $request->name;
            if (file_exists($image)) {
                unlink($image);
            }
        }

        $response= array(
            'status' => 'success',
          );
        return \Response::json($response);

    }
}