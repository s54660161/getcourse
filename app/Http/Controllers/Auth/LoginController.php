<?php

namespace App\Http\Controllers\Auth;
use Carbon\Carbon;
use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Auth;
use Socialite;
use App\User;
class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    
    public function redirectTo(){
       //dd(url()->previous());
        return url()->previous();
    }
  
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }
  
    public function redirectToProvider($provider)
    {
        return Socialite::driver($provider)->redirect();
    }
 
 
    public function handleProviderCallback($provider)
    {
       // $user = Socialite::driver($provider)->user();
        try {
            $user = Socialite::driver($provider)->user();
        } catch (\Exception $e) {
            return redirect('/login');
        }
        $authUser = $this->findOrCreateUser($user, $provider);
        Auth::login($authUser, true);
        User::where('id',$authUser->id)->update(['updated_at' => Carbon::now()]);

        return redirect('/courseme');
    }
 
 
    public function findOrCreateUser($user, $provider)
    {

    
        $authUser = User::where('provider_id', $user->id)->first();

       //dd($user->id);
        if ($authUser) {
            return $authUser;
        }
 
        return User::create([
            'name'     => $user->name,
            // 'email'    => $user->email,
            'provider' => $provider,
            'provider_id' => $user->id
        ]);
    }
}