<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\ContactModel;

// use Illuminate\Support\Facades\Mail;
// use App\Mail\SendEmailTest;
// use App\Mail\Replymail;
class MsgcontactController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
      $data['all_msg'] = ContactModel::orderBy('id', 'DESC')->get();

      return view('admin.msgcontact',$data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'g-recaptcha-response' => 'required|captcha'
        ]);

        $db = New ContactModel;
        
        $db->name = $request->name;
        $db->email = ($request->email == "" ? "" : $request->email);
        $db->tel = $request->tel;
        $db->title = ($request->title == "" ? "" : $request->title);
        $db->description = ($request->message == "" ? "" : $request->message);
        $db->save();


        // Mail::to($request->email)->send(new SendEmailTest($request->name));

        return redirect(url()->previous())->with('msg', 'ส่งข้อมูลการติดต่อถึงเจ้าหน้าที่เรียบร้อย');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request)
    {
      $id =   $request->id;
      ContactModel::destroy($id);
      $response= array(
         'status' => 'success',
       );
      return \Response::json($response);
    }


    public function replycontact(Request $request){

      $contact = ContactModel::find($request->id);
      //dd($data['contactbyadmin']->title);
      // $db = New Replycontact;
      // $db->contact_id = $request->id;
      // $db->reply_msg = $request->replycontact;
      // $db->save();

      $data['valuetomail'] = array(
        'name' => $contact->name,
        'title' => $contact->title,
        'replycontact' => $request->replycontact
      );
      
//      Mail::to($contact->email)->send(new Replymail($data['valuetomail']));

      return redirect('admin/msgcontact')->with('msg', 'ส่งข้อมูลการตอบกลับเรียบร้อย');

    }
}