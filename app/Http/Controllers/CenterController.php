<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use session;
use App\Products;
use App\SubCategoryService;
use App\SubCategoryProduct;
use App\District;
use App\SubDistrict;


class CenterController extends Controller
{
    public function apiGet(){
      $name = $dd = session('userdata');
      //$name = $this->generateRandomString();
      return $name;
    }


    public function createDirectory($path) {
        $year = date("Y");
        $month = date("m");
        $year_folder = $path . '/' . $year . '/';

        $month_folder = $year_folder . $month . '/';
        if(!is_dir(public_path($year_folder))){
	           mkdir(public_path($year_folder));
              chmod(public_path($year_folder),0777);
            }
        if(!is_dir(public_path($month_folder))){
              mkdir(public_path($month_folder) );
              chmod(public_path($month_folder),0777);
            }
        return $month_folder;

      }

      function generateRandomString($length = 5) {
          $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
          $charactersLength = strlen($characters);
          $randomString = '';
          for ($i = 0; $i < $length; $i++) {
              $randomString .= $characters[rand(0, $charactersLength - 1)];
          }
          return md5($randomString);
      }

      function DateThai($strDate)
	{
		$strYear = date("Y",strtotime($strDate))+543;
		$strMonth= date("n",strtotime($strDate));
		$strDay= date("j",strtotime($strDate));
		$strHour= date("H",strtotime($strDate));
		$strMinute= date("i",strtotime($strDate));
		$strSeconds= date("s",strtotime($strDate));
		$strMonthCut = $thai_month_arr=array(   
            "0"=>"",   
            "1"=>"มกราคม",   
            "2"=>"กุมภาพันธ์",   
            "3"=>"มีนาคม",   
            "4"=>"เมษายน",   
            "5"=>"พฤษภาคม",   
            "6"=>"มิถุนายน",    
            "7"=>"กรกฎาคม",   
            "8"=>"สิงหาคม",   
            "9"=>"กันยายน",   
            "10"=>"ตุลาคม",   
            "11"=>"พฤศจิกายน",   
            "12"=>"ธันวาคม"                    
        ); 
		$strMonthThai=$strMonthCut[$strMonth];
		return "$strDay $strMonthThai $strYear";
    }
    
    function convertToSlug($string)
    {
        return preg_replace('/[^A-Za-z0-9ก-๙\-]/u', '-',str_replace('&', '-and-', $string));
    }


    function ajaxSubcategory(Request $request){
        $category_id = $request->input('category_id');
        $subcatgory = SubCategoryProduct::where('category_id', $category_id)->get();
        if ($subcatgory != NULL) {
          $response = array(
            'status' => 'success',
            'html' => $subcatgory
         );
         return \Response::json($response);
        }
      }

      function ajaxSubcategoryService(Request $request){
        $category_id = $request->input('category_id');
        $subcatgory = SubCategoryService::where('category_id', $category_id)->get();
        if ($subcatgory != NULL) {
          $response = array(
            'status' => 'success',
            'html' => $subcatgory
         );
         return \Response::json($response);
        }
      }


      function ajaxDistrict(Request $request){
        $province_id = $request->input('province_id');
        $districts = District::where('province_id', $province_id)->get();

        //dd($districts);
        if ($districts != NULL) {
          $response = array(
            'status' => 'success',
            'html' => $districts
         );
         return \Response::json($response);
        }
      }

      function ajaxSubDistrict(Request $request){
        $amphur_id = $request->input('amphur_id');
        $subdistricts = SubDistrict::where('district_id', $amphur_id)->get();
        if ($subdistricts != NULL) {
          $response = array(
            'status' => 'success',
            'html' => $subdistricts
         );
         return \Response::json($response);
        }
      }


      function ajaxPostalCode(Request $request){
        $sub_district_id = $request->input('tambon_id');
        $postalCode = SubDistrict::where('id', $sub_district_id)->get();
        if ($postalCode != NULL) {
          $response = array(
            'status' => 'success',
            'html' => $postalCode
         );
         return \Response::json($response);
        }
      }


}