<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class OrderListModel extends Model
{
    protected $table = 'orderlist';
    public $timestamps = true;
  
  
    function orderall(){
  
      $orderall = DB::table('orderlist')
              ->leftJoin('users', 'users.id', '=', 'orderlist.member_id')
              ->select('orderlist.*', 'users.name as member_name')
              ->orderBy('id', 'desc')
              ->get();
  
      return $orderall;
    }
  
}