<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class MailUserName extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * Create a new message instance.
     *
     * @return void
     */

    public $data_mail;
    public function __construct($data_mail)
    {
        $this->data_mail = $data_mail;
       
        //dd($this->address);
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->from('no-reply@getbiz.co','[DO NOT REPLY] BY บริษัท เก็ท บิซ (ไทยแลนด์) จำกัด')
        ->subject('รหัสเข้าใช้งานระบบ www.getbiz.co')
        ->view('mails.mailusername');
    }
}