<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class MailSuccessBuyCourse extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * Create a new message instance.
     *
     * @return void
     */

    public $demo;
    public $user;
    public function __construct($demo,$user)
    {
        $this->demo = $demo;
        $this->user = $user;
        //dd($this->address);
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->from('no-reply@getbiz.co','[DO NOT REPLY] BY บริษัท เก็ท บิซ (ไทยแลนด์) จำกัด')
        ->subject('ยืนยันการชำระเงินเรียบร้อย')
        ->view('mails.mailsuccessbuycourse');
    }
}