<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class WebSetting extends Model
{
    protected $table = 'websetting';
    public $timestamps = false;
    
}