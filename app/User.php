<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Support\Facades\DB;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name',
        'email',
        'provider',
        'provider_id',
        'password',
        'auth_id',
        'address',
        'tambon',
        'amphur',
        'province',
        'postcode',
        'tel',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];


    function getAddress($userid){

        $getAddress = DB::table('users')
                ->leftJoin('postal_codes', 'postal_codes.id', '=', 'users.postcode')
                ->leftJoin('sub_districts', 'sub_districts.id', '=', 'users.tambon')
                ->leftJoin('districts', 'districts.id', '=', 'users.amphur')
                ->leftJoin('provinces', 'provinces.id', '=', 'users.province')
                ->where('users.id', '=',$userid)
                ->select(
                  'users.*',
                  'users.name as member_name',
                  'provinces.name as province_name',
                  'districts.name as districts_name',
                  'sub_districts.name as tambon_name',
                  'postal_codes.code as postcode_name'
                  )
                ->first();
        return $getAddress;
      }

    function usersCountCourse(){
        $usersCountCourse = DB::table('users')
        ->select('users.*', 'member_count.count_course')
        ->leftJoin(DB::raw('(SELECT member_id,  COUNT(`member_id`) as count_course FROM `member_course` GROUP BY `member_id`)
        member_count'), 
        function($join)
        {
           $join->on('users.id', '=', 'member_count.member_id');
        })
        ->get();
    
        return $usersCountCourse;
    }
}