<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class NoticePaymentModel extends Model
{
  protected $table = 'notice_payment';
  public $timestamps = true;
}
