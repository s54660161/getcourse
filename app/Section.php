<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Section extends Model
{
    protected $table = 'section_video';
    public $timestamps = true;


    public function lecture()
    {
        return $this->hasMany('App\Lecture','lecture_section_id')->orderBy('lecture_sequence','asc');
    }

}