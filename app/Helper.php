<?php

function formatDateThat($strDate)
{
    $strYear = date("Y",strtotime($strDate))+543;
    $strMonth= date("n",strtotime($strDate));
    $strDay= date("j",strtotime($strDate));
    $strHour= date("H",strtotime($strDate));
    $strMinute= date("i",strtotime($strDate));
    $strSeconds= date("s",strtotime($strDate));
    $strMonthCut = Array("","ม.ค.","ก.พ.","มี.ค.","เม.ย.","พ.ค.","มิ.ย.","ก.ค.","ส.ค.","ก.ย.","ต.ค.","พ.ย.","ธ.ค.");
    $strMonthThai=$strMonthCut[$strMonth];
    return "$strDay $strMonthThai $strYear $strHour:$strMinute";
}

function vimeoGetDetailVideo($id,$type){
       
    if($id){
        if($type == 0){
             $vimeo =  json_decode(file_get_contents("https://vimeo.com/api/oembed.json?url=https%3A//vimeo.com/".$id));
             $toTime = gmdate("i:s", $vimeo->duration);
             return $toTime;
        }else{
            $youtube =  json_decode(file_get_contents("https://www.googleapis.com/youtube/v3/videos?part=snippet%2CcontentDetails%2Cstatistics&id=".$id."&key=AIzaSyDvgmHOKwECUoFs72o385k9--o-CeCNOhE"));
            $pattern='/PT(\d+)M(\d+)S/';
            preg_match($pattern,$youtube->items[0]->contentDetails->duration,$matches);
            $seconds = $matches[1]*60+$matches[2];
            $toTime =  gmdate("i:s",$seconds);
            return $toTime;
        }
    }else{
        return "-";
    }
  
  }

  function convertToSlug($string)
  {
      return preg_replace('/[^A-Za-z0-9ก-๙\-]/u', '-',str_replace('&', '-and-', $string));
  }

  function urlGetBiz(){
    $url = 'https://www.getbiz.co/';
    return $url;
}