<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Lecture extends Model
{
    protected $table = 'lecture_video';
    public $timestamps = true;


    public function section()
    {
        return $this->belongsTo('App\Section');
    }
}