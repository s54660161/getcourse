<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
class MemberCourse extends Model
{
    protected $table = 'member_course';
    public $timestamps = true;


    function checkCourseMember($member_id,$id){
        $checkCourseMember = DB::table('member_course')->where([
            ['member_id', '=', $member_id],
            ['product_id', '=', $id]
        ])->first();
        return $checkCourseMember;
    }

    function countMemberCourse(){
       
        $countMemberCourse = DB::table('products')
        ->select('products.*', 'TotalActiveMemberCourse.*','TotalAllMemberCourse.*')
        ->leftJoin(DB::raw('(SELECT product_id, COUNT(member_id) as member_count FROM `member_course` WHERE status_buy_course =1  GROUP BY product_id)
           TotalActiveMemberCourse'), 
        function($join)
        {
           $join->on('products.id', '=', 'TotalActiveMemberCourse.product_id');
        })
        ->leftJoin(DB::raw('(SELECT product_id, COUNT(member_id) as allmember FROM `member_course`   GROUP BY product_id)
                TotalAllMemberCourse'), 
        function($join)
        {
            $join->on('products.id', '=', 'TotalAllMemberCourse.product_id');
        })
        ->get();
        
        //dd($countMemberCourse);

        return $countMemberCourse;
    }
}