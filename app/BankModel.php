<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class BankModel extends Model
{
  protected $table = 'payment_account';
  public $timestamps = false;
}