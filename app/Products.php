<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Products extends Model
{
    protected $table = 'products';
    public $timestamps = true;

    function countSection(){
        $countSection = DB::table('section_video')
         ->select(DB::raw('count(sec_product_id) as section_video,sec_product_id'))
         ->groupBy('sec_product_id')
         ->get();
 
         return $countSection;
     }

     function Products(){
       
        $Products = DB::table('products')
        ->select('products.*',DB::raw('SUM(lecture.count_lec) as sum_count_lec'))
        ->leftJoin(DB::raw('(SELECT section_video.id as sec_id, section_video.sec_product_id   FROM `section_video`)
        section'),
        function($join)
        {
           $join->on('products.id', '=', 'section.sec_product_id');
        })
        ->leftJoin( DB::raw('(SELECT `lecture_section_id` ,COUNT(`lecture_section_id`) as count_lec FROM `lecture_video` GROUP BY `lecture_section_id`)
        lecture'), function($join){
            $join->on('section.sec_id', '=', 'lecture.lecture_section_id');
        })
        ->where('products.tc_status', '=', 0)
        ->orderBy('products.id', 'DESC')
        ->groupBy('products.id')
        ->get();
        
    // dd($Products);

        return $Products;
    }

    function ProductsReccomment(){
       
        $Products = DB::table('products')
        ->select('products.*',DB::raw('SUM(lecture.count_lec) as sum_count_lec'))
        ->leftJoin(DB::raw('(SELECT section_video.id as sec_id, section_video.sec_product_id   FROM `section_video`)
        section'),
        function($join)
        {
           $join->on('products.id', '=', 'section.sec_product_id');
        })
        ->leftJoin( DB::raw('(SELECT `lecture_section_id` ,COUNT(`lecture_section_id`) as count_lec FROM `lecture_video` GROUP BY `lecture_section_id`)
        lecture'), function($join){
            $join->on('section.sec_id', '=', 'lecture.lecture_section_id');
        })
        ->where([['products.tc_status', '=', 0],['products.tc_recommend', '=', 1]])
        ->orderBy('products.id', 'DESC')
        ->groupBy('products.id')
        ->limit(5)
        ->get();
        
     //  dd($Products);

        return $Products;
    }

    
    function ProductsById($id){
       
        $ProductsById = DB::table('products')
        ->select('products.*',DB::raw('SUM(lecture.count_lec) as sum_count_lec'))
        ->leftJoin(DB::raw('(SELECT section_video.id as sec_id, section_video.sec_product_id   FROM `section_video`)
        section'),
        function($join)
        {
           $join->on('products.id', '=', 'section.sec_product_id');
        })
        ->leftJoin( DB::raw('(SELECT `lecture_section_id` ,COUNT(`lecture_section_id`) as count_lec FROM `lecture_video` GROUP BY `lecture_section_id`)
        lecture'), function($join){
            $join->on('section.sec_id', '=', 'lecture.lecture_section_id');
        })
        ->where([['products.tc_status', '=', 0],['products.id','=',$id]])
        ->orderBy('products.id', 'DESC')
        ->groupBy('products.id')
        ->first();
        
      //  dd($countMemberCourse);

        return $ProductsById;
    }


    function ProductsPromotion(){
       
        $Products = DB::table('products')
        ->select('products.*',DB::raw('SUM(lecture.count_lec) as sum_count_lec'))
        ->leftJoin(DB::raw('(SELECT section_video.id as sec_id, section_video.sec_product_id   FROM `section_video`)
        section'),
        function($join)
        {
           $join->on('products.id', '=', 'section.sec_product_id');
        })
        ->leftJoin( DB::raw('(SELECT `lecture_section_id` ,COUNT(`lecture_section_id`) as count_lec FROM `lecture_video` GROUP BY `lecture_section_id`)
        lecture'), function($join){
            $join->on('section.sec_id', '=', 'lecture.lecture_section_id');
        })
        ->where([['products.tc_promotion', '=', 1],['products.tc_status', '=', 0]])
        ->orderBy('products.id', 'DESC')
        ->groupBy('products.id')
        ->limit(5)
        ->get();
  
        return $Products;
    }
 
    function ProductsFree(){
       
        $Products = DB::table('products')
        ->select('products.*',DB::raw('SUM(lecture.count_lec) as sum_count_lec'))
        ->leftJoin(DB::raw('(SELECT section_video.id as sec_id, section_video.sec_product_id   FROM `section_video`)
        section'),
        function($join)
        {
           $join->on('products.id', '=', 'section.sec_product_id');
        })
        ->leftJoin( DB::raw('(SELECT `lecture_section_id` ,COUNT(`lecture_section_id`) as count_lec FROM `lecture_video` GROUP BY `lecture_section_id`)
        lecture'), function($join){
            $join->on('section.sec_id', '=', 'lecture.lecture_section_id');
        })
        ->where([['products.tc_free', '=', 1],['products.tc_status', '=', 0]])
        ->orderBy('products.id', 'DESC')
        ->groupBy('products.id')
        ->limit(5)
        ->get();
        
     //  dd($Products);

        return $Products;
    }
}