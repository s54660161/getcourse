@extends('admin.layout.main_admin')



@section('content')
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header" style="padding:0px">
        @if (session('msg'))
        <div class="alert alert-success">
            {{ session('msg') }}
        </div>
        @endif
        <div class=" small-box bg-aqua" style="margin:0px">


            <div class="inner">
                <div class="text-right">
                    <span class="pull-left">
                        <h4>การแสดงผลของวิดีโอ :
                            @if($product->tc_video_platform == 0)
                            <i class="fa fa-vimeo "></i> Vimeo
                            @else
                            <i class="fa fa-youtube-play text-red"></i> Youtube

                            @endif
                        </h4>
                    </span>
                    <a href="{{ route('admin.section.show',$product_id) }}" class="btn  bg-orange btn-md">
                        <i class="fa fa-fw fa-plus"></i> เพิ่มบทเรียน
                    </a>
                </div>

            </div>
        </div>
    </section>

    <section class="content">
        <div class="row">
            <div class="col-md-12 connectedSortable " id="section_course">

                @foreach($sections as $sec)
                <!-- TO DO List -->
                <div class="box box-primary box-solid" id="section_{{ $sec->id }}">
                    <div class=" box-header">
                        <i class="fa fa-arrows"></i>

                        <h3 class="box-title">{{ $sec->sec_title }}</h3>
                        <h3 class="box-title pull-right">
                            <button class="btn btn-danger delete_section" section_id="{{$sec->id}}">
                                <i class="fa fa-fw fa-trash "></i> ลบบทเรียน
                            </button>

                            <a href="{{ route('admin.section.edit',$sec->id) }}" class="btn btn-success">
                                <i class="fa fa-fw fa-wrench "></i> แก้ไขหัวข้อบทเรียน
                            </a>
                            <a href="{{ route('admin.lecture.show',$sec->id) }}" class="btn btn-info">
                                <i class="fa fa-fw fa-plus"></i> สร้างวิดีโอบทเรียน
                            </a>
                        </h3>

                    </div>
                    <!-- /.box-header -->
                    <div class="box-body">
                        <!-- See dist/js/pages/dashboard.js to activate the todoList plugin -->
                        <ul class="todo-list" id="todo">
                            @foreach($lectures as $lecture)
                            @if($lecture->lecture_section_id == $sec->id)
                            <li style="border-left:4px solid #00c0ef;" id="lecture_video_{{ $lecture->id }}">
                                <!-- drag handle -->
                                <span class="handle">
                                    <i class="fa fa-ellipsis-v"></i>
                                    <i class="fa fa-ellipsis-v"></i>
                                </span>

                                <!-- todo text -->
                                <span class="text">{{ $lecture->lecture_title}}</span>
                                <!-- Emphasis label -->
                                <!-- General tools such as edit or delete-->
                                <div class="tools" style="display:inline-block;">
                                    <a href="{{route('admin.lecture.edit',$lecture->id)}}">
                                        <i class="fa fa-edit" data-toggle="tooltip" data-placement="top"
                                            title="แก้ไขวิดีโอบทเรียน"></i>
                                    </a>
                                    <i class="fa fa-trash-o delete_lecture" lecture_id="{{$lecture->id}}"
                                        data-toggle="tooltip" data-placement="top" title="ลบวิดีโอบทเรียน">
                                    </i>
                                </div>
                            </li>
                            @endif
                            @endforeach
                        </ul>
                    </div>
                    <!-- /.box-body -->

                </div>
                <!-- /.box -->
                @endforeach
            </div>
        </div>

        <div class="alert-tab">
            <div class="alert-cart">
                <div class="alert alert-success alert-dismissible">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                    <i class="icon fa fa-check"></i> <span id="alert-text">Save Success</span>
                </div>
            </div>
        </div>

    </section>
</div>

@endsection


@section('script')

<style>
.alert-tab {
    position: fixed;
    bottom: 50%;
    right: 1%;
    padding: 12px;
}

.alert-tab .alert-cart {
    position: fixed;
    bottom: 5%;
    right: 1%;
    display: none;
}
</style>
<script src="{{URL::asset('admin_backend/dist/js/pages/dashboard.js')}}"></script>

<script>
var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');

$.widget.bridge('uibutton', $.ui.button);

$("#section_course").sortable({
    update: function(e, ui) {
        var data = $(this).sortable('serialize');

        $.ajax({
            url: "/admin/section/sortSection",
            type: 'post',
            data: {
                _token: CSRF_TOKEN,
                data: data

            },
            success: function(result) {
                if (result.status == "OK") {

                    $('#alert-text').text("Section Save Success");
                    $('.alert-cart').fadeIn(1000);
                    setTimeout(function() {
                        $('.alert-cart').fadeOut(1000);
                    }, 4000);
                }
            },
            complete: function() {

            }
        });
    }

});

$(".todo-list").sortable({
    update: function(e, ui) {
        var data = $(this).sortable('serialize');
        console.log(data)

        $.ajax({
            url: "/admin/lecture/sortLecture",
            type: 'post',
            data: {
                _token: CSRF_TOKEN,
                data: data

            },
            success: function(result) {
                if (result.status == "OK") {
                    $('#alert-text').text("Lecture Save Success");
                    $('.alert-cart').fadeIn(1000);
                    setTimeout(function() {
                        $('.alert-cart').fadeOut(1000);
                    }, 4000);
                }

            },
            complete: function() {

            }
        });
    }
});

$(".delete_lecture").click(function(e) {
    var id = $(this).attr("lecture_id");
    if (confirm("คุณต้องการลบวิดีโอใช่หรือไม่ ?")) {
        console.log(id);
        $.ajax({
            type: "POST",
            url: "/admin/lecture/destroy",
            data: {
                _token: CSRF_TOKEN,
                id: id
            },
            dataType: 'JSON',
            success: function(data) {
                //console.log(data['data']);
                if (data['status'] == "success") {
                    $("#lecture_video_" + id).remove();

                    $('#alert-text').text("Lecture Delete Success");
                    $('.alert-cart').fadeIn(1000);
                    setTimeout(function() {
                        $('.alert-cart').fadeOut(1000);
                    }, 4000);
                }


            }
        });
    } else {
        e.preventDefault();
    }
});

$(".delete_section").click(function(e) {
    var id = $(this).attr("section_id");
    if (confirm("คุณต้องการลบบทเรียนใช่หรือไม่")) {
        // console.log(id);
        $.ajax({
            type: "POST",
            url: "/admin/section/destroy",
            data: {
                _token: CSRF_TOKEN,
                id: id
            },
            dataType: 'JSON',
            success: function(data) {
                //console.log(data['data']);
                if (data['status'] == "success") {
                    $("#section_" + id).remove();
                    $('#alert-text').text("Section Delete Success");
                    $('.alert-cart').fadeIn(1000);
                    setTimeout(function() {
                        $('.alert-cart').fadeOut(1000);
                    }, 4000);
                }

            }
        });
    } else {
        e.preventDefault();
    }
});
</script>

@endsection