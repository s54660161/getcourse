<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title><?php echo (!empty($keys_global->title) ? $keys_global->title :  "" ) ?> | ADMIN PAGE</title>
    <link rel="shortcut icon" href="{{ URL::asset(!empty($keys_global->favicon) ? $keys_global->favicon :  "") }} ">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <!-- Tell the browser to be responsive to screen width -->
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <!-- Bootstrap 3.3.7 -->
    <link rel="stylesheet" href="{{URL::asset('admin_backend/bower_components/bootstrap/dist/css/bootstrap.min.css')}}">
    <!-- Font Awesome -->
    <link rel="stylesheet"
        href="{{URL::asset('admin_backend/bower_components/font-awesome/css/font-awesome.min.css')}}">
    <!-- Ionicons -->
    <link rel="stylesheet" href="{{URL::asset('admin_backend/bower_components/Ionicons/css/ionicons.min.css')}}">
    <!-- Select2 -->
    <link rel="stylesheet" href="{{ URL::asset('admin_backend/bower_components/select2/dist/css/select2.min.css')}}">
    <!-- Theme style -->
    <link rel="stylesheet" href="{{URL::asset('admin_backend/dist/css/AdminLTE.min.css')}}">

    <!-- AdminLTE Skins. Choose a skin from the css/skins
       folder instead of downloading all of them to reduce the load. -->
    <link rel="stylesheet" href="{{URL::asset('admin_backend/dist/css/skins/_all-skins.css')}}">



    <script src="{{URL::asset('admin_backend/bower_components/jquery/dist/jquery.min.js')}}"></script>
    <script src="{{URL::asset('admin_backend/bower_components/jquery-ui/jquery-ui.min.js')}}"></script>

    <!-- Bootstrap 3.3.7 -->
    <script src="{{URL::asset('admin_backend/bower_components/bootstrap/dist/js/bootstrap.min.js')}}"></script>
    <script src="{{ URL::asset('admin_backend/bower_components/select2/dist/js/select2.full.min.js')}}"></script>

    <!-- SlimScroll -->
    <script src="{{URL::asset('admin_backend/bower_components/jquery-slimscroll/jquery.slimscroll.min.js')}}"></script>
    <!-- FastClick -->
    <script src="{{URL::asset('admin_backend/bower_components/fastclick/lib/fastclick.js')}}"></script>
    <!-- AdminLTE App -->
    <script src="{{URL::asset('admin_backend/dist/js/adminlte.min.js')}}"></script>
    <!-- AdminLTE for demo purposes -->
    <script src="{{URL::asset('admin_backend/dist/js/demo.js')}}"></script>
    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <![endif]-->

    <!-- Google Font -->
    <link rel="stylesheet"
        href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">
</head>

<body class="hold-transition skin-blue sidebar-mini">
    <!-- Site wrapper -->
    <div class="wrapper">

        <header class="main-header">
            <!-- Logo -->
            <a href="/admin/blank" class="logo">
                <!-- mini logo for sidebar mini 50x50 pixels -->
                <span class="logo-mini"><b>B</b></span>
                <!-- logo for regular state and mobile devices -->
                <span class="logo-lg">
                    <b>

                        ADMIN
                    </b>
                </span>
            </a>
            <!-- Header Navbar: style can be found in header.less -->
            <nav class="navbar navbar-static-top">
                <!-- Sidebar toggle button-->
                <a href="#" class="sidebar-toggle" data-toggle="push-menu" role="button">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </a>

                <div class="navbar-custom-menu">
                    <ul class="nav navbar-nav">

                        <!-- User Account: style can be found in dropdown.less -->
                        <li class="dropdown user user-menu">

                            <a href="/admin/logout"><span class="glyphicon glyphicon-off"></span> LOGOUT</a>
                            <ul class="dropdown-menu">
                                <!-- User image -->
                                <li class="user-header">
                                    <img src="{{ URL::asset(!empty($keys_global->favicon) ? $keys_global->favicon :  "") }}"
                                        class="img-circle" alt="User Image">

                                    <p>
                                        @if(Session::has('adminlogin'))
                                        {{ session('adminlogin')->username }}
                                        @endif
                                    </p>
                                </li>

                                <!-- Menu Footer-->
                                <!-- <li class="user-footer">
                                    <div class="pull-left">
                                    <a href="#" class="btn btn-default btn-flat">Profile</a>
                                    </div>
                                    <div class="pull-right">
                                    <a href="/admin/logout" class="btn btn-default btn-flat">Sign out</a>
                                    </div>
                                </li> -->
                            </ul>
                        </li>

                    </ul>
                </div>
            </nav>
        </header>

        <!-- =============================================== -->

        <!-- Left side column. contains the sidebar -->
        <aside class="main-sidebar">
            <!-- sidebar: style can be found in sidebar.less -->
            <section class="sidebar">
                <!-- Sidebar user panel -->
                <div class="user-panel">
                    <div class="pull-left image">
                        <img src="{{ URL::asset(!empty($keys_global->favicon) ? $keys_global->favicon :  "") }}"
                            class="img-circle" alt="User Image">
                    </div>
                    <div class="pull-left info">
                        <p>
                            @if(Session::has('adminlogin'))
                            {{ session('adminlogin')->username }}
                            @endif
                        </p>
                        <a href="#"><i class="fa fa-circle text-success"></i> Online</a>
                    </div>
                </div>
                <!-- sidebar menu: : style can be found in sidebar.less -->
                <ul class="sidebar-menu" data-widget="tree">

                    <li><a href="/admin/products"><i class="fa fa-play-circle text-aqua"></i>
                            <span>COURSE</span></a></li>
                    <li><a href="/admin/orderall"><i class="fa fa-book text-aqua"></i>
                            <span>ORDER</span></a></li>
                    <li>
                        <a href="/admin/categoryproductall">
                            <i class="fa fa-sitemap text-aqua"></i>
                            <span>CATEGORY</span></a>
                    </li>
                    <li>
                        <a href="/admin/teacher">
                            <i class="fa fa-user-plus text-aqua"></i>
                            <span>MENTORS</span></a>
                    </li>
                    <li>
                        <a href="/admin/memberall">
                            <i class="fa fa-user text-aqua"></i>
                            <span>MEMBER</span>
                        </a>
                    </li>

                    <!-- <li><a href="/admin/bank"><i class="fa fa-bank text-aqua"></i>
                            <span>PAYMENT</span></a></li> -->
                    <li><a href="/admin/slideshow"><i class="fa fa-file-image-o text-aqua"></i>
                            <span>BANNER</span></a></li>
                    <li><a href="/admin/contact"><i class="fa  fa-envelope text-aqua"></i>
                            <span>CONTACT US</span></a></li>
                    <li><a href="/admin/setting"><i class="fa fa-cog text-aqua"></i>
                            <span>SETTING</span></a></li>
                    <!-- <li><a href="/admin/banner"><i class="fa fa-circle-o text-yellow"></i>
                            <span>แบนเนอร์สินค้า</span></a></li> -->

                </ul>
            </section>
            <!-- /.sidebar -->
        </aside>

        <!-- =============================================== -->


        @yield('content')


        <!-- =============================================== -->


        <footer class="main-footer text-center">

            <strong>Copyright &copy; 2018 .</strong> All rights
            reserved.
        </footer>
    </div>
    <!-- jQuery 3 -->
    @yield('script')
    <script>
    $(document).ready(function() {
        $('.sidebar-menu').tree()
    })
    </script>
    @if(session('msg'))
    <script>
    setTimeout(function() {
        $('.alert-success').fadeOut(1000);
    }, 3000);
    </script>
    @endif
</body>

</html>