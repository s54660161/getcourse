@extends('admin.layout.main_admin')


@section('content')

<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <section class="content">
        <div class="row">
            <div class="col-md-12">
                <!-- SELECT2 EXAMPLE -->
                @if (session('msg'))
                <div class="alert alert-success">
                    {{ session('msg') }}
                </div>
                @endif
                <div class="box box-success">
                    <div class="box-header with-border">
                        <h3 class="box-title">แบนเนอร์สินค้า</h3>
                        <center><button type="button" name="button" class="btn btn-success" data-toggle="modal"
                                data-target="#main-category">เพิ่มรูปภาพแบนเนอร์</button></center>
                        <div class="box-tools pull-right">
                            <button type="button" class="btn btn-box-tool" data-widget="collapse"><i
                                    class="fa fa-minus"></i></button>
                            <button type="button" class="btn btn-box-tool" data-widget="remove"><i
                                    class="fa fa-remove"></i></button>
                        </div>
                    </div>
                    <!-- /.box-header -->
                    <div class="box-body">
                        <div class="col-md-8 col-md-offset-2">
                            <table id="example1" class="table table-bordered table-striped text-center">
                                <thead>
                                    <tr>
                                        <th>No.</th>
                                        <th>รุปภาพแบนเนอร์</th>
                                        <th>Manage</th>
                                    </tr>
                                </thead>

                                <tbody>
                                    @foreach($banners as $key=>$value)
                                    <tr id="miancategory{{ $value->id }}">
                                        <td>{{ $key+1}}.</td>
                                        <td><img src="{{ URL::asset($value->picture) }}" width="250px" alt=""></td>
                                        <td>
                                            <button style="margin-top:10px;" type="button" class="btn  btn-info"
                                                data-toggle="modal" data-target="#main-category{{$value->id}}"> <span
                                                    class="glyphicon  glyphicon-wrench"></span> </button>
                                            <button style="margin-top:10px;" maincategoryID="{{ $value->id }}"
                                                class="btn btn-danger del_main"> <span
                                                    class="glyphicon glyphicon-trash"></span> </button>
                                        </td>
                                    </tr>
                                    @endforeach
                                </tbody>

                            </table>
                        </div>
                    </div>
                    <!-- /.box-body -->
                </div>
                <!-- /.box -->
            </div>

        </div>
    </section>
</div>



<div class="modal fade" id="main-category">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">เพิ่มรูปแบนเนอร์</h4>
            </div>

            <form class="" action="/admin/addbanner" method="post" enctype="multipart/form-data">
                @csrf
                <div class="modal-body">
                    <div class="form-group">
                        <label class=" control-label">รูปภาพ</label>
                        <div class="file-upload">
                            <div class="file-select">
                                <div class="file-select-button" id="fileName">Choose File</div>
                                <div class="file-select-name" id="noFile">No file chosen...</div>
                                <input type="file" name="chooseFile" id="chooseFile" require>
                            </div>
                        </div>
                        <!-- <span class="text-red">* Image size 1920</span> -->

                        <img class="img-responsive" id="blah" src="" />

                    </div><!-- class="form-group" -->

                    <div class="form-group">
                        <label>เลือกสินค้าที่ต้องการลิ้งค์</label>
                        <select class="form-control select2" style="width: 100%;" name="product_id">
                            <option selected="selected" value="0">เลือกสินค้า</option>
                            @foreach($products as $product)
                            <option value="{{$product->id}}">{{$product->pd_name_th}}</option>
                            @endforeach
                        </select>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-primary">Save changes</button>
                </div>
            </form>

        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>
<!-- /.modal -->

@foreach($banners as $value)
<div class="modal fade" id="main-category{{ $value->id }}">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">แก้ไขรูปแบนเนอร์</h4>
            </div>

            <form class="" action="/admin/updatebanner" method="post" enctype="multipart/form-data">
                @csrf
                <div class="modal-body">
                    <div class="form-group">
                        <label class=" control-label">รูปภาพหัวข้อ</label>
                        <div class="file-upload file-uploadEdit">
                            <div class="file-select">
                                <div class="file-select-button fileNameold" id="fileNameold">Choose File</div>
                                <div class="file-select-name noFileold" id="noFileold">No file chosen...</div>
                                <input type="file" name="chooseFileOld" id="chooseFileEdit" class="chooseFileEdit"
                                    {{ !empty($value->picture) ? "" : "required"  }}>
                            </div>
                        </div>
                        <!-- <span class="text-red">* Image size 1920</span> -->

                        <img class="img-responsive blahold" id="blahold" src="" />
                        @if(!empty($value->picture))
                        <img class="img-responsive imgOld" src="{{ URL::asset($value->picture)}}" />
                        <input type="hidden" name="imageold" value="{{$value->picture}}">
                        @endif
                    </div><!-- class="form-group" -->
                    <div class="form-group">
                        <label>เลือกสินค้าที่ต้องการลิ้งค์</label>
                        <select class="form-control select2" style="width: 100%;" name="product_id">

                            @if($value->product_id == "0" || $value->product_id == "")
                            <option selected="selected">เลือกสินค้า</option>
                            @endif

                            @foreach($products as $product)
                            <option value="{{$product->id}}" {{ $product->id == $value->product_id ? "selected" : "" }}>
                                {{$product->pd_name_th}}
                            </option>
                            @endforeach
                        </select>
                    </div>
                </div>
                <div class="modal-footer">
                    <input type="hidden" name="id" value="{!!$value->id!!}" id="">

                    <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-primary">Save changes</button>
                </div>
            </form>

        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>
<!-- /.modal -->
@endforeach

@endsection




@section('script')
<link rel="stylesheet" href="{{ URL::asset('admin_backend/dist/css/page.css')}}">


<!-- DataTables -->
<link rel="stylesheet"
    href="{{ URL::asset('admin_backend/bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css')}}">

<!-- DataTables -->
<script src="{{ URL::asset('admin_backend/bower_components/datatables.net/js/jquery.dataTables.min.js')}}"></script>
<script src="{{ URL::asset('admin_backend/bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js')}}">
</script>

<script type="text/javascript">
var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');

$(function() {
    $('#example1').DataTable()
    $('#example2').DataTable()
    $('.select2').select2()

});


$(".del_main").click(function(e) {
    var id = $(this).attr("maincategoryID");
    if (confirm("คุณต้องการลบรุปแบนเนอร์ ?")) {
        //alert(id);
        $.ajax({
            type: "POST",
            url: "/admin/deletebanner",
            data: {
                _token: CSRF_TOKEN,
                id: id
            },
            dataType: 'JSON',
            success: function(data) {
                //console.log(data['data']);
                if (data['status'] == "success")
                    location.reload();
            }
        });
    } else {
        e.preventDefault();
    }
});
</script>


<script type="text/javascript">
function readURL(input) {

    if (input.files && input.files[0]) {
        var reader = new FileReader();

        reader.onload = function(e) {
            $('#blah').attr('src', e.target.result);
        }
        reader.readAsDataURL(input.files[0]);
    }
}
$("#chooseFile").change(function() {
    readURL(this);
});
$('#chooseFile').bind('change', function() {
    var filename = $("#chooseFile").val();
    if (/^\s*$/.test(filename)) {
        $(".file-upload").removeClass('active');
        $("#noFile").text("No file chosen...");
    } else {
        $(".file-upload").addClass('active');
        $("#noFile").text(filename.replace("C:\\fakepath\\", ""));
    }
});
</script>

<script type="text/javascript">
function readURLEdit(input) {
    if (input.files && input.files[0]) {
        var reader = new FileReader();
        reader.onload = function(e) {
            $('.imgOld').hide();
            $('.blahold').attr('src', e.target.result);
        }
        reader.readAsDataURL(input.files[0]);
    }
}
$(".chooseFileEdit").change(function() {
    readURLEdit(this);
});
$('.chooseFileEdit').bind('change', function() {
    var filename = $("#chooseFileEdit").val();
    if (/^\s*$/.test(filename)) {
        $(".file-uploadEdit").removeClass('active');
        $(".noFileold").text("No file chosen...");
    } else {
        $(".file-uploadEdit").addClass('active');
        $(".noFileold").text(filename.replace("C:\\fakepath\\", ""));
    }
});
</script>
@endsection