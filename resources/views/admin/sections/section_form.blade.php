<div class="box-body">
    <!-- /.box-header -->
    <div class="form-group">
        <div class="input-group">
            <div class="input-group-btn">
                <button type="button" class="btn btn-info input-lg">ชื่อของบทเรียน</button>
            </div>
            <!-- /btn-group -->
            <input type="text" name="sec_title" class="form-control input-lg"
                value="{{ !empty($EditSection->sec_title) ? $EditSection->sec_title : ""  }}">
        </div>
    </div>


    <!-- <div class="form-group">
        <hr>
        <p><label>เลือกการแสดงของวิดีโอ: </label> </p>
        <label>
            <input type="radio" name="sec_video_platform" value="0" class="flat-red platform_video"
                <?php echo (!empty($EditSection->sec_video_platform) ? ($EditSection->sec_video_platform == 0 ? "checked" : "") : "checked") ?>>
            Vimeo
        </label>
        <label>
            <input type="radio" name="sec_video_platform" value="1" class="flat-red platform_video"
                <?php echo (!empty($EditSection->sec_video_platform) ? ($EditSection->sec_video_platform == 1 ? "checked" : "") : "") ?>>
            Youtube
        </label>
    </div> -->
    <div class="form-group">
        <hr>
        <p><label>เลือกสถานะการแสดง: </label> </p>
        <label>
            <input type="radio" name="sec_status" value="0" class="flat-red platform_video"
                <?php echo (!empty($EditSection->sec_status) ? ($EditSection->sec_status == 0 ? "checked" : "") : "checked") ?>>
            แสดง
        </label>
        <label>
            <input type="radio" name="sec_status" value="1" class="flat-red platform_video"
                <?php echo (!empty($EditSection->sec_status) ? ($EditSection->sec_status == 1 ? "checked" : "") : "") ?>>
            ไม่แสดง
        </label>
    </div>
</div>
<!-- /.box-body -->
<div class="box-footer text-center">
    <input type="hidden" name="sec_product_id" value="{{ $sec_product_id }}">
    <Button type="submit" class="btn btn-info btn-lg ">สร้างบทเรียน </Button>
</div>