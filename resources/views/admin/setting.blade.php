@extends('admin.layout.main_admin')


@section('content')
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">


    <!-- Main content -->
    <section class="content">

        @if(session('msg'))
        <div class="alert alert-success">
            <h4>{{ session('msg')}}</h4>
        </div>
        @endif


        <!-- general form elements -->
        <div class="box box-info">

            <div class="box-header with-border">
                <h3 class="box-title">ตั้งค่าเว็บไซต์
                </h3>
                <!-- tools box -->
                <div class="pull-right box-tools">
                    <button type="button" class="btn btn-info btn-sm" data-widget="collapse" data-toggle="tooltip"
                        title="" data-original-title="Collapse">
                        <i class="fa fa-minus"></i></button>
                    <button type="button" class="btn btn-info btn-sm" data-widget="remove" data-toggle="tooltip"
                        title="" data-original-title="Remove">
                        <i class="fa fa-times"></i></button>
                </div>
                <!-- /. tools -->
            </div>
            <!-- /.box-header -->
            <!-- form start -->

            <div class="box-body pad">
                <form role="form" action="/admin/setting/update" method="post" enctype="multipart/form-data">
                    @csrf
                    @method('PUT')
                    <div class="col-md-6">
                        <div class="box box-info box-solid">
                            <div class="box-header with-border">
                                <h3 class="box-title">ที่อยู่สำหรับติดต่อ</h3>

                                <div class="box-tools pull-right">
                                    <button type="button" class="btn btn-box-tool" data-widget="collapse"><i
                                            class="fa fa-minus"></i>
                                    </button>
                                </div>
                                <!-- /.box-tools -->
                            </div>
                            <!-- /.box-header -->
                            <div class="box-body" style="">
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label class=" control-label nopadding">ชื่อบริษัท</label>
                                        <div class=" nopadding">
                                            <textarea class="form-control" name="name_office"
                                                rows="2">{!! (!empty($setting->name_office) ? $setting->name_office : "") !!}</textarea>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label class=" control-label nopadding">ที่อยู่</label>
                                        <div class=" nopadding">
                                            <textarea class="form-control" name="address"
                                                rows="3">{!! (!empty($setting->address) ? $setting->address : "") !!}</textarea>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="exampleInputEmail1">เบอโทรศัพท์ : </label>
                                        <input type="text" class="form-control" id="exampleInputEmail1" name="tel"
                                            value="{!! (!empty($setting->tel) ? $setting->tel : "") !!}">
                                    </div>
                                </div>

                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="exampleInputEmail1">FAX : </label>
                                        <input type="text" class="form-control" id="exampleInputEmail1" name="fax"
                                            value="{!! (!empty($setting->fax) ? $setting->fax : "") !!}">
                                    </div>
                                </div>

                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="exampleInputEmail1">Facebook Page : </label>
                                        <input type="text" class="form-control" id="exampleInputEmail1"
                                            name="facebook_page"
                                            value="{!! (!empty($setting->facebook_page) ? $setting->facebook_page : "") !!}">
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="exampleInputEmail1">Intragram: </label>
                                        <input type="text" class="form-control" id="exampleInputEmail1" name="ig_page"
                                            value="{!! (!empty($setting->ig_page) ? $setting->ig_page : "") !!}">
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="exampleInputEmail1">Lind id : </label>
                                        <input type="text" class="form-control" id="exampleInputEmail1" name="line_id"
                                            value="{!! (!empty($setting->line_id) ? $setting->line_id : "") !!}">
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="exampleInputEmail1">Email : </label>
                                        <input type="text" class="form-control" id="" name="email"
                                            value="{!! (!empty($setting->email) ? $setting->email : "") !!}">
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="exampleInputEmail1">Website : </label>
                                        <input type="text" class="form-control" id="" name="website"
                                            value="{!! (!empty($setting->website) ? $setting->website : "") !!}">
                                    </div>
                                </div>
                            </div>
                            <!-- /.box-body -->
                        </div>

                        <div class="box box-info box-solid">
                            <div class="box-header with-border">
                                <h3 class="box-title">ตั้งค่าสำหรับรายละเอียดเว็บ</h3>

                                <div class="box-tools pull-right">
                                    <button type="button" class="btn btn-box-tool" data-widget="collapse"><i
                                            class="fa fa-minus"></i>
                                    </button>
                                </div>
                                <!-- /.box-tools -->
                            </div>
                            <!-- /.box-header -->
                            <div class="box-body" style="">
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label for="exampleInputEmail1">Title Page : </label>
                                        <textarea class="form-control" name="title"
                                            rows="3">{!! (!empty($setting->title) ? $setting->title : "") !!}</textarea>
                                    </div>
                                </div>

                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label for="exampleInputEmail1">Keyword Page: </label>
                                        <textarea class="form-control" name="keyword"
                                            rows="3">{!! (!empty($setting->keyword) ? $setting->keyword : "") !!}</textarea>
                                    </div>
                                </div>

                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label for="exampleInputEmail1">Description Page: </label>
                                        <textarea class="form-control" name="description"
                                            rows="3">{!! (!empty($setting->description) ? $setting->description : "") !!}</textarea>
                                    </div>
                                </div>
                            </div>
                            <!-- /.box-body -->
                        </div>
                    </div><!-- /.md-6 -->
                    <!-- /.tab left -->

                    <!-- /.tab right -->
                    <div class="col-md-6">
                        <div class="box box-info box-solid">
                            <div class="box-header with-border">
                                <h3 class="box-title">ตั้งค่าข้อความ</h3>

                                <div class="box-tools pull-right">
                                    <button type="button" class="btn btn-box-tool" data-widget="collapse"><i
                                            class="fa fa-minus"></i>
                                    </button>
                                </div>
                                <!-- /.box-tools -->
                            </div>
                            <!-- /.box-header -->
                            <div class="box-body" style="">
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label for="tabheader_text">Header ของเว็บ : </label>
                                        <textarea class="form-control" name="tabheader_text"
                                            rows="3">{!! (!empty($setting->tabheader_text) ? $setting->tabheader_text : "") !!}</textarea>
                                    </div>
                                </div>
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label for="text_footer_left">รายละเอียด Footer ด้านซ้าย : </label>
                                        <textarea class="form-control" name="text_footer_left"
                                            rows="3">{!! (!empty($setting->text_footer_left) ? $setting->text_footer_left : "") !!}</textarea>
                                    </div>
                                </div>
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label for="exampleInputEmail1">รายละเอียด Footer ด้านขวา : </label>
                                        <textarea class="form-control" name="text_footer_right"
                                            rows="3">{!! (!empty($setting->text_footer_right) ? $setting->text_footer_right : "") !!}</textarea>
                                    </div>
                                </div>
                            </div>
                            <!-- /.box-body -->
                        </div>

                        <div class="box box-info box-solid">
                            <div class="box-header with-border">
                                <h3 class="box-title">ตั้งค่าสำหรับเก็บสถิติ</h3>

                                <div class="box-tools pull-right">
                                    <button type="button" class="btn btn-box-tool" data-widget="collapse"><i
                                            class="fa fa-minus"></i>
                                    </button>
                                </div>
                                <!-- /.box-tools -->
                            </div>
                            <!-- /.box-header -->
                            <div class="box-body" style="">
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label for="exampleInputEmail1">Facebook pixels : </label>
                                        <textarea class="form-control" name="facebook_pixel"
                                            rows="3">{!! (!empty($setting->facebook_pixel) ? $setting->facebook_pixel : "") !!}</textarea>
                                    </div>
                                </div>
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label for="exampleInputEmail1">Google analytics : </label>
                                        <textarea class="form-control" name="google_analytics"
                                            rows="3">{!! (!empty($setting->google_analytics) ? $setting->google_analytics : "") !!}</textarea>
                                    </div>
                                </div>
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label for="exampleInputEmail1">Hitstat : </label>
                                        <textarea class="form-control" name="hitstat"
                                            rows="3">{!! (!empty($setting->hitstat) ? $setting->hitstat : "") !!}</textarea>
                                    </div>
                                </div>

                            </div>
                        </div>
                        <!-- /.box-body -->

                        <div class="box box-info box-solid">
                            <div class="box-header with-border">
                                <h3 class="box-title">ตั้งค่าสำหรับรูป</h3>

                                <div class="box-tools pull-right">
                                    <button type="button" class="btn btn-box-tool" data-widget="collapse"><i
                                            class="fa fa-minus"></i>
                                    </button>
                                </div>
                                <!-- /.box-tools -->
                            </div>
                            <!-- /.box-header -->
                            <div class="box-body" style="">
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label class=" control-label">รูปภาพ Logo</label>
                                        <div class="file-upload">
                                            <div class="file-select">
                                                <div class="file-select-button" id="fileName">Choose File</div>
                                                <div class="file-select-name" id="noFile">No file chosen...</div>
                                                <input type="file" name="chooseFilelogo" id="chooseFilelogo" rquired>
                                            </div>
                                        </div>
                                        <img class="img-responsive" id="blah" src="" />
                                        @if(!empty($setting->logo))
                                        <img class="img-responsive chooseFilelogo_old"
                                            src="{{ URL::asset($setting->logo)}}" />
                                        <input type="hidden" name="imageold_chooseFilelogo" value="{{$setting->logo}}">
                                        @endif
                                    </div><!-- class="form-group" -->
                                </div>

                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label class=" control-label">รูปภาพ Favicon</label>
                                        <div class="file-upload file-favicon">
                                            <div class="file-select ">
                                                <div class="file-select-button" id="favicon">Choose File</div>
                                                <div class="file-select-name" id="noFilefavicon">No file chosen...</div>
                                                <input type="file" name="chooseFilefavicon" id="chooseFilefavicon"
                                                    rquired>
                                            </div>
                                        </div>
                                        <img class="img-responsive" id="blahfavicon" src="" />
                                        @if(!empty($setting->favicon))
                                        <img class="img-responsive chooseFilefavicon_old"
                                            src="{{ URL::asset($setting->favicon)}}" />
                                        <input type="hidden" name="imageold_chooseFilefavicon"
                                            value="{{$setting->favicon}}">
                                        @endif
                                    </div><!-- class="form-group" -->
                                </div>


                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label class=" control-label">รูปภาพ Line Qrcode</label>
                                        <div class="file-upload file-lineqrcode">
                                            <div class="file-select">
                                                <div class="file-select-button" id="fileNameMb">Choose File</div>
                                                <div class="file-select-name" id="noFilelineqrcode">No file chosen...
                                                </div>
                                                <input type="file" name="lineqrcode" id="lineqrcode">
                                            </div>
                                        </div>
                                        <img class="img-responsive" id="blahMb" src="" />
                                        @if(!empty($setting->line_qrcode))
                                        <img class="img-responsive lineqrcode_old"
                                            src="{{ URL::asset($setting->line_qrcode)}}" />
                                        <input type="hidden" name="imageold_lineqrcode"
                                            value="{{$setting->line_qrcode}}">
                                        @endif
                                    </div><!-- class="form-group" -->
                                </div>
                            </div>
                        </div>
                        <!-- /.box-body -->
                    </div> <!-- /.md-6 -->
                    <!-- /.tab right -->


            </div>

            <div class="box-footer text-center">
                @if(!empty($setting->id))
                <input type="hidden" name="id" value="{{$setting->id}}">
                @endif
                <button type="submit" class="btn btn-primary btn-lg"><span class="glyphicon glyphicon-save"></span>
                    บันทึก</button>
            </div>
            </form>

        </div>

    </section>
    <!-- /.content -->
</div>
<!-- /.content-wrapper -->

@endsection




@section('script')
<link rel="stylesheet" href="{{ URL::asset('admin_backend/dist/css/page.css')}}">

<script type="text/javascript">
function readURL(input) {

    if (input.files && input.files[0]) {
        var reader = new FileReader();

        reader.onload = function(e) {
            $('.chooseFilelogo_old').hide();
            $('#blah').attr('src', e.target.result);
        }
        reader.readAsDataURL(input.files[0]);
    }
}
$("#chooseFilelogo").change(function() {
    readURL(this);
});
$('#chooseFilelogo').bind('change', function() {
    var filename = $("#chooseFilelogo").val();
    if (/^\s*$/.test(filename)) {
        $(".file-upload").removeClass('active');
        $("#noFile").text("No file chosen...");
    } else {
        $(".file-upload").addClass('active');
        $("#noFile").text(filename.replace("C:\\fakepath\\", ""));
    }
});
</script>
<script type="text/javascript">
function readURLfavicon(input) {

    if (input.files && input.files[0]) {
        var reader = new FileReader();

        reader.onload = function(e) {
            $('.chooseFilefavicon_old').hide();
            $('#blahfavicon').attr('src', e.target.result);
        }
        reader.readAsDataURL(input.files[0]);
    }
}
$("#chooseFilefavicon").change(function() {
    readURLfavicon(this);
});
$('#chooseFilefavicon').bind('change', function() {
    var filename = $("#chooseFilefavicon").val();
    if (/^\s*$/.test(filename)) {
        $(".file-favicon").removeClass('active');
        $("#noFilefavicon").text("No file chosen...");
    } else {
        $(".file-favicon").addClass('active');
        $("#noFilefavicon").text(filename.replace("C:\\fakepath\\", ""));
    }
});
</script>

<script type="text/javascript">
function readURLMb(input) {

    if (input.files && input.files[0]) {
        var reader = new FileReader();

        reader.onload = function(e) {
            $('.lineqrcode_old').hide();

            $('#blahMb').attr('src', e.target.result);
        }
        reader.readAsDataURL(input.files[0]);
    }
}
$("#lineqrcode").change(function() {
    readURLMb(this);
});
$('#lineqrcode').bind('change', function() {
    var filename = $("#lineqrcode").val();
    if (/^\s*$/.test(filename)) {
        $(".file-lineqrcode").removeClass('active');
        $("#noFilelineqrcode").text("No file chosen...");
    } else {
        $(".file-lineqrcode").addClass('active');
        $("#noFilelineqrcode").text(filename.replace("C:\\fakepath\\", ""));
    }
});
</script>

@endsection