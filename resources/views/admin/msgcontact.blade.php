@extends('admin.layout.main_admin')



@section('content')
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        @if (session('msg'))
        <div class="alert alert-success">
            {{ session('msg') }}
        </div>
        @endif

    </section>

    <section class="content">
        <div class="row">

            <div class="col-xs-12">

                <div class="box  box-info  box-solid">
                    <div class="box-header with-border">
                        <h3 class="box-title">ข้อมูลการติดต่อจากผู้ใช้งาน</h3>
                        <div class="box-tools pull-right">
                            <button type="button" class="btn btn-box-tool" data-widget="collapse"><i
                                    class="fa fa-minus"></i></button>

                        </div>
                    </div>
                    <div class="box-body">
                        <table id="example1" class="table table-bordered table-striped">
                            <thead>
                                <tr>
                                    <th>No.</th>
                                    <th>ชื่อ-นามสุกล</th>
                                    <th>อีเมล</th>
                                    <th>เบอร์โทร</th>
                                    <th>หัวข้อ</th>
                                    <th>Manage</th>
                                </tr>
                            </thead>
                            <tbody>

                                @foreach($all_msg as $key => $value)
                                <tr id="knowledge{{ $value->id }}">
                                    <td>{{ $key+1 }}.</td>
                                    <td>
                                        {{ $value->name }}
                                    </td>
                                    <td>{{ $value->email }}</td>
                                    <td>{{ $value->tel }}</td>
                                    <td>{{ $value->title }}</td>
                                    <td>
                                        <button style="margin-top:10px;" type="button" class="btn  btn-info"
                                            data-toggle="modal" data-target="#see_contact{{ $value->id }}"> <span
                                                class="glyphicon glyphicon-zoom-in"></span> </button>
                                        <button style="margin-top:10px;" value_id="{{ $value->id }}"
                                            class="btn btn-danger del_file"> <span
                                                class="glyphicon glyphicon-trash"></span> </button>
                                    </td>
                                </tr>
                                @endforeach

                            </tbody>
                        </table>
                    </div>
                    <!-- /.box-body -->
                </div>
                <!-- /.box -->
            </div>
        </div>
    </section>

</div>
<!-- /.content-wrapper -->
@foreach($all_msg as $key => $value)

<!-- Modal -->
<div class="modal fade" id="see_contact{{ $value->id }}" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
    aria-hidden="true">
    <div class="modal-dialog  modal-lg" role="document">
        <div class="modal-content">
            <form action="/admin/replycontact" method="post">
                @csrf
                <div class="modal-header">
                    <h3 class="modal-title" id="exampleModalLabel">ชื่อผู้ติดต่อ : {{$value->name}}</h3s>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                </div>
                <div class="modal-body">
                    <dl class="dl-horizontal">
                        <dt>ชื่อ : </dt>
                        <dd>{{ $value->name }}</dd>

                        <dt>อีเมล :</dt>
                        <dd>{{ $value->email }}</dd>

                        <dt>โทรศัพท์ :</dt>
                        <dd>{{ $value->tel }}</dd>

                        <dt>หัวข้อ :</dt>
                        <dd>{{ $value->title }}</dd>

                        <dt>รายละเอียด :</dt>
                        <dd>{{ $value->description }}</dd>
                    </dl>
                </div>
                <hr>
                <div class="modal-body">
                    <div class="row">
                        <div class="form-group">
                            <label for="" class="col-sm-2 control-label">ตอบกลับข้อความ</label>
                            <div class="col-sm-10">
                                <textarea type="text" class="form-control" name="replycontact" placeholder="ตอบกลับ"
                                    rows="5" required></textarea>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <input type="hidden" name="id" value="{{$value->id}}">
                    <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-primary">Reply</button>
                </div>
            </form>
        </div>
    </div>
</div>
@endforeach

@endsection




@section('script')
<style media="screen">
dd,
dt {
    font-size: 18px;
}

dt {
    padding-bottom: 15px;
}
</style>
<!-- DataTables -->
<link rel="stylesheet"
    href="{{ URL::asset('admin_backend/bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css')}}">
<!-- DataTables -->
<script src="{{ URL::asset('admin_backend/bower_components/datatables.net/js/jquery.dataTables.min.js')}}"></script>
<script src="{{ URL::asset('admin_backend/bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js')}}">
</script>

<script type="text/javascript">
var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');

$(function() {
    $('#example1').DataTable()
});
$('.del_file').on('click', function() {
    if (confirm('คุณต้องการลบการติดต่อนี้หรือไม่')) {
        var id = $(this).attr('value_id')
        console.log(id);
        $.ajax({
            type: "POST",
            url: '/admin/contact/delete',
            data: {
                _token: CSRF_TOKEN,
                id: id
            },
            dataType: 'JSON',
            success: function(data) {
                //console.log(data['data']);
                if (data['status'] == "success")
                    $("#knowledge" + id).remove();
            }

        });
    }
});
</script>



@endsection