<div class="box-body">
    <div class="form-group">
        <div class="input-group">
            <div class="input-group-btn">
                <button type="button" class="btn btn-info input-lg">ชื่อบทเรียน</button>
            </div>
            <!-- /btn-group -->
            <input type="text" name="lecture_title" class="form-control input-lg"
                value="{{!empty($EditLecture->lecture_title) ? $EditLecture->lecture_title : ""  }}" required>
        </div>
    </div>
    <!-- /.box-header -->
    <div class="form-group">
        <div class="input-group">
            <div class="input-group-btn">
                <button type="button" class="btn btn-info input-lg">Code Video</button>
            </div>
            <!-- /btn-group -->
            <input type="text" name="lecture_video_link" class="form-control input-lg"
                value="{{!empty($EditLecture->lecture_video_link) ? $EditLecture->lecture_video_link : ""  }}">
        </div>
        <span class="text-danger has-error">ใส่เป็นรหัสของวิดีโอเช่น abs12344</span>
    </div>

    <div class="form-group">
        <hr>
        <p><label>ชมฟรี: </label> </p>
        <label>
            <input type="radio" name="lecture_free_video" value="0" class="flat-red platform_video"
                <?php echo (!empty($EditLecture->lecture_free_video) ? ($EditLecture->lecture_free_video == 0 ? "checked" : "") : "checked") ?>>
            ไม่ฟรี
        </label>
        <label>
            <input type="radio" name="lecture_free_video" value="1" class="flat-red platform_video"
                <?php echo (!empty($EditLecture->lecture_free_video) ? ($EditLecture->lecture_free_video == 1 ? "checked" : "") : "") ?>>
            ฟรี
        </label>
    </div>


    <!-- <div class="form-group dynamic">
        <div class="row entry">
            <div class="col-lg-5">
                <label for="exampleInputEmail1">หัวข้อ </label>
                <div class="" style="">
                    <input type="text" class="form-control" name="title[]">

                </div>
            </div>
            <div class="col-lg-5">
                <label for="exampleInputEmail1">File</label>
                <div class="" style="">
                    <input type="text" class="form-control" name="file[]">
                </div>
            </div>
            <span class="input-group-btn">
                <button type="button" class="btn btn-info btn-flat btn-add" data-toggle="tooltip" data-placement="right"
                    title="เพิ่ม Tags"><span class="glyphicon glyphicon-plus"></span></button>
            </span>
        </div>
    </div> -->

</div>
<!-- /.box-body -->
<div class="box-footer text-center">
    <input type="hidden" name="lecture_section_id" value="{{ $lecture_section_id }}">
    <Button type="submit" class="btn btn-info btn-lg ">สร้างวิดีโอบทเรียน </Button>
</div>