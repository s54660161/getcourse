@extends('admin.layout.main_admin')



@section('content')
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        @if (session('msg'))
        <div class="alert alert-success">
            {{ session('msg') }}
        </div>
        @endif

    </section>

    <section class="content">
        <form role="form" action="{{ route('admin.lecture.store')}}" method="post">
            @csrf
            <div class="box box-info">
                <div class="box-header with-border">
                    <h3 class="box-title">สร้างวิดีโอบทเรียน</h3>

                    <div class="box-tools pull-right">
                        <button type="button" class="btn btn-box-tool" data-widget="collapse"><i
                                class="fa fa-minus"></i></button>
                        <button type="button" class="btn btn-box-tool" data-widget="remove"><i
                                class="fa fa-remove"></i></button>
                    </div>
                </div>

                @include('admin.lectures.lecture_form')
            </div>

        </form>

    </section>
</div>


@endsection

@section('script')
<link rel="stylesheet" href="{{URL::asset('admin_backend/plugins/iCheck/all.css')}}">

<!-- iCheck 1.0.1 -->
<script src="{{URL::asset('admin_backend/plugins/iCheck/icheck.min.js')}}"></script>

<script>
$('input[type="checkbox"].flat-red, input[type="radio"].flat-red').iCheck({
    //  checkboxClass: 'icheckbox_flat-green ',
    radioClass: 'iradio_flat-green ',
})
$(function() {
    $(document).on('click', '.btn-add', function(e) {
        e.preventDefault();

        var controlForm = $('.dynamic'),
            currentEntry = $(this).parents('.entry:first'),
            newEntry = $(currentEntry.clone()).appendTo(controlForm);

        newEntry.find('input').val('');
        controlForm.find('.entry:not(:last) .btn-add')
            .removeClass('btn-add').addClass('btn-remove')
            .removeClass('btn-success').addClass('btn-danger')
            .html('<span class="glyphicon glyphicon-minus"></span>');
    }).on('click', '.btn-remove', function(e) {
        $(this).parents('.entry:first').remove();

        e.preventDefault();
        return false;
    });
});
</script>

@endsection