@extends('admin.layout.main_admin')


@section('content')

<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <section class="content">
        <div class="row">
            <div class="col-md-12">
                <!-- SELECT2 EXAMPLE -->
                @if (session('msg-category'))
                <div class="alert alert-success">
                    {{ session('msg-category') }}
                </div>
                @endif
                <div class="box box-info box-solid">
                    <div class="box-header with-border">
                        <h3 class="box-title">หมวดหมู่คอร์สทั้งหมด</h3>
                        <button type="button" name="button" class="btn btn-lg bg-orange   pull-right"
                            data-toggle="modal" data-target="#category">
                            <i class="fa fa-fw fa-plus"></i> เพิ่มหมวดหมู่คอร์ส
                        </button>

                    </div>
                    <!-- /.box-header -->
                    <div class="box-body">
                        <table id="example1" class="table table-bordered table-striped">
                            <thead>
                                <tr>
                                    <th>No.</th>
                                    <th>หมวดหมู่หลัก</th>
                                    <th>Manage</th>
                                </tr>
                            </thead>

                            <tbody>
                                @foreach($category as $key=>$main)
                                <tr id="miancategory{{ $main->id }}">
                                    <td>{{ $key+1}}.</td>
                                    <td>{{ $main->category_name }}</td>
                                    <td>
                                        <button style="margin-top:10px;" type="button" class="btn  btn-info"
                                            data-toggle="modal" data-target="#edit-category{{$main->id}}"> <span
                                                class="glyphicon  glyphicon-wrench"></span> </button>
                                        <button style="margin-top:10px;" categoryID="{{ $main->id }}"
                                            class="btn btn-danger del_category"> <span
                                                class="glyphicon glyphicon-trash"></span> </button>
                                    </td>
                                </tr>
                                @endforeach
                            </tbody>

                        </table>
                    </div>
                    <!-- /.box-body -->
                </div>
                <!-- /.box -->
            </div>


        </div>
    </section>
</div>


<!-- **** Category **** -->
<div class="modal fade" id="category">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">หมวดหมู่หลัก</h4>
            </div>

            <form class="" action="/admin/create-categoryproduct" method="post">
                @csrf
                <div class="modal-body">
                    <div class="form-group">
                        <label class="control-label">ชื่อหมวดหมู่หลัก</label>
                        <input type="text" name="category_name" class="form-control" value="" required>
                    </div><!-- class="form-group" -->
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-primary">Save changes</button>
                </div>
            </form>

        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>

@foreach($category as $key=>$main)
<!-- /.modal -->
<div class="modal fade" id="edit-category{{ $main->id }}">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">แก้ไขหมวดหมู่ {{$main->category_name}}</h4>
            </div>

            <form class="" action="/admin/edit-categoryproduct" method="post">
                @csrf
                <div class="modal-body">
                    <div class="form-group">
                        <label class="control-label">ชื่อหมวดหมู่หลัก</label>
                        <input type="text" name="category_name" class="form-control" value="{{$main->category_name}}"
                            required>
                    </div><!-- class="form-group" -->
                </div>
                <div class="modal-footer">
                    <input type="hidden" name="id" value="{{ $main->id }}">

                    <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-primary">Save changes</button>
                </div>
            </form>

        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>
<!-- /.modal -->
@endforeach


@endsection




@section('script')
<!-- DataTables -->
<link rel="stylesheet"
    href="{{ URL::asset('admin_backend/bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css')}}">

<!-- DataTables -->
<script src="{{ URL::asset('admin_backend/bower_components/datatables.net/js/jquery.dataTables.min.js')}}"></script>
<script src="{{ URL::asset('admin_backend/bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js')}}">
</script>

<script type="text/javascript">
var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');

$(function() {
    $('#example1').DataTable()
    $('#example2').DataTable()
});


$(".del_category").click(function(e) {
    var id = $(this).attr("categoryID");
    if (confirm(
            "ถ้าคุณลบหมวดหมู่หลัก หมวดหมู่ย่อยที่อยู่ในหมวดหมู่หลักจะถูกลบด้วย คุณต้องการลบหมวดหมู่หลักหรือไม่ ?"
        )) {
        //alert(img_id);
        $.ajax({
            type: "POST",
            url: "/admin/delete-categoryproduct",
            data: {
                _token: CSRF_TOKEN,
                id: id
            },
            dataType: 'JSON',
            success: function(data) {
                //console.log(data['data']);
                if (data['status'] == "success")
                    location.reload();
            }
        });
    } else {
        e.preventDefault();
    }
});


$(".del_subcategory").click(function(e) {
    var id = $(this).attr("subcategoryID");
    if (confirm("คุณต้องการลบหมวดหมู่ย่อย ?")) {
        //alert(img_id);
        $.ajax({
            type: "POST",
            url: "/admin/delete-subcategoryproduct",
            data: {
                _token: CSRF_TOKEN,
                id: id
            },
            dataType: 'JSON',
            success: function(data) {
                //console.log(data['data']);
                if (data['status'] == "success")
                    location.reload();
            }
        });
    } else {
        e.preventDefault();
    }
});
</script>
@endsection