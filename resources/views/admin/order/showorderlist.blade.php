@extends('admin.layout.main_admin')

@section('content')
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            Order
            <small>{{ $orderlist->order_id }}</small>
        </h1>
    </section>



    <!-- Main content -->
    <section class="invoice">
        <!-- title row -->
        <div class="row">
            <div class="col-xs-12">
                <h2 class="page-header">
                    <i class="fa fa-globe"></i> ออเดอร์การสั่งสินค้า
                </h2>
            </div>
            <!-- /.col -->
        </div>
        <!-- info row -->
        <div class="row invoice-info">
            <div class="col-sm-4 invoice-col">
                <h4>ผู้สั่งสินค้า</h4>
                <address>
                    <strong>{{ $users_account->name }}</strong><br>
                    โทรศัพท์: {{ $users_account->tel  }}<br>
                    อีเมล : {{ $users_account->email  }}
                </address>
            </div>
            <!-- /.col -->

            <!-- /.col -->
            <div class="col-sm-4 invoice-col">
                <h3><b>รหัสสินค้า {{$orderlist->order_id}}</b></h3>
                <b>วันที่การสั่งสินค้า:</b> {{$orderlist->created_at}}<br>
                <b>สถานะการจ่ายเงิน :</b>
                {{($orderlist->order_status === "waiting" ? "รอการยืนยันการชำระเงิน" : ($orderlist->order_status == "pending" ? "รอการยืนยันจากเจ้าหน้าที่"  : "ชำระเงินเรียบร้อย "))}}
                @if($orderlist->order_status === "success")
                <br><b>ชำระเงินผ่านช่อง :</b> <span class="text-success text-bold">{{$orderlist->payment_method}}</span>
                @endif
            </div>
            <!-- /.col -->
        </div>
        <!-- /.row -->

        <!-- Table row -->
        <div class="row">
            <div class="col-xs-12 table-responsive">
                <table class="table table-striped">
                    <thead>
                        <tr>
                            <th>#</th>
                            <th>ชื่อสินค้า</th>
                            <th>ราคา</th>
                            <th>ราคารวม</th>
                        </tr>
                    </thead>
                    <tbody>
                        @php $key = 1; @endphp
                        @foreach($unserializeItem as $itemOrder)

                        <tr>
                            <td>{{$key}}</td>
                            <td>{{strtoupper($itemOrder->name)}}</td>
                            <td>{{ number_format($itemOrder->price) }} บาท</td>
                            <td>{{ number_format($itemOrder->quantity * $itemOrder->price) }} บาท</td>
                        </tr>
                        @php $key++; @endphp
                        @endforeach
                    </tbody>
                </table>
            </div>
            <!-- /.col -->
        </div>
        <!-- /.row -->

        <div class="row">
            <!-- accepted payments column -->
            <div class="col-xs-6">

                @if(!empty($notice_payment))
                <p class="lead">การยืนยันการชำระเงินผ่านทางเว็บไซต์ :</p>
                <p class="well well-sm no-shadow text-" style="margin-top: 10px;">
                    ธนาคาร : {{ $notice_payment->bank_name }} <br>
                    รหัสสินค้า : {{ $notice_payment->order_id }} <br>
                    วัน/เวลา การชำระเงิน : {{ $notice_payment->payment_datetime }} <br>
                    จำนวนเงิน : {{ number_format($notice_payment->amount) }} บาท<br>
                    ใบเสร็จการจ่ายเงิน : <img src="{{URL::asset($notice_payment->picture_slip)}}" width="170px"
                        style="padding:25px 0;" alt=""> <br>
                    รายละเอียด : {{ $notice_payment->description}} <br>
                    ชื่อผู้ยืนยันการชำระเงิน : {{ $notice_payment->member_name }} <br>
                    อีเมลล์ : {{ $notice_payment->email }} <br>
                    เบอร์โทรศัพท์ : {{ $notice_payment->tel }} <br>
                    <?php if ($orderlist->order_status!== "success"): ?>
                    <a href="/admin/confirmpayment/success/{{ $orderlist->id }}" class="btn btn-success"
                        style="margin: 15px 0;">ยืนยันการชำระเงิน</a>
                    <?php else: ?>
                    <a href="/admin/confirmpayment/pending/{{ $orderlist->id }}" class="btn btn-success"
                        style="margin: 15px 0;">ยกเลิกยืนยันการชำระเงิน</a>
                    <?php endif; ?>

                </p>
                @else
                @if($orderlist->order_status === "success")
                <p class="lead"> ชำระเงินผ่านช่อง :
                    <span class="text-success text-bold">{{$orderlist->payment_method}}</span>
                </p>
                @else
                <p class="text-muted well well-sm no-shadow text-danger" style="margin-top: 10px;">
                    ไม่มีการยืนยันการจ่ายผ่านทางเว็บไซต์
                </p>
                @endif
                @endif
            </div>
            <!-- /.col -->
            <div class="col-xs-6">
                <p class="lead">สรุปการสั่งสินค้า</p>

                <div class="table-responsive">
                    <table class="table">
                        <tr>
                            <th>สินค้าทั้งหมด</th>
                            <td>{{ $orderlist->order_totalqty }} ชิ้น</td>
                        </tr>
                        <tr>
                            <th style="width:50%">ราคารวม:</th>
                            <td>{{ number_format($orderlist->order_totalall) }} บาท</td>
                        </tr>
                        <!--
            <tr>
              <th>Shipping:</th>
              <td>$5.80</td>
            </tr>
            <tr>
              <th>Total:</th>
              <td>$265.24</td>
            </tr> -->
                    </table>
                </div>
            </div>
            <!-- /.col -->
        </div>
        <!-- /.row -->

        <!-- this row will not appear when printing -->
        <!-- <div class="row no-print">
            <div class="col-xs-12">
                <a href="/admin/invoice-print/{{ $orderlist->id }}" target="_blank" class="btn btn-default"><i
                        class="fa fa-print"></i> Print</a>
            </div>
        </div> -->
    </section>
    <!-- /.content -->
    <div class="clearfix"></div>
</div>
@endsection

@section('script')
@endsection