@extends('admin.layout.main_admin')

@section('content')
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        @if (session('isuccess'))
        <div class="alert alert-success">
            {{ session('isuccess') }}
        </div>
        @endif

    </section>

    <section class="content">
        <div class="row">

            <div class="col-xs-12">

                <div class="box box-info  box-solid">
                    <div class="box-header with-border ">
                        <h3 class="box-title">ออเดอร์สั่งซื้อทั้งหมด</h3>

                    </div>
                    <div class="box-body">
                        <table id="example1" class="table table-bordered table-striped">
                            <thead>
                                <tr>
                                    <th>No.</th>
                                    <th>วันที่สั่งสินค้า</th>
                                    <th>รหัสรายการสินค้า</th>
                                    <th>ชื่อผู้สั่งสินค้า</th>
                                    <th>จำนวนสินค้า</th>
                                    <th>ราคา</th>
                                    <th>สถานะ</th>
                                    <th>Manage</th>
                                </tr>
                            </thead>

                            <tbody>
                                @foreach($orderall as $key => $value)
                                <tr id="order{{ $value->id }}"
                                    class="{{ ($value->order_status === "waiting" ? "" :  ($value->order_status == "pending" ? : "bg-green")) }}">
                                    <td>{{ $key+1 }}.</td>
                                    <td>{{ $value->created_at }}</td>
                                    <!-- <td>{{ $value->member_name }}</td> -->
                                    <td>
                                        {{ $value->order_id }}
                                    </td>
                                    <td>{{ $value->member_name }}</td>
                                    <td>{{ $value->order_totalqty }}</td>

                                    <td>{{ number_format($value->order_totalall) }}</td>
                                    <td>
                                        {{($value->order_status === "waiting" ? "รอการยืนยันการชำระเงินจากผู้สั่งซื้อ" : ($value->order_status == "pending" ? "รอยืนยันการชำระเงินจากเจ้าหน้าที่"  : "ชำระเงินเรียบร้อย "))}}
                                    </td>
                                    <td>
                                        @if($value->order_status === "pending" || $value->order_status === "waiting")
                                        <button style="margin-top:10px;" class="btn btn-success" data-toggle="modal"
                                            data-target="#exampleModal_{{ $value->id }}">
                                            <span class="glyphicon glyphicon-ok"></span>
                                            ยืนยันการชำระเงิิน </button>
                                        @else
                                        <button style="margin-top:10px;" value_id="{{ $value->id }}" data-toggle="modal"
                                            data-target="#exampleModal_{{ $value->id }}" class="btn btn-danger"> <span
                                                class="glyphicon glyphicon-ok"></span>
                                            ยกเลิกการชำระเงิน</button>
                                        @endif
                                        |
                                        <a style="margin-top:10px;" href="/admin/showorderlist/{{ $value->id }}"
                                            type="button" class="btn  btn-info"> <span
                                                class="glyphicon  glyphicon-wrench"></span> </a>

                                        <button style="margin-top:10px;" value_id="{{ $value->id }}"
                                            class="btn btn-danger del_file"> <span
                                                class="glyphicon glyphicon-trash"></span> </button>
                                    </td>
                                </tr>
                                @endforeach
                            </tbody>

                        </table>
                    </div>
                    <!-- /.box-body -->
                </div>
                <!-- /.box -->
            </div>
        </div>
    </section>

</div>
<!-- /.content-wrapper -->

@foreach($orderall as $key => $value)
<div class="modal  " id="exampleModal_{{$value->id}}" tabindex="-1" role="dialog">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            @if($value->order_status === "pending" || $value->order_status === "waiting")
            <div class="modal-header">

                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
                <h4 class="modal-title">กรุณาตรวจสอบรายละเอียด</h4>

            </div>
            <div class="modal-body">
                <h4 class="text-center">ยืนยันการชำระเงินของคุณ : {{$value->member_name}}</h4>
                <hr>
                <h4><strong>รหัสายการสินค้า</strong> : {{ $value->order_id}}</h4>
                <h4><strong>ชื่อผู้สั่งสินค้า</strong> : {{ $value->member_name}}</h4>
                <h4><strong>ราคา</strong> : {{ $value->order_totalall}}</h4>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">ยกเลิก</button>
                <a type="button" class="btn btn-primary"
                    href="{{ url('/admin/confirmpayment/success/'.$value->id)}} ">ยืนยันการชำระเงิน</a>
            </div>
            @else
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
                <h4 class="modal-title">กรุณาตรวจสอบรายละเอียด</h4>

            </div>
            <div class="modal-body">
                <h4 class="text-center">ยกเลิกการชำระเงินของคุณ : {{$value->member_name}}</h4>
                <hr>
                <h4><strong>รหัสายการสินค้า</strong> : {{ $value->order_id}}</h4>
                <h4><strong>ชื่อผู้สั่งสินค้า</strong> : {{ $value->member_name}}</h4>
                <h4><strong>ราคารวมสินค้า</strong> : {{ $value->order_totalall}}</h4>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">ยกเลิก</button>
                <a type="button" class="btn btn-primary"
                    href="{{ url('/admin/confirmpayment/pending/'.$value->id)}} ">ยกเลิกการชำระเงิน</a>
            </div>
            @endif
        </div>
    </div>
</div>
@endforeach

@endsection




@section('script')

<link rel="stylesheet" href="{{ URL::asset('admin_backend/dist/css/page.css')}}">
<!-- DataTables -->
<link rel="stylesheet"
    href="{{ URL::asset('admin_backend/bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css')}}">

<!-- DataTables -->
<script src="{{ URL::asset('admin_backend/bower_components/datatables.net/js/jquery.dataTables.min.js')}}"></script>
<script src="{{ URL::asset('admin_backend/bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js')}}">
</script>

<script type="text/javascript">
var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');
$(function() {
    $('#example1').DataTable({
        "iDisplayLength": 50
    })
});
$(".del_file").click(function(e) {
    var id = $(this).attr("value_id");
    if (confirm("Are you sure to delete this data ?")) {
        //alert(img_id);
        $.ajax({
            type: "POST",
            url: "/admin/order/delete",
            data: {
                _token: CSRF_TOKEN,
                id: id
            },
            dataType: 'JSON',
            success: function(data) {
                //console.log(data['data']);
                if (data['status'] == "success")
                    $("#order" + id).remove();
            }
        });
    } else {
        e.preventDefault();
    }
});
</script>
@endsection