@extends('admin.layout.main_admin')



@section('content')
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        @if (session('msg'))
        <div class="alert alert-success">
            {{ session('msg') }}
        </div>
        @endif

    </section>
    <section class="content">
        <div class="row">

            <div class="col-xs-12">

                <div class="box box-info box-solid">

                    <div class="box-header with-border">
                        <h3 class="box-title">บัญชีธนาคาร</h3>
                        <button type="button" name="button" class="btn btn-lg bg-orange   pull-right"
                            data-toggle="modal" data-target="#addBank">
                            <i class="fa fa-fw fa-plus"></i> เพิ่มบัญชีธนาคาร
                        </button>

                    </div>
                    <div class="box-body">
                        <table id="example1" class="table table-bordered table-striped">
                            <thead>
                                <tr>
                                    <th>No.</th>
                                    <th>รูป</th>
                                    <th>ชื่อเจ้าของบัญชี</th>
                                    <th>ชื่อธนาคาร</th>
                                    <th>สาขาธนาคาร</th>
                                    <th>เลขบัญชี</th>
                                    <th>Manage</th>
                                </tr>
                            </thead>
                            <tbody>

                                @foreach($bank as $key => $value)
                                <tr id="bank{{ $value->id }}">
                                    <td>{{ $key+1 }}.</td>
                                    <td>
                                        <img src="{{ URL::asset($value->picture) }}" class="img-responsive" alt="">
                                    </td>
                                    <td>{{ $value->name_owner }}</td>
                                    <td>{{ $value->bank_name }}</td>
                                    <td>{{ $value->bank_branch }}</td>
                                    <td>{{ $value->account_number }}</td>
                                    <td>
                                        <button style="margin-top:10px;" type="button" class="btn  btn-info"
                                            data-toggle="modal" data-target="#editTeaser{{ $value->id }}"> <span
                                                class="glyphicon  glyphicon-wrench"></span> </button>
                                        <button style="margin-top:10px;" bank_id="{{ $value->id }}"
                                            class="btn btn-danger del_file"> <span
                                                class="glyphicon glyphicon-trash"></span> </button>
                                    </td>
                                </tr>
                                @endforeach

                            </tbody>
                        </table>
                    </div>
                    <!-- /.box-body -->
                </div>
                <!-- /.box -->
            </div>
        </div>
    </section>

</div>
<!-- /.content-wrapper -->

<div class="modal  fade" id="addBank">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">คุณต้องการเพิ่มทีมแพทย์</h4>
            </div>
            <form action="{{ url('admin/bank')}}" method="post" enctype="multipart/form-data">
                @csrf
                <div class="modal-body">
                    <div class="form-group">
                        <label class=" control-label">รูปภาพ</label>
                        <div class="file-upload">
                            <div class="file-select">
                                <div class="file-select-button" id="fileName">Choose File</div>
                                <div class="file-select-name" id="noFile">No file chosen...</div>
                                <input type="file" name="chooseFile" id="chooseFile" required />
                            </div>
                        </div>
                        <img class="img-responsive" id="blah" src="" />

                    </div><!-- class="form-group" -->

                    <div class="form-group">
                        <label class="control-label">ชื่อเจ้าของบัญชี</label>
                        <input type="text" name="name_owner" class="form-control" required>
                    </div><!-- class="form-group" -->


                    <div class="form-group">
                        <label class="control-label">ชื่อธนาคาร</label>
                        <input type="text" name="bank_name" class="form-control" required>
                    </div><!-- class="form-group" -->

                    <div class="form-group">
                        <label class="control-label">สาขา</label>
                        <input type="text" name="bank_branch" class="form-control" required>
                    </div><!-- class="form-group" -->

                    <div class="form-group">
                        <label class="control-label">เลขบัญชี</label>
                        <input type="text" name="account_number" class="form-control" required>
                    </div>
                </div>

                <div class="modal-footer">
                    <button type="submit" class="btn btn-info">ยืนยัน</button>
                    <button type="button" class="btn btn-default pull-left" data-dismiss="modal">ปิด</button>
                </div>
            </form>

        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>
<!-- /.modal -->



@foreach($bank as $key => $value)
<div class="modal  fade" id="editTeaser{{ $value->id }}">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">คุณต้องการแก้ไขทีมแพทย์</h4>
            </div>
            <form action="{{ url('admin/bank/update')}}" method="post" enctype="multipart/form-data">
                @csrf
                @method('PUT')
                <div class="modal-body">
                    <div class="form-group">
                        <label class=" control-label">รูปภาพ</label>
                        <div class="file-upload file-Edit">
                            <div class="file-select">
                                <div class="file-select-button fileNameEdit">Choose File</div>
                                <div class="file-select-name noFileEdit">No file chosen...</div>
                                <input type="file" name="chooseFile" class="chooseFileEdit" />
                            </div>
                        </div>
                        <img class="img-responsive blahEdit" src="" />
                        @if(!empty($value->picture))
                        <img class="img-responsive imgOld" src="{{ URL::asset($value->picture)}}" />
                        <input type="hidden" name="imageold" value="{{$value->picture}}">
                        @endif
                    </div><!-- class="form-group" -->

                    <div class="form-group">
                        <label class="control-label">ชื่อเจ้าของบัญชี</label>
                        <input type="text" name="name_owner" class="form-control" value="{{ $value->name_owner }}"
                            required>
                    </div><!-- class="form-group" -->

                    <div class="form-group">
                        <label class="control-label">ชื่อธนาคาร</label>
                        <input type="text" name="bank_name" class="form-control" value="{{ $value->bank_name }}"
                            required>
                    </div><!-- class="form-group" -->

                    <div class="form-group">
                        <label class="control-label">สาขา</label>
                        <input type="text" name="bank_branch" class="form-control" value="{{ $value->bank_branch }}"
                            required>
                    </div><!-- class="form-group" -->

                    <div class="form-group">
                        <label class="control-label">เลขบัญชี</label>
                        <input type="text" name="account_number" class="form-control"
                            value="{{ $value->account_number }}" required>
                    </div>
                </div>

                <div class="modal-footer">
                    <input type="hidden" name="id" value="{{ $value->id }}" />
                    <button type="submit" class="btn btn-info">ยืนยัน</button>
                    <button type="button" class="btn btn-default pull-left" data-dismiss="modal">ปิด</button>
                </div>
            </form>

        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>
<!-- /.modal -->
@endforeach
@endsection




@section('script')
<meta name="csrf-token" content="{{ csrf_token() }}" />
<link rel="stylesheet" href="{{ URL::asset('admin_backend/dist/css/page.css')}}">
<!-- DataTables -->
<link rel="stylesheet"
    href="{{ URL::asset('admin_backend/bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css')}}">

<!-- DataTables -->
<script src="{{ URL::asset('admin_backend/bower_components/datatables.net/js/jquery.dataTables.min.js')}}"></script>
<script src="{{ URL::asset('admin_backend/bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js')}}">
</script>

<script type="text/javascript">
var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');
$(function() {
    $('#example1').DataTable()
});
$(".del_file").click(function(e) {
    var bank_id = $(this).attr("bank_id");
    if (confirm("Are you sure to delete this data ?")) {
        //alert(img_id);
        $.ajax({
            type: "POST",
            url: "/admin/bank/delete",
            data: {
                _token: CSRF_TOKEN,
                bank_id: bank_id
            },
            dataType: 'JSON',
            success: function(data) {
                //console.log(data['data']);
                if (data['status'] == "success")
                    $("#bank" + bank_id).remove();
            }
        });
    } else {
        e.preventDefault();
    }
});
</script>

<script type="text/javascript">
function readURL(input) {

    if (input.files && input.files[0]) {
        var reader = new FileReader();

        reader.onload = function(e) {
            $('#blah').attr('src', e.target.result);
        }
        reader.readAsDataURL(input.files[0]);
    }
}
$("#chooseFile").change(function() {
    readURL(this);
});
$('#chooseFile').bind('change', function() {
    var filename = $("#chooseFile").val();
    if (/^\s*$/.test(filename)) {
        $(".file-upload").removeClass('active');
        $("#noFile").text("No file chosen...");
    } else {
        $(".file-upload").addClass('active');
        $("#noFile").text(filename.replace("C:\\fakepath\\", ""));
    }
});
</script>

<script type="text/javascript">
function readURLEdit(input) {
    if (input.files && input.files[0]) {
        var reader = new FileReader();

        reader.onload = function(e) {
            $('.imgOld').hide();
            $('.blahEdit').attr('src', e.target.result);
        }
        reader.readAsDataURL(input.files[0]);
    }
}
$(".chooseFileEdit").change(function() {
    readURLEdit(this);
});
$('.chooseFileEdit').bind('change', function() {
    var filename = $(".chooseFileMb").val();
    if (/^\s*$/.test(filename)) {
        $(".file-Edit").removeClass('active');
        $(".noFileEdit").text("No file chosen...");
    } else {
        $(".file-Editb").addClass('active');
        // $(".noFileEdit").text(filename.replace("C:\\fakepath\\", ""));
    }
});
</script>
@endsection