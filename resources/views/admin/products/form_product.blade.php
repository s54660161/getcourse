      <div class="box-body">


          <div class="col-xs-6">
              <div class="form-group">
                  <label for="exampleInputEmail1">ชื่อคอร์ส</label>
                  <input type="text" class="form-control " id="exampleInputEmail1" name="tc_namecourse"
                      value="{{ !empty($EditProduct->tc_namecourse) ? $EditProduct->tc_namecourse : ""  }}" required>
              </div>
          </div>


          <div class="col-xs-6">
              <div class="form-group">
                  <label for="exampleInputEmail1">คำอธิบาย</label>
                  <input type="text" class="form-control" id="exampleInputEmail1" name="tc_title"
                      value="{{ !empty($EditProduct->tc_title) ? $EditProduct->tc_title : ""  }}">
              </div>
          </div>

          <div class="row" style="padding:0px 15px;">
              <div class="col-xs-6">
                  <div class="form-group">
                      <label>เลือกชื่อผู้สอน</label>

                      <select multiple="multiple" data-placeholder="เลือกผู้สอน" class="form-control select2"
                          name="tc_nameteacher[]" id="teacher" required>
                          @foreach($teachers as $teacher)
                          <option value="{{ $teacher->id }}"
                              {{ !empty($teacher_array) ? (in_array( $teacher->id,$teacher_array) ? "selected" : "") : ""}}>
                              {{ $teacher->name }}
                          </option>
                          @endforeach
                      </select>
                  </div>
              </div>

              <div class="col-xs-6">
                  <div class="form-group">
                      <label>เลือกหมวดหมู่</label>

                      <select multiple="multiple" data-placeholder="เลือกหมวดหมู่คอร์ส" class="form-control select2"
                          name="tc_category[]" id="category" required>
                          @foreach($category as $cate)
                          <option value="{{ $cate->id }}"
                              {{ !empty($category_array) ? (in_array($cate->id ,$category_array) ? "selected" : "") : ""}}>
                              {{ $cate->category_name }}</option>
                          @endforeach
                      </select>
                  </div>
              </div>
          </div>

          <div class="col-xs-6">
              <div class="form-group">
                  <label for="exampleInputEmail1">URL</label>
                  <input type="text" class="form-control" id="exampleInputEmail1" name="tc_slug_name"
                      value="{{ !empty($EditProduct->tc_slug_name) ? $EditProduct->tc_slug_name : ""  }}" required>
              </div>
          </div>



          <div class="col-xs-12" style="padding:0px">
              <hr>

              <div class="col-xs-6">
                  <div class="form-group">
                      <label for="exampleInputEmail1">Woo Product Id </label>
                      <input type="text" class="form-control" id="exampleInputEmail1" name="tc_woo_product_id"
                          value="{{ !empty($EditProduct->tc_woo_product_id) ? $EditProduct->tc_woo_product_id : ""  }}">
                  </div>
              </div>

              <div class="col-xs-6">
                  <div class="form-group">
                      <label for="exampleInputEmail1"> Landing Page Code</label>
                      <input type="text" class="form-control" id="exampleInputEmail1" name="tc_landingpage_code"
                          value="{{ !empty($EditProduct->tc_landingpage_code) ? $EditProduct->tc_landingpage_code : ""  }}">
                  </div>
              </div>
          </div>

          <div class="col-xs-12" style="padding:0px">
              <hr>
              <div class="col-xs-12">
                  <div class="form-group">
                      <p><label>คอร์สฟรี : </label> </p>
                      <label>
                          <input type="radio" name="tc_free" value="0" class="flat-red course_free"
                              <?php echo (!empty($EditProduct->tc_free) ? ($EditProduct->tc_free == 0 ? "checked" : "") : "checked") ?>>
                          ไม่ฟรี
                      </label>
                      <label>
                          <input type="radio" name="tc_free" value="1" class="flat-red course_free"
                              <?php echo (!empty($EditProduct->tc_free) ? ($EditProduct->tc_free == 1 ? "checked" : "") : "") ?>>
                          ฟรี
                      </label>
                  </div>
              </div>
              <div id="course-free">
                  <div class="col-xs-6">
                      <div class="form-group">
                          <label for="exampleInputEmail1">ราคาปกติ</label>
                          <input type="text" class="form-control" id="exampleInputEmail1" name="tc_price"
                              value="{{ isset($EditProduct->tc_price) ? $EditProduct->tc_price : ""  }}">
                      </div>
                  </div>

                  <div class="col-xs-6">
                      <div class="form-group">
                          <label for="exampleInputEmail1">ราคาที่ลดเหลือ</label>
                          <input type="text" class="form-control" min="0" id="exampleInputEmail1" name="tc_discount"
                              value="{{ isset($EditProduct->tc_discount) ? $EditProduct->tc_discount : ""  }}">
                      </div>
                  </div>
              </div>
          </div>



          <div class="col-xs-12" style="padding:0px">
              <hr>
              <div class="col-xs-4">
                  <div class="form-group">
                      <p><label>การแสดงผล : </label> </p>
                      <label>
                          <input type="radio" name="tc_status" value="0" class="flat-red"
                              <?php echo (!empty($EditProduct->tc_status) ? ($EditProduct->tc_status == 0 ? "checked" : "") : "checked") ?>>
                          แสดง
                      </label>
                      <label>
                          <input type="radio" name="tc_status" value="1" class="flat-red"
                              <?php echo (!empty($EditProduct->tc_status) ? ($EditProduct->tc_status == 1 ? "checked" : "") : "") ?>>
                          ไม่แสดง
                      </label>
                  </div>
              </div>



              <div class="col-xs-4">
                  <div class="form-group">
                      <p><label>สินค้าโปรโมชั่น : </label> </p>
                      <label>
                          <input type="radio" name="tc_promotion" value="0" class="flat-red"
                              <?php echo (!empty($EditProduct->tc_promotion) ? ($EditProduct->tc_promotion == 0 ? "checked" : "") : "checked") ?>>
                          ไม่ใช่
                      </label>
                      <label>
                          <input type="radio" name="tc_promotion" value="1" class="flat-red"
                              <?php echo (!empty($EditProduct->tc_promotion) ? ($EditProduct->tc_promotion == 1 ? "checked" : "") : "") ?>>
                          ใช่
                      </label>
                  </div>
              </div>


              <div class="col-xs-4">
                  <div class="form-group">
                      <p><label>รายการแนะนำ : </label> </p>
                      <label>
                          <input type="radio" name="tc_recommend" value="0" class="flat-red"
                              <?php echo (!empty($EditProduct->tc_recommend) ? ($EditProduct->tc_recommend == 0 ? "checked" : "") : "checked") ?>>
                          ไม่ใช่
                      </label>
                      <label>
                          <input type="radio" name="tc_recommend" value="1" class="flat-red"
                              <?php echo (!empty($EditProduct->tc_recommend) ? ($EditProduct->tc_recommend == 1 ? "checked" : "") : "") ?>>
                          ใช่
                      </label>
                  </div>
              </div>
          </div>


          <div class="col-xs-12">
              <hr>
              <div class="form-group">
                  <label class=" control-label nopadding" for="editor">รายละเอียด</label>
                  <div class=" nopadding">
                      <textarea class="form-control limited" name="tc_description" id="editor" rows="3" required>
                         {{ !empty($EditProduct->tc_description) ? $EditProduct->tc_description : ""  }}
                       </textarea>
                  </div>
              </div>
              <hr>
          </div>



          <div class="col-xs-6">
              <div class="form-group dynamic">
                  <label for="exampleInputEmail1">Tags </label>
                  <div class="input-group  entry " style="padding-top:15px;padding-bottom:15px;">
                      <input type="text" class="form-control" name="tc_tags[]">
                      <span class="input-group-btn">
                          <button type="button" class="btn btn-info btn-flat btn-add" data-toggle="tooltip"
                              data-placement="right" title="เพิ่ม Tags"><span
                                  class="glyphicon glyphicon-plus"></span></button>
                      </span>
                  </div>
              </div>
          </div>

          @isset($EditProduct)
          <div class="col-xs-6 pt-4">
              <div class="box">
                  <div class="box-header">
                      <h3 class="box-title">Tags</h3>
                  </div>
                  <!-- /.box-header -->
                  <div class="box-body no-padding">
                      <table class="table table-striped">
                          <tr>
                              <th style="width: 10px">#</th>
                              <th>Tags</th>

                              <th style="width: 40px">ลบ</th>
                          </tr>

                          @if($tags_all !== "")
                          @for($i = 0 ; $i < count($tags_all); $i++) <tr id="tags_{{$i}}">
                              <td>{{$i+1}}</td>
                              <td>
                                  <div class="">
                                      <p>{{ $tags_all[$i] }}</p>
                              </td>
                              <td><button type="button" class="btn btn-danger deletetag" tag_index="{{ $i }}"
                                      id_product="{{$EditProduct->id}}">Delete</button></td>
                              </tr>
                              @endfor
                              @endif


                      </table>
                  </div>
                  <!-- /.box-body -->
              </div>
              <!-- /.box -->
          </div>
          @endisset

          <!-- /.Video Form -->
          <div class="col-xs-12">
              <hr>
              <div class="col-xs-12" style="padding:0px">
                  <label for="exampleInputEmail1">วิดีโอแนะนำคอร์ส</label>

                  <div class="form-group">
                      <p><label>เลือก: </label> </p>
                      <label>
                          <input type="radio" name="tc_video_platform" value="0" class="flat-red platform_video"
                              <?php echo (!empty($EditProduct->tc_video_platform) ? ($EditProduct->tc_video_platform == 0 ? "checked" : "") : "checked") ?>>
                          Vimeo
                      </label>
                      <label>
                          <input type="radio" name="tc_video_platform" value="1" class="flat-red platform_video"
                              <?php echo (!empty($EditProduct->tc_video_platform) ? ($EditProduct->tc_video_platform == 1 ? "checked" : "") : "") ?>>
                          Youtube
                      </label>
                  </div>
              </div>

              <div class="form-group" id="platform_vimeo"
                  style="{{ (isset($EditProduct->tc_video_platform) ? ($EditProduct->tc_video_platform == 0 ? "display:block" : "display:none") : "display:block") }}">

                  <div class="input-group">
                      <div class="input-group-addon">
                          <i class="fa fa-vimeo text-aqua"></i> https://player.vimeo.com/video/
                      </div>
                      <input type="text" class="form-control" id="exampleInputEmail1" name="tc_video_preview_vimeo"
                          value="{{ !empty($EditProduct->tc_video_preview) ? $EditProduct->tc_video_preview : ""  }}">
                  </div>
              </div>

              <div class="form-group" id="platform_youtube"
                  style="{{ (isset($EditProduct->tc_video_platform) ? ($EditProduct->tc_video_platform == 1 ? "display:block" : "display:none") : "display:none") }}">
                  <div class="input-group">
                      <div class="input-group-addon">
                          <i class="fa  fa-youtube-play text-red"></i> https://www.youtube.com/embed/
                      </div>
                      <input type="text" class="form-control" id="exampleInputEmail1" name="tc_video_preview_youtube"
                          value="{{ !empty($EditProduct->tc_video_preview) ? $EditProduct->tc_video_preview : ""  }}">
                  </div>
              </div>
          </div>
          <!-- /.Video Form -->



          <div class="col-xs-12">
              <hr>
              <div class="form-group">
                  <label class=" control-label">รูปภาพหน้าปก</label>
                  <div class="file-upload">
                      <div class="file-select">
                          <div class="file-select-button" id="fileName">Choose File</div>
                          <div class="file-select-name" id="noFile">No file chosen...</div>
                          <input type="file" name="chooseFile" id="chooseFile"
                              {{ !empty($EditProduct->tc_picture) ? "" : "required"  }}>
                      </div>
                  </div>
                  <!-- <span class="text-red">* Image size 1920</span> -->

                  <img class="img-responsive" id="blah" src="" />
                  @if(!empty($EditProduct->tc_picture))
                  <img class="img-responsive imgOld" src="{{ URL::asset($EditProduct->tc_picture)}}" />
                  <input type="hidden" name="imageold" value="{{$EditProduct->tc_picture}}">
                  @endif
              </div><!-- class="form-group" -->
          </div><!-- class="col-sm-10"-->


      </div>
      <!-- /.box-body -->

      <div class="box-footer text-center">

          <button type="submit" class="btn btn-primary btn-lg text-center">บันทึก</button>
          @if(!empty($EditProduct->id))
          <input type="hidden" name="id" value="{{$EditProduct->id}}">

          <!-- <a href="{{ route('admin.curriculum',$EditProduct->id) }}"
              class="btn btn-success btn-lg text-center">แก้ไขบทเรียน</a> -->
          @endif
      </div>