@extends('admin.layout.main_admin')



@section('content')
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        @if (session('msg'))
        <div class="alert alert-success">
            {{ session('msg') }}
        </div>
        @endif

    </section>

    <section class="content">
        <div class="row">

            <div class="col-xs-12">

                <div class="box box-info  box-solid">
                    <div class="box-header with-border ">
                        <h3 class="box-title">คอร์สทั้งหมด</h3>
                        <a href="/admin/products/create" class="btn bg-orange btn-lg pull-right">
                            <i class="fa fa-fw fa-plus"></i> เพิ่มคอร์ส
                        </a>
                    </div>
                    <div class="box-body">
                        <table id="example1" class="table table-bordered table-striped">
                            <thead>
                                <tr>
                                    <th>No.</th>
                                    <th>รูป</th>
                                    <th>ชื่อคอร์ส</th>
                                    <th>สถานะ</th>
                                    <th>จำนวนนักเรียน</th>
                                    <th>ผู้ลงทะเบียน</th>

                                    <th>วันที่สร้าง</th>
                                    <th>Manage</th>
                                </tr>
                            </thead>
                            <tbody>

                                @foreach($products as $key => $value)
                                <tr id="products{{ $value->id }}">
                                    <td>{{ $key+1 }}.</td>
                                    <td>
                                        <img src="{{ URL::asset(!empty($value->tc_picture) ? $value->tc_picture : '/images/blank_page.jpg' ) }}"
                                            width="120px;" alt="">
                                    </td>
                                    <td>{{ strtoupper($value->tc_namecourse) }}</td>

                                    <td>{{ ($value->tc_status == 0 ? "แสดงคอร์ส" : "ไม่แสดงคอร์ส" ) }}</td>
                                    <td>
                                        @if($value->member_count == "")
                                        0 คน
                                        @else
                                        {{$value->member_count}} คน
                                        &nbsp;
                                        <a href="/admin/seeMemberCourse/{{ $value->id }}" class="btn btn-success btn-xs"
                                            data-toggle="tooltip" data-placement="top" title="ดูนักเรียนในคลาส">
                                            <span class="glyphicon glyphicon-search"></span>
                                        </a>
                                        @endif
                                    </td>
                                    <td>
                                        @if($value->allmember == "")
                                        0 คน
                                        @else
                                        {{$value->allmember}} คน
                                        &nbsp;
                                        <a href="/admin/seeMemberCourseAll/{{ $value->id }}"
                                            class="btn btn-success btn-xs" data-toggle="tooltip" data-placement="top"
                                            title="ดูนักเรียนในคลาส">
                                            <span class="glyphicon glyphicon-search"></span>
                                        </a>
                                        @endif
                                    </td>
                                    <td>{{ $value->created_at }}</td>
                                    <td>
                                        <a style="margin-top:10px;" href="{{ route('admin.curriculum',$value->id) }}"
                                            type="button" class="btn  btn-success" data-toggle="tooltip"
                                            data-placement="top" title="แก้ไขวิดีโอบทเรียน">
                                            <span class="glyphicon glyphicon-triangle-right"></span>
                                        </a>
                                        <a style="margin-top:10px;" href="/admin/products/{{ $value->id }}/edit"
                                            type="button" class="btn  btn-info" data-toggle="tooltip"
                                            data-placement="top" title="แก้ไขรายละเอียดคอร์ส">
                                            <span class="glyphicon  glyphicon-wrench"></span>
                                        </a>
                                        <button style="margin-top:10px;" value_id="{{ $value->id }}"
                                            class="btn btn-danger del_product" data-toggle="tooltip"
                                            data-placement="top" title="ลบคอร์ส">
                                            <span class="glyphicon glyphicon-trash"></span>
                                        </button>
                                    </td>
                                </tr>
                                @endforeach

                            </tbody>
                        </table>
                    </div>
                    <!-- /.box-body -->
                </div>
                <!-- /.box -->
            </div>
        </div>
    </section>

</div>
<!-- /.content-wrapper -->

@endsection




@section('script')

<link rel="stylesheet" href="{{ URL::asset('admin_backend/dist/css/page.css')}}">
<!-- DataTables -->
<link rel="stylesheet"
    href="{{ URL::asset('admin_backend/bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css')}}">

<!-- DataTables -->
<script src="{{ URL::asset('admin_backend/bower_components/datatables.net/js/jquery.dataTables.min.js')}}"></script>
<script src="{{ URL::asset('admin_backend/bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js')}}">
</script>

<script type="text/javascript">
var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');

$(function() {
    $('#example1').DataTable({
        "iDisplayLength": 50
    });
})
$(".del_product").click(function(e) {
    var id = $(this).attr("value_id");
    if (confirm("Are you sure to delete this data ?")) {
        // alert(id);
        $.ajax({
            type: "POST",
            url: "/admin/products/destroy",
            data: {
                _token: CSRF_TOKEN,
                id: id
            },
            dataType: 'JSON',
            success: function(data) {
                //console.log(data['data']);
                if (data['status'] == "success")
                    $("#products" + id).remove();
            }
        });
    } else {
        e.preventDefault();
    }
});
</script>

<script type="text/javascript">
function readURL(input) {

    if (input.files && input.files[0]) {
        var reader = new FileReader();

        reader.onload = function(e) {
            $('#blah').attr('src', e.target.result);
        }
        reader.readAsDataURL(input.files[0]);
    }
}
$("#chooseFile").change(function() {
    readURL(this);
});
$('#chooseFile').bind('change', function() {
    var filename = $("#chooseFile").val();
    if (/^\s*$/.test(filename)) {
        $(".file-upload").removeClass('active');
        $("#noFile").text("No file chosen...");
    } else {
        $(".file-upload").addClass('active');
        $("#noFile").text(filename.replace("C:\\fakepath\\", ""));
    }
});
</script>

<script type="text/javascript">
function readURLEdit(input) {
    if (input.files && input.files[0]) {
        var reader = new FileReader();

        reader.onload = function(e) {
            $('.imgOld').hide();
            $('.blahEdit').attr('src', e.target.result);
        }
        reader.readAsDataURL(input.files[0]);
    }
}
$(".chooseFileEdit").change(function() {
    readURLEdit(this);
});
$('.chooseFileEdit').bind('change', function() {
    var filename = $(".chooseFileMb").val();
    if (/^\s*$/.test(filename)) {
        $(".file-Edit").removeClass('active');
        $(".noFileEdit").text("No file chosen...");
    } else {
        $(".file-Editb").addClass('active');
        // $(".noFileEdit").text(filename.replace("C:\\fakepath\\", ""));
    }
});
</script>
@endsection