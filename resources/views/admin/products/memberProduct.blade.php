@extends('admin.layout.main_admin')



@section('content')
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        @if (session('msg'))
        <div class="alert alert-success">
            {{ session('msg') }}
        </div>
        @endif

    </section>

    <section class="content">
        <div class="row">

            <div class="col-xs-12">

                <div class="box box-info  box-solid">
                    <div class="box-header with-border ">
                        <h3 class="box-title">นักเรียนในคลาส</h3>

                    </div>
                    <div class="box-body">
                        <table id="example1" class="table table-bordered table-striped">
                            <thead>
                                <tr>
                                    <th class="text-center">No.</th>
                                    <th>ชื่อ-นามสกุล</th>
                                    <th>ลงทะเบียนจาก</th>
                                    <th>อีเมล</th>
                                    <th>วันที่สมัคร</th>
                                    <th>เข้าใช้ล่าสุด</th>
                                    <th>วันที่สมัครคลาส</th>
                                </tr>
                            </thead>
                            <tbody>

                                @foreach($member as $key => $value)
                                <tr id="">
                                    <td>{{ $key+1 }}.</td>
                                    <td>{{ $value->name }}</td>
                                    <td>{{ ($value->provider === NULL ? "Email" : $value->provider) }}</td>
                                    <td>{{ $value->email  }}</td>
                                    <td>{{ $value->created_at  }}</td>
                                    <td>{{ $value->updated_at  }}</td>
                                    @foreach($memberCourse as $key => $membercourse)
                                    @if($membercourse->member_id == $value->id)
                                    <td>{{ $membercourse->created_at  }}</td>
                                    @endif
                                    @endforeach

                                </tr>
                                @endforeach

                            </tbody>
                        </table>
                    </div>
                    <!-- /.box-body -->
                </div>
                <!-- /.box -->
            </div>
        </div>
    </section>

</div>
<!-- /.content-wrapper -->

@endsection




@section('script')

<link rel="stylesheet" href="{{ URL::asset('admin_backend/dist/css/page.css')}}">
<!-- DataTables -->
<link rel="stylesheet"
    href="{{ URL::asset('admin_backend/bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css')}}">

<!-- DataTables -->
<script src="{{ URL::asset('admin_backend/bower_components/datatables.net/js/jquery.dataTables.min.js')}}"></script>
<script src="{{ URL::asset('admin_backend/bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js')}}">
</script>

<script type="text/javascript">
var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');

$(function() {
    $('#example1').DataTable({
        "iDisplayLength": 50
    });
})
$(".del_product").click(function(e) {
    var id = $(this).attr("value_id");
    if (confirm("Are you sure to delete this data ?")) {
        // alert(id);
        $.ajax({
            type: "POST",
            url: "/admin/products/destroy",
            data: {
                _token: CSRF_TOKEN,
                id: id
            },
            dataType: 'JSON',
            success: function(data) {
                //console.log(data['data']);
                if (data['status'] == "success")
                    $("#products" + id).remove();
            }
        });
    } else {
        e.preventDefault();
    }
});
</script>

<script type="text/javascript">
function readURL(input) {

    if (input.files && input.files[0]) {
        var reader = new FileReader();

        reader.onload = function(e) {
            $('#blah').attr('src', e.target.result);
        }
        reader.readAsDataURL(input.files[0]);
    }
}
$("#chooseFile").change(function() {
    readURL(this);
});
$('#chooseFile').bind('change', function() {
    var filename = $("#chooseFile").val();
    if (/^\s*$/.test(filename)) {
        $(".file-upload").removeClass('active');
        $("#noFile").text("No file chosen...");
    } else {
        $(".file-upload").addClass('active');
        $("#noFile").text(filename.replace("C:\\fakepath\\", ""));
    }
});
</script>

<script type="text/javascript">
function readURLEdit(input) {
    if (input.files && input.files[0]) {
        var reader = new FileReader();

        reader.onload = function(e) {
            $('.imgOld').hide();
            $('.blahEdit').attr('src', e.target.result);
        }
        reader.readAsDataURL(input.files[0]);
    }
}
$(".chooseFileEdit").change(function() {
    readURLEdit(this);
});
$('.chooseFileEdit').bind('change', function() {
    var filename = $(".chooseFileMb").val();
    if (/^\s*$/.test(filename)) {
        $(".file-Edit").removeClass('active');
        $(".noFileEdit").text("No file chosen...");
    } else {
        $(".file-Editb").addClass('active');
        // $(".noFileEdit").text(filename.replace("C:\\fakepath\\", ""));
    }
});
</script>
@endsection