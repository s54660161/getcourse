@extends('admin.layout.main_admin')



@section('content')
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">


    <!-- Main content -->
    <section class="content">

        <!-- general form elements -->
        <div class="box box-info">
            <div class="box-header with-border">
                <h3 class="box-title">แก้ไขคอร์ส</h3>
            </div>
            <!-- /.box-header -->
            <!-- form start -->
            <form role="form" action="/admin/products/update" method="post" enctype="multipart/form-data">
                @csrf
                @method('PUT')
                @include('admin.products.form_product')
            </form>
        </div>

    </section>
    <!-- /.content -->
</div>
<!-- /.content-wrapper -->
@endsection

@section('script')

<link rel="stylesheet" href="{{ URL::asset('admin_backend/dist/css/page.css')}}">
<link rel="stylesheet" href="{{URL::asset('admin_backend/plugins/iCheck/all.css')}}">
<!-- DataTables -->
<link rel="stylesheet"
    href="{{ URL::asset('admin_backend/bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css')}}" />

<!-- Select2 -->
<script src="{{URL::asset('admin_backend/dist/js/ckeditor/ckeditor.js')}}"></script>
<script src="{{URL::asset('admin_backend/plugins/iCheck/icheck.min.js')}}"></script>
<!-- DataTables -->
<script src="{{ URL::asset('admin_backend/bower_components/datatables.net/js/jquery.dataTables.min.js')}}"></script>
<script src="{{ URL::asset('admin_backend/bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js')}}">
</script>


<script type="text/javascript">
$('.platform_video').on('ifClicked', function(event) {
    if (this.value == 1) {
        //console.log(this.value);
        $('#platform_youtube').show();
        $('#platform_vimeo').hide();
        event.preventDefault();

    } else {
        //console.log(this.value);
        $('#platform_youtube').hide();
        $('#platform_vimeo').show();
        event.preventDefault();

    }
});

$('.select2').select2();
//Flat red color scheme for iCheck
$('input[type="checkbox"].flat-red, input[type="radio"].flat-red').iCheck({
    checkboxClass: 'icheckbox_flat-green',
    radioClass: 'iradio_flat-green'
})
$(function() {
    $('#example1').DataTable({
        "iDisplayLength": 5,
        'searching': false,
    });
})
</script>

<script>
/* exported initSample */
function loadBootstrap(event) {
    if (event.name == 'mode' && event.editor.mode == 'source')
        return; // Skip loading jQuery and Bootstrap when switching to source mode.

    var jQueryScriptTag = document.createElement('script');
    var bootstrapScriptTag = document.createElement('script');

    jQueryScriptTag.src = 'https://code.jquery.com/jquery-1.11.3.min.js';
    bootstrapScriptTag.src = 'https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js';

    var editorHead = event.editor.document.$.head;

    editorHead.appendChild(jQueryScriptTag);
    jQueryScriptTag.onload = function() {
        editorHead.appendChild(bootstrapScriptTag);
    };
}
if (CKEDITOR.env.ie && CKEDITOR.env.version < 9)
    CKEDITOR.tools.enableHtml5Elements(document);

// The trick to keep the editor in the sample quite small
// unless user specified own height.
CKEDITOR.config.height = 400;
CKEDITOR.config.width = 'auto';

var initSample = (function() {
    var wysiwygareaAvailable = isWysiwygareaAvailable(),
        isBBCodeBuiltIn = !!CKEDITOR.plugins.get('bbcode');

    return function() {
        var editorElement = CKEDITOR.document.getById('editor');
        //var editorElement = CKEDITOR.document.getById( 'editor1' );

        // :(((
        if (isBBCodeBuiltIn) {
            editorElement.setHtml(
                'Hello world!\n\n' +
                'I\'m an instance of [url=http://ckeditor.com]CKEditor[/url].'
            );
        }

        // Depending on the wysiwygare plugin availability initialize classic or inline editor.
        if (wysiwygareaAvailable) {
            /*CKEDITOR.replace( 'editor' ); */
            CKEDITOR.replace('editor', {
                filebrowserImageBrowseUrl: '{{ URL::asset("admin_backend/filemanager/index.html")}}',

                contentsCss: [
                    'https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css'
                ],
                on: {
                    instanceReady: loadBootstrap,
                    mode: loadBootstrap
                }
            });

            CKEDITOR.on('instanceReady', function(ev) {

                //resp. images for bootstrap 3
                ev.editor.dataProcessor.htmlFilter.addRules({
                    elements: {
                        $: function(element) {
                            // Output dimensions of images as width and height
                            if (element.name == 'img') {
                                var style = element.attributes.style;
                                //responzive images

                                //declare vars
                                var tclass = "";
                                var add_class = false;

                                tclass = element.attributes.class;

                                //console.log(tclass);
                                //console.log(typeof (tclass));

                                if (tclass === "undefined" || typeof(tclass) ===
                                    "undefined") {
                                    add_class = true;
                                } else {
                                    //console.log("I am not undefined");
                                    if (tclass.indexOf("img-fluid") == -1) {
                                        add_class = true;
                                    }
                                }

                                if (add_class) {
                                    var rclass = (tclass === undefined || typeof(
                                            tclass) === "undefined" ? "img-fluid" :
                                        tclass + " " + "img-fluid");
                                    element.attributes.class = rclass;
                                }

                                if (style) {
                                    // Get the width from the style.
                                    var match = /(?:^|\s)width\s*:\s*(\d+)px/i.exec(
                                            style),
                                        width = match && match[1];

                                    // Get the height from the style.
                                    match = /(?:^|\s)height\s*:\s*(\d+)px/i.exec(style);
                                    var height = match && match[1];

                                    if (width) {
                                        element.attributes.style = element.attributes
                                            .style.replace(
                                                /(?:^|\s)width\s*:\s*(\d+)px;?/i, '');
                                        element.attributes.width = width;
                                    }

                                    if (height) {
                                        element.attributes.style = element.attributes
                                            .style.replace(
                                                /(?:^|\s)height\s*:\s*(\d+)px;?/i, '');
                                        element.attributes.height = height;
                                    }
                                }
                            }



                            if (!element.attributes.style)
                                delete element.attributes.style;

                            return element;
                        }
                    }
                });
            });
        } else {
            editorElement.setAttribute('contenteditable', 'true');
            /*CKEDITOR.inline( 'editor' ); */
            CKEDITOR.inline('editor', {
                filebrowserImageBrowseUrl: '{{ URL::asset("admin_backend/filemanager/index.html")}}',
            });


            // TODO we can consider displaying some info box that
            // without wysiwygarea the classic editor may not work.
        }
    };

    function isWysiwygareaAvailable() {
        // If in development mode, then the wysiwygarea must be available.
        // Split REV into two strings so builder does not replace it :D.
        if (CKEDITOR.revision == ('%RE' + 'V%')) {
            return true;
        }

        return !!CKEDITOR.plugins.get('wysiwygarea');
    }
})();

initSample();
</script>


<script type="text/javascript">
function readURL(input) {

    if (input.files && input.files[0]) {
        var reader = new FileReader();

        reader.onload = function(e) {
            $('.imgOld').hide();
            $('#blah').attr('src', e.target.result);
        }
        reader.readAsDataURL(input.files[0]);
    }
}
$("#chooseFile").change(function() {
    readURL(this);
});
$('#chooseFile').bind('change', function() {
    var filename = $("#chooseFile").val();
    if (/^\s*$/.test(filename)) {
        $(".file-upload").removeClass('active');
        $("#noFile").text("No file chosen...");
    } else {
        $(".file-upload").addClass('active');
        $("#noFile").text(filename.replace("C:\\fakepath\\", ""));
    }
});
</script>


<script>
$(function() {
    $(document).on('click', '.btn-add', function(e) {
        e.preventDefault();

        var controlForm = $('.dynamic'),
            currentEntry = $(this).parents('.entry:first'),
            newEntry = $(currentEntry.clone()).appendTo(controlForm);

        newEntry.find('input').val('');
        controlForm.find('.entry:not(:last) .btn-add')
            .removeClass('btn-add').addClass('btn-remove')
            .removeClass('btn-success').addClass('btn-danger')
            .html('<span class="glyphicon glyphicon-minus"></span>');
    }).on('click', '.btn-remove', function(e) {
        $(this).parents('.entry:first').remove();

        e.preventDefault();
        return false;
    });
});
$(".deletetag").click(function(e) {
    var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');

    var id_index = $(this).attr("tag_index");
    var id_product = $(this).attr("id_product");
    if (confirm("คุณต้องการลบtag หรือไม่ ?")) {

        $.ajax({
            type: "POST",
            url: "/admin/products/deletetags",
            data: {
                _token: CSRF_TOKEN,
                id_product: id_product,
                id_index: id_index
            },
            dataType: 'JSON',
            success: function(data) {
                //console.log(data['data']);
                if (data['status'] == "success")
                    $("#tags_" + id_index).remove();
            }
        });
    } else {
        e.preventDefault();
    }
});
</script>
@endsection