@extends('admin.layout.main_admin')

@section('content')
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        @if (session('isuccess'))
        <div class="alert alert-success">
            {{ session('isuccess') }}
        </div>
        @endif

    </section>

    <section class="content">
        <div class="row">

            <div class="col-xs-12">

                <div class="box box-info  box-solid">
                    <div class="box-header with-border ">
                        <h3 class="box-title">สมาชิกที่ลงทะเบียนทั้งหมด</h3>
                    </div>
                    <div class="box-body">
                        <table id="example1" class="table table-bordered table-striped">
                            <thead>
                                <tr>
                                    <th>No.</th>
                                    <th>ชื่อ-นามสกุล</th>
                                    <th>ลงทะเบียนจาก</th>
                                    <th>อีเมล</th>
                                    <th>คลาสเรียน</th>
                                    <th>วันที่สมัคร</th>
                                    <th>Manage</th>
                                </tr>
                            </thead>

                            <tbody>
                                @foreach($memberall as $key => $value)
                                <tr>
                                    <td>{{ $key+1 }}.</td>
                                    <td>{{ $value->name }}</td>
                                    <td>{{ ($value->provider === NULL ? "Email" : $value->provider) }}</td>
                                    <td>{{ $value->email  }}</td>
                                    <td>
                                        @if($value->count_course == "")
                                        0 คลาส
                                        @else
                                        {{$value->count_course}} คลาส
                                        @endif
                                    </td>
                                    <td>{{ $value->created_at  }}</td>
                                    <td>
                                        <a style="margin-top:10px;" href="/admin/memberall/{{$value->id}}" type="button"
                                            class="btn  btn-info"> <span class="glyphicon  glyphicon-wrench"></span>
                                        </a>
                                        <!-- <button style="margin-top:10px;" value_id="{{$value->id}}"  class="btn btn-danger del_file"> <span class="glyphicon glyphicon-trash"></span> </button> -->
                                    </td>
                                </tr>
                                @endforeach
                            </tbody>

                        </table>
                    </div>
                    <!-- /.box-body -->
                </div>
                <!-- /.box -->
            </div>
        </div>
    </section>

</div>
<!-- /.content-wrapper -->

@endsection




@section('script')

<link rel="stylesheet" href="{{ URL::asset('admin_backend/dist/css/page.css')}}">
<!-- DataTables -->
<link rel="stylesheet"
    href="{{ URL::asset('admin_backend/bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css')}}">

<!-- DataTables -->
<script src="{{ URL::asset('admin_backend/bower_components/datatables.net/js/jquery.dataTables.min.js')}}"></script>
<script src="{{ URL::asset('admin_backend/bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js')}}">
</script>

<script type="text/javascript">
var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');
$(function() {
    $('#example1').DataTable({
        "iDisplayLength": 50
    });
})
</script>
@endsection