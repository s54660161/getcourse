@extends('admin.layout.main_admin')

@section('content')
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Main content -->
    <section class="content">

        <!-- About Me Box -->
        <div class="box    box-info  box-solid ">
            <div class="box-header with-border">
                <h3 class="box-title">ข้อมูลผู้ใช้</h3>
                <div class="box-tools pull-right">
                    <button type="button" class="btn btn-box-tool" data-widget="collapse"><i
                            class="fa fa-minus"></i></button>

                </div>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
                <strong class="text-aqua"><i class="fa fa-book margin-r-5 "></i>รายละเอียดที่ลงทะเบียน</strong>

                <p class="">
                    <dl class="dl-horizontal">
                        <dt>
                            <p>ชื่อ-นามสกุล</p>
                        </dt>
                        <dd>
                            <p>{{ $member->name }}</p>
                        </dd>

                        <dt>
                            <p>อีเมล</p>
                        </dt>
                        <dd>
                            <p>{{ $member->email }}</p>
                        </dd>

                        <dt>
                            <p>สมัครสมาชิกจาก</p>
                        </dt>
                        <dd>
                            <p>{{ ($member->provider === NULL ? "Email" : $member->provider) }}</p>
                        </dd>
                        <dt>
                            <p>เบอร์โทรศัพท์</p>
                        </dt>
                        <dd>
                            <p>{{ $member->tel }}</p>
                        </dd>

                        <dt>
                            <p>วันที่สมัครใช้งาน</p>
                        </dt>
                        <dd>
                            <p>{{ $member->created_at }}</p>
                        </dd>
                    </dl>
                </p>



            </div>
            <!-- /.box-body -->
        </div>
        <!-- /.box -->
        <div class="box    box-info  box-solid ">
            <div class="box-header with-border">
                <h3 class="box-title">คลาสเรียนที่ผู้ใช้งานซื้อไว้ทั้งหมด </h3>
                <div class="box-tools pull-right">
                    <button type="button" class="btn btn-box-tool" data-widget="collapse"><i
                            class="fa fa-minus"></i></button>

                </div>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
                <table id="example1" class="table table-bordered table-striped">
                    <thead>
                        <tr>
                            <th>No.</th>
                            <th>รูป</th>
                            <th>ชื่อคอร์ส</th>
                            <th>สถานะ</th>

                            <th>วันที่สร้าง</th>

                        </tr>
                    </thead>
                    <tbody>

                        @foreach($coursemember as $key => $value)
                        <tr id="products{{ $value->id }}">
                            <td>{{ $key+1 }}.</td>
                            <td>
                                <img src="{{ URL::asset(!empty($value->tc_picture) ? $value->tc_picture : '/images/blank_page.jpg' ) }}"
                                    width="120px;" alt="">
                            </td>
                            <td>{{ strtoupper($value->tc_namecourse) }}</td>

                            <td>{{ ($value->tc_status == 0 ? "แสดงคอร์ส" : "ไม่แสดงคอร์ส" ) }}</td>


                            <td>{{ $value->created_at }}</td>

                        </tr>
                        @endforeach

                    </tbody>
                </table>
            </div>
        </div>

    </section>
    <!-- /.content -->
</div>
<!-- /.content-wrapper -->
@endsection

@section('script')

<!-- DataTables -->
<link rel="stylesheet"
    href="{{ URL::asset('admin_backend/bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css')}}">

<!-- DataTables -->
<script src="{{ URL::asset('admin_backend/bower_components/datatables.net/js/jquery.dataTables.min.js')}}"></script>
<script src="{{ URL::asset('admin_backend/bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js')}}">
</script>
<script>
    $(function () {
        $('#example1').DataTable({
            "iDisplayLength": 50
        });
    })

</script>
@endsection

<style media="screen">
    dt,
    dd,
    p {
        font-size: 16px;

    }

    strong {
        font-size: 20px;
    }

</style>
