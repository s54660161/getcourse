      <div class="box-body">

          <div class="col-xs-6">
              <div class="form-group">
                  <label for="exampleInputEmail1">ชื่อผู้สอน</label>
                  <input type="text" class="form-control" id="exampleInputEmail1" name="name"
                      value="{{ !empty($EditTeacher->name) ? $EditTeacher->name : ""  }}" required>
              </div>
          </div>

          <div class="col-xs-6">
              <div class="form-group">
                  <label for="exampleInputEmail1">ชื่อวิชา</label>
                  <input type="text" class="form-control" id="exampleInputEmail1" name="course"
                      value="{{ !empty($EditTeacher->course) ? $EditTeacher->course : ""  }}">
              </div>
          </div>
          <div class="col-xs-12">
              <div class="form-group">
                  <label for="exampleInputEmail1">คำอธิบาย</label>
                  <input type="text" class="form-control" id="exampleInputEmail1" name="title"
                      value="{{ !empty($EditTeacher->title) ? $EditTeacher->title : ""  }}" required>
              </div>
          </div>

          <!-- <div class="col-xs-12">
              <hr>
              <div class="form-group">
                  <label class=" control-label nopadding" for="editor">รายละเอียด</label>
                  <div class=" nopadding">
                      <textarea class="form-control limited" name="description" id="editor" rows="3" required>
                         {{ !empty($EditTeacher->description) ? $EditTeacher->description : ""  }}
                       </textarea>
                  </div>
              </div>
              <hr>
          </div> -->
          <div class="col-xs-12" style="padding-right: 0px;padding-left:0px;">

              <div class="col-xs-6">
                  <div class="form-group dynamic">
                      <label for="exampleInputEmail1">ประสบการณ์ผู้สอน </label>
                      <div class="input-group  entry " style="padding-top:15px;padding-bottom:15px;">
                          <input type="text" class="form-control" name="experience[]">
                          <span class="input-group-btn">
                              <button type="button" class="btn btn-info btn-flat btn-add">
                                  <span class="glyphicon glyphicon-plus"></span></button>
                          </span>
                      </div>
                  </div>
              </div>

              @isset($EditTeacher)
              <div class="col-xs-6 pt-4">

                  <div class="box box-info box-solid">
                      <div class="box-header">
                          <h3 class="box-title">ประสบการณ์ผู้สอน</h3>
                      </div>
                      <!-- /.box-header -->
                      <div class="box-body no-padding">
                          <table id="example1" class="table table-striped">

                              <tr>
                                  <th style="width: 10px">#</th>
                                  <th>ประสบการณ์ผู้สอน</th>

                                  <th style="width: 40px">ลบ</th>
                              </tr>

                              @if($experience_all !== "")
                              @for($i = 0 ; $i < count($experience_all); $i++) <tr id="experience_{{$i}}">
                                  <td>{{$i+1}}</td>
                                  <td>
                                      <div class="">
                                          <p>{{ $experience_all[$i] }}</p>
                                  </td>
                                  <td><button type="button" class="btn btn-danger deleteexperience"
                                          experience_index="{{ $i }}" id_product="{{$EditTeacher->id}}">Delete</button>
                                  </td>
                                  </tr>
                                  @endfor
                                  @endif
                          </table>
                      </div>
                      <!-- /.box-body -->
                  </div>
                  <!-- /.box -->
              </div>
              @endisset
          </div>
          <div class="col-xs-12" style="padding-right: 0px;padding-left:0px;">

              <div class="col-xs-6">
                  <div class="form-group dynamic-portfolio">
                      <label for="exampleInputEmail1">ผลงานเเละรางวัลที่ได้รับ</label>
                      <div class="input-group  entry " style="padding-top:15px;padding-bottom:15px;">
                          <input type="text" class="form-control" name="portfolio[]">
                          <span class="input-group-btn">
                              <button type="button" class="btn btn-info btn-flat btn-add-portfolio">
                                  <span class="glyphicon glyphicon-plus"></span></button>
                          </span>
                      </div>
                  </div>
              </div>

              @isset($EditTeacher)
              <div class="col-xs-6 pt-4">

                  <div class="box box-info box-solid">
                      <div class="box-header">
                          <h3 class="box-title">ผลงานเเละรางวัลที่ได้รับ</h3>
                      </div>
                      <!-- /.box-header -->
                      <div class="box-body no-padding">
                          <table id="example1" class="table table-striped">

                              <tr>
                                  <th style="width: 10px">#</th>
                                  <th>ผลงานเเละรางวัลที่ได้รับ</th>

                                  <th style="width: 40px">ลบ</th>
                              </tr>

                              @if($portfolio_all !== "")
                              @for($i = 0 ; $i < count($portfolio_all); $i++) <tr id="portfolio_{{$i}}">
                                  <td>{{$i+1}}</td>
                                  <td>
                                      <div class="">
                                          <p>{{ $portfolio_all[$i] }}</p>
                                  </td>
                                  <td><button type="button" class="btn btn-danger deleteportfolio"
                                          portfolio_index="{{ $i }}" id_product="{{$EditTeacher->id}}">Delete</button>
                                  </td>
                                  </tr>
                                  @endfor
                                  @endif
                          </table>
                      </div>
                      <!-- /.box-body -->
                  </div>
                  <!-- /.box -->
              </div>
              @endisset
          </div>

          <div class="col-xs-12">
              <hr>

              <div class="form-group">
                  <label class=" control-label">วิดีโอแนะนำตัว</label>
                  <div class="input-group">
                      <div class="input-group-addon">
                          <i class="fa fa-vimeo text-aqua"></i> https://player.vimeo.com/video/
                      </div>
                      <input type="text" class="form-control" id="exampleInputEmail1" name="code_video"
                          value="{{ !empty($EditTeacher->code_video) ? $EditTeacher->code_video : ""  }}">
                  </div>
              </div>
          </div>

          <div class="col-xs-12">
              <hr>
              <div class="form-group">
                  <label class=" control-label">รูปภาพผู้สอน</label>
                  <div class="file-upload">
                      <div class="file-select">
                          <div class="file-select-button" id="fileName">Choose File</div>
                          <div class="file-select-name" id="noFile">No file chosen...</div>
                          <input type="file" name="chooseFile" id="chooseFile"
                              {{ !empty($EditTeacher->picture) ? "" : "required"  }}>
                      </div>
                  </div>
                  <!-- <span class="text-red">* Image size 1920</span> -->

                  <img class="img-responsive" id="blah" src="" />
                  @if(!empty($EditTeacher->picture))
                  <img class="img-responsive imgOld" src="{{ URL::asset($EditTeacher->picture)}}" />
                  <input type="hidden" name="imageold" value="{{$EditTeacher->picture}}">
                  @endif
              </div><!-- class="form-group" -->
          </div><!-- class="col-sm-10"-->


      </div>
      <!-- /.box-body -->

      <div class="box-footer text-center">

          <button type="submit" class="btn btn-primary btn-lg text-center">บันทึก</button>
      </div>