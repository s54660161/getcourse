@extends('admin.layout.main_admin')



@section('content')
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">


    <!-- Main content -->
    <section class="content">

        <!-- general form elements -->
        <div class="box box-info">
            <div class="box-header with-border">
                <h3 class="box-title">แก้ไขผู้สอน</h3>
            </div>
            <!-- /.box-header -->
            <!-- form start -->
            <form role="form" action="{{ route('admin.teacher.update',$EditTeacher->id) }}" method="post"
                enctype="multipart/form-data">
                @csrf
                @method('PUT')
                @include('admin.teachers.form_teacher')
            </form>
        </div>

    </section>
    <!-- /.content -->
</div>
<!-- /.content-wrapper -->
@endsection

@section('script')

<link rel="stylesheet" href="{{ URL::asset('admin_backend/dist/css/page.css')}}">
<link rel="stylesheet"
    href="{{ URL::asset('admin_backend/bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css')}}" />


<script src="{{ URL::asset('admin_backend/bower_components/datatables.net/js/jquery.dataTables.min.js')}}"></script>
<script src="{{ URL::asset('admin_backend/bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js')}}">
</script>




<script type="text/javascript">
function readURL(input) {

    if (input.files && input.files[0]) {
        var reader = new FileReader();

        reader.onload = function(e) {
            $('.imgOld').hide();
            $('#blah').attr('src', e.target.result);
        }
        reader.readAsDataURL(input.files[0]);
    }
}
$("#chooseFile").change(function() {
    readURL(this);
});
$('#chooseFile').bind('change', function() {
    var filename = $("#chooseFile").val();
    if (/^\s*$/.test(filename)) {
        $(".file-upload").removeClass('active');
        $("#noFile").text("No file chosen...");
    } else {
        $(".file-upload").addClass('active');
        $("#noFile").text(filename.replace("C:\\fakepath\\", ""));
    }
});

$(function() {
    $(document).on('click', '.btn-add', function(e) {
        e.preventDefault();

        var controlForm = $('.dynamic'),
            currentEntry = $(this).parents('.entry:first'),
            newEntry = $(currentEntry.clone()).appendTo(controlForm);

        newEntry.find('input').val('');
        controlForm.find('.entry:not(:last) .btn-add')
            .removeClass('btn-add').addClass('btn-remove')
            .removeClass('btn-success').addClass('btn-danger')
            .html('<span class="glyphicon glyphicon-minus"></span>');
    }).on('click', '.btn-remove', function(e) {
        $(this).parents('.entry:first').remove();

        e.preventDefault();
        return false;
    });
});

$(function() {
    $(document).on('click', '.btn-add-portfolio', function(e) {
        e.preventDefault();

        var controlForm = $('.dynamic-portfolio'),
            currentEntry = $(this).parents('.entry:first'),
            newEntry = $(currentEntry.clone()).appendTo(controlForm);

        newEntry.find('input').val('');
        controlForm.find('.entry:not(:last) .btn-add-portfolio')
            .removeClass('btn-add-portfolio').addClass('btn-remove-portfolio')
            .removeClass('btn-success').addClass('btn-danger')
            .html('<span class="glyphicon glyphicon-minus"></span>');
    }).on('click', '.btn-remove-portfolio', function(e) {
        $(this).parents('.entry:first').remove();

        e.preventDefault();
        return false;
    });
});


$(".deleteexperience").click(function(e) {
    var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');

    var id_index = $(this).attr("experience_index");
    var id_product = $(this).attr("id_product");
    if (confirm("คุณต้องการลบประสบการณ์ผู้สอน หรือไม่ ?")) {

        $.ajax({
            type: "POST",
            url: "/admin/teacher/deleteexperience",
            data: {
                _token: CSRF_TOKEN,
                id_product: id_product,
                id_index: id_index
            },
            dataType: 'JSON',
            success: function(data) {
                //console.log(data['data']);
                if (data['status'] == "success")
                    $("#experience_" + id_index).remove();
            }
        });
    } else {
        e.preventDefault();
    }
});

$(".deleteportfolio").click(function(e) {
    var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');

    var id_index = $(this).attr("portfolio_index");
    var id_product = $(this).attr("id_product");
    if (confirm("คุณต้องการลบผลงานเเละรางวัลที่ได้รับ  หรือไม่ ?")) {

        $.ajax({
            type: "POST",
            url: "/admin/teacher/deleteportfolio",
            data: {
                _token: CSRF_TOKEN,
                id_product: id_product,
                id_index: id_index
            },
            dataType: 'JSON',
            success: function(data) {
                //console.log(data['data']);
                if (data['status'] == "success")
                    $("#portfolio_" + id_index).remove();
            }
        });
    } else {
        e.preventDefault();
    }
});
</script>


@endsection