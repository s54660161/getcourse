<!doctype html>
<html lang="en">

<head>

    <meta name="viewport" content="width=device-width" />
    <meta charset="UTF-8">
    <link href="https://fonts.googleapis.com/css?family=Sarabun:100,400,700" rel="stylesheet">
    <title>ใบสั่งซื้อเลขที่ : {{$demo->order_id}}</title>
    <style type="text/css">
    /* -------------------------------------
    GLOBAL
    A very basic CSS reset
------------------------------------- */
    * {
        margin: 0;
        padding: 0;
        font-family: "Helvetica Neue", "Helvetica", Helvetica, Arial, sans-serif;
        box-sizing: border-box;
        font-size: 14px;
    }

    img {
        max-width: 100%;
    }

    body {
        -webkit-font-smoothing: antialiased;
        -webkit-text-size-adjust: none;
        width: 100% !important;
        height: 100%;
        line-height: 1.6;
    }

    /* Let's make sure all tables have defaults */
    table td {
        vertical-align: top;
    }

    /* -------------------------------------
    BODY & CONTAINER
------------------------------------- */
    body {
        background-color: #f6f6f6;
    }

    .body-wrap {
        background-color: #f6f6f6;
        width: 100%;
    }

    .container {
        display: block !important;
        max-width: 920px !important;
        margin: 0 auto !important;
        /* makes it centered */
        clear: both !important;
    }

    .content {
        max-width: 920px;
        margin: 0 auto;
        display: block;
        padding: 20px;
    }

    /* -------------------------------------
    HEADER, FOOTER, MAIN
------------------------------------- */
    .main {
        background: #fff;
        border: 1px solid #e9e9e9;
        border-radius: 3px;
    }

    .content-wrap {
        padding: 20px;
    }

    .content-block {
        padding: 0 0 20px;
    }

    .header {
        width: 100%;
        margin-bottom: 20px;
    }

    .footer {
        width: 100%;
        clear: both;
        color: #999;
        padding: 20px;
    }

    .footer a {
        color: #999;
    }

    .footer p,
    .footer a,
    .footer unsubscribe,
    .footer td {
        font-size: 12px;
    }

    /* -------------------------------------
    TYPOGRAPHY
------------------------------------- */
    h1,
    h2,
    h3 {
        font-family: "Helvetica Neue", Helvetica, Arial, "Lucida Grande", sans-serif;
        color: #000;
        margin: 40px 0 0;
        line-height: 1.2;
        font-weight: 400;
    }

    h1 {
        font-size: 32px;
        font-weight: 500;
    }

    h2 {
        font-size: 24px;
    }

    h3 {
        font-size: 18px;
    }

    h4 {
        font-size: 14px;
        font-weight: 600;
    }

    p,
    ul,
    ol {
        margin-bottom: 10px;
        font-weight: normal;
    }

    p li,
    ul li,
    ol li {
        margin-left: 5px;
        list-style-position: inside;
    }

    /* -------------------------------------
    LINKS & BUTTONS
------------------------------------- */
    a {
        color: #4a7aba;
        text-decoration: underline;
    }

    .btn-primary {
        text-decoration: none;
        color: #FFF;
        background-color: #4a7aba;
        border: solid #4a7aba;
        border-width: 5px 10px;
        line-height: 2;
        font-weight: bold;
        text-align: center;
        cursor: pointer;
        display: inline-block;
        border-radius: 5px;
        text-transform: capitalize;
    }

    /* -------------------------------------
    OTHER STYLES THAT MIGHT BE USEFUL
------------------------------------- */
    .last {
        margin-bottom: 0;
    }

    .first {
        margin-top: 0;
    }

    .aligncenter {
        text-align: center;
    }

    .alignright {
        text-align: right;
    }

    .alignleft {
        text-align: left;
    }

    .clear {
        clear: both;
    }

    /* -------------------------------------
    ALERTS
    Change the class depending on warning email, good email or bad email
------------------------------------- */
    .alert {
        font-size: 16px;
        color: #fff;
        font-weight: 500;
        padding: 20px;
        text-align: center;
        border-radius: 3px 3px 0 0;
    }

    .alert a {
        color: #fff;
        text-decoration: none;
        font-weight: 500;
        font-size: 16px;
    }

    .alert.alert-warning {
        background: #f8ac59;
    }

    .alert.alert-bad {
        background: #ed5565;
    }

    .alert.alert-good {
        background: #4a7aba;
    }

    /* -------------------------------------
    INVOICE
    Styles for the billing table
------------------------------------- */
    .invoice {
        margin: 40px auto;
        text-align: left;
        width: 90%;
    }

    .invoice td {
        padding: 5px 0;
    }

    .invoice .invoice-items {
        width: 100%;
    }

    .invoice .invoice-items td {
        border-top: #eee 1px solid;
    }

    .invoice .invoice-items .total td {
        border-top: 2px solid #333;
        border-bottom: 2px solid #333;
        font-weight: 700;
    }

    /* -------------------------------------
    RESPONSIVE AND MOBILE FRIENDLY STYLES
------------------------------------- */
    @media only screen and (max-width: 640px) {

        h1,
        h2,
        h3,
        h4 {
            font-weight: 600 !important;
            margin: 20px 0 5px !important;
        }

        h1 {
            font-size: 22px !important;
        }

        h2 {
            font-size: 18px !important;
        }

        h3 {
            font-size: 16px !important;
        }

        .container {
            width: 100% !important;
        }

        .content,
        .content-wrap {
            padding: 10px !important;
        }

        .invoice {
            width: 100% !important;
        }
    }
    </style>
</head>

<body>

    <table class="body-wrap">

        <tbody>
            <tr>
                <td></td>
                <td class="container" width="920">
                    <div class="content">

                        <table class="main" width="100%" cellpadding="0" cellspacing="0">

                            <tbody>
                                <tr>
                                    <td class="content-wrap">
                                        <table width="100%" cellpadding="0" cellspacing="0" class="invoice"
                                            style="margin-bottom:0px;margin-top:0px;">
                                            <tbody>
                                                <tr>
                                                    <td class="alignleft"><img src="{{ URL::asset('images/logo.jpg') }}"
                                                            alt="" class="alignleft"></td>
                                                    <td class="content-block alignright">
                                                        <h3>{{ $keys_global->name_office}}</h3>
                                                        <p>ใบสั่งซื้อหมายเลข : {{$demo->order_id}}</p>
                                                    </td>
                                                </tr>

                                            </tbody>
                                        </table>
                                    </td>
                                </tr>
                            </tbody>

                            <tbody>
                                <tr>
                                    <td class="content-wrap aligncenter">
                                        <table width="100%" cellpadding="0" cellspacing="0">
                                            <tbody>


                                                <tr>
                                                    <td class="content-block">
                                                        <table class="invoice">

                                                            <tbody>
                                                                <tr>
                                                                    <th>ชื่อ - นามสกุล</th>
                                                                </tr>
                                                                <tr>
                                                                    <td>{{$user->name}}</td>
                                                                </tr>
                                                                <tr>
                                                                    <th>เบอรโทรศัพท์</th>
                                                                </tr>
                                                                <tr>
                                                                    <td>{{$user->tel}}</td>
                                                                </tr>
                                                                <tr>
                                                                    <td>
                                                                        <table class="invoice-items" cellpadding="0"
                                                                            cellspacing="0">
                                                                            <thead>
                                                                                <tr>
                                                                                    <th>รายการสินค้า</th>
                                                                                    <th>จำนวน</th>
                                                                                    <th>ราคา</th>

                                                                                    <th class="alignright">ราคารวม</th>
                                                                                </tr>
                                                                            </thead>
                                                                            <tbody>
                                                                                <?php $unserializeItem = unserialize($demo->order_item); $i=1;?>
                                                                                @foreach($unserializeItem as $key =>
                                                                                $item)
                                                                                <tr>
                                                                                    <td>{{ $item->name }}</td>
                                                                                    <td>{{ $item->quantity }} คอร์ส</td>
                                                                                    <td>{{ number_format($item->price) }}
                                                                                        บาท
                                                                                    </td>
                                                                                    <td class="alignright">
                                                                                        {{ number_format($item->price * $item->quantity)  }}
                                                                                        บาท
                                                                                    </td>
                                                                                </tr>
                                                                                @endforeach
                                                                            </tbody>
                                                                            <tfoot class="total">

                                                                                <tr>
                                                                                    <td></td>
                                                                                    <td></td>
                                                                                    <td class="alignright">
                                                                                        Total</td>
                                                                                    <td class="alignright">
                                                                                        {{ number_format($demo->order_totalall)}}
                                                                                        บาท
                                                                                    </td>
                                                                                </tr>

                                                                            </tfoot>
                                                                        </table>
                                                                    </td>
                                                                </tr>
                                                            </tbody>
                                                        </table>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td class="content-block">
                                                        <a href="{{url("/myorder")}}">แจ้งการชำระเงิน</a> |
                                                        <a href="http://line.me/ti/p/{{ $keys_global->line_id }}">
                                                            ติดต่อผ่าน LINE</a>

                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td class="content-block">
                                                        {{ $keys_global->name_office}}
                                                        <p>{{ $keys_global->address}}</p>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>*** กรุณาอย่าตอบกลับอีเมลนี้
                                                        เนื่องจากเป็นอีเมลอัตโนมัติสำหรับแจ้งสถานะคำสั่งซื้อเท่านั้น ***
                                                    </td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                        <!-- <div class="footer">
                            <table width="100%">
                                <tbody>
                                    <tr>
                                        <td class="aligncenter content-block">Questions? Email <a
                                                href="mailto:">support@company.inc</a></td>
                                    </tr>
                                </tbody>
                            </table>
                        </div> -->
                    </div>
                </td>
                <td></td>
            </tr>
        </tbody>
    </table>
</body>

</html>