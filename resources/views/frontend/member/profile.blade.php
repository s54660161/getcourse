@extends('frontend.layout.main')

@section('content')
<div class="container py-5">

    @if (session('msg'))
    <div class="row justify-content-center">
        <div class="col-md-10">
            <div class="alert alert-success">
                {{ session('msg') }}
            </div>
        </div>
    </div>
    @endif
    <div class="row justify-content-center">

        <div class="col-md-10">
            <div class="card">
                <div class="card-header">{{ __('ประวัติส่วนตัว') }}</div>

                <div class="card-body">
                    <form method="POST" action="{{ route('editProfile') }}">
                        @csrf

                        <div class="form-group row">
                            <label for="name"
                                class="col-md-4 col-form-label text-md-right">{{ __('ชื่อ-นามสกุล (ภาษาไทย)') }}</label>

                            <div class="col-md-6">
                                <input id="name" type="text" class="form-control @error('name') is-invalid @enderror"
                                    name="name" value="{{ !empty($member->name) ? $member->name : ""  }}" required
                                    autocomplete="name" autofocus>

                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="email" class="col-md-4 col-form-label text-md-right">{{ __('อีเมล') }}</label>

                            <div class="col-md-6">
                                <input id="email" type="email" class="form-control @error('email') is-invalid @enderror"
                                    name="email" value="{{ !empty($member->email) ? $member->email : ""  }}" required
                                    autocomplete="email">


                            </div>
                        </div>


                        <div class="form-group row">
                            <label for="tel"
                                class="col-md-4 col-form-label text-md-right">{{ __('เบอร์โทรศัพท์') }}</label>

                            <div class="col-md-6">
                                <input id="tel" type="text" class="form-control @error('tel') is-invalid @enderror"
                                    name="tel" value="{{ !empty($member->tel) ? $member->tel : ""  }}" required
                                    autocomplete="tel">
                            </div>
                        </div>




                        <div class="form-group row mb-0">
                            <div class="col-md-6 offset-md-4">

                                <button type="submit" class="btn btn-primary">
                                    {{ __('แก้ไขประวัติ') }}
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@section('script')

@endsection