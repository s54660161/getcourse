@extends('frontend.layout.main')

@section('content')
<section id="ordermember-page">
    <div class="tab_title">
        <div class="container">
            <div class="row">
                <div class="col-lg-12 col-12 py-3">
                    <h3 class="color-sky"><i class="fas fa-play"></i> คลาสเรียนของฉัน</h3>
                </div>
            </div>
        </div>
    </div>
    <div class="container">
        <div class="col-12 px-0">
            <div class="row">
                @if(count($course) == 0)
                <div class="col-12 pt-4">
                    <div class="alert alert-primary text-center" role="alert">
                        <h3>ไม่มีคลาสเรียน</h3>

                    </div>
                </div>
                @endif
                @foreach($course as $value)
                <div class="col-lg-3 col-12">
                    <div class="card card-wrapper my-4">
                        <a href="/courses/section/{{$value->id}}">

                            <img src="{{ URL::asset(!empty($value->tc_picture) ? $value->tc_picture : '/images/tutor_banner2.png' ) }}"
                                class="img-fluid card-img-top" alt="">

                            <!-- teachers -->
                            <?php $teacher_arr = json_decode($value->tc_nameteacher); ?>
                            @foreach($teachers as $teacher)
                            @if($teacher->id == $teacher_arr['0'])
                            <div class="avatar mx-auto white"><img
                                    src="{{ URL::asset(!empty($teacher->picture) ? $teacher->picture :'/images/blank_page.jpg' ) }}"
                                    class="img-fluid rounded-circle " alt="Sample avatar image.">
                            </div>
                            @endif
                            @endforeach

                            <div class="card-body px-lg-3 py-lg-4 px-0 text-center">
                                <h4 class="card-title font-weight-bold">{{$value->tc_namecourse}}</h4>
                            </div>

                            <!-- <div class="card-footer d-flex">
                                        <div class="mr-auto">
                                            <p>{{$value->count_lec}} บทเรียน</p>

                                        </div>
                                    </div> -->
                        </a>
                    </div>
                </div>
                @endforeach
            </div>

        </div>
    </div>
</section>

@endsection