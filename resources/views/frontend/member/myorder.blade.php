@extends('frontend.layout.main')

@section('content')
<section id="ordermember-page">
    <div class="tab_title">
        <div class="container">
            <div class="row">
                <div class="col-lg-12 col-12 py-3">
                    <h3 class="color-sky"><i class="far fa-credit-card"></i> รายการสั่งซื้อ</h3>
                </div>
            </div>
        </div>
    </div>
    <div class="container">
        <div class="row">


            @if(session('msg'))
            <div class="col-lg-12 col-12 py-4">
                <div class="alert alert-success" role="alert">
                    <p>{{session('msg')}}</p>
                </div>
            </div>
            @endif

            <div class="col-12 py-3">

                <div class="row">
                    @if(count($orderlists) == 0)
                    <div class="col-12">
                        <div class="alert alert-primary text-center" role="alert">
                            <h3>ไม่มีรายการสั่งซื้อ</h3>

                        </div>
                    </div>
                    @endif
                    <div class="col-12 ">
                        @foreach($orderlists as $list)
                        @php $items = unserialize($list->order_item); @endphp
                        <div class="card my-2">
                            <div class="row card-body ">
                                <div class="col-lg-8 col-12">
                                    @foreach($items as $item)
                                    <h3 class="color-sky ">{{$item->name}}</h3>
                                    @endforeach
                                    <p class="py-2">เลขที่ใบสั่งซื้อ : {{$list->order_id}}</p>
                                    <p class="py-2">วันที่สั่งซื้อ : {{$list->created_at}}</p>
                                    <p class="py-2">ราคา : {{number_format($list->order_totalall)}} บาท</p>
                                    <p class="py-2">สถานะ :
                                        @if($list->order_status == "waiting")
                                        <span class="text-danger">
                                            รอการชำระเงิน
                                        </span>
                                        @elseif($list->order_status == "pending")
                                        <span class="text-warning">
                                            รอการยืนยันจากเจ้าหน้าที่
                                        </span>
                                        @else
                                        <span class="text-success">
                                            ชำระเงินเรียบร้อย
                                        </span>
                                        @endif
                                    </p>
                                </div>
                                <div class="col-lg-4 col-12 py-lg-0 py-3 d-flex align-items-center">
                                    @if($list->order_status == "waiting")
                                    @foreach($products as $product)
                                    @if($product->id == $item->id)
                                    @if($product->tc_woo_product_id != 0)
                                    <a href="https://market.geniusschoolthailand.com/checkout/?add-to-cart={{$product->tc_woo_product_id}}&lpcode={{$product->tc_landingpage_code}}"
                                        class="btn btn-primary btn-block">
                                        <i class="far fa-credit-card"></i> ชำระเงิน
                                    </a>

                                    @endif
                                    @endif
                                    @endforeach
                                    @endif
                                </div>
                            </div>
                        </div>
                        @endforeach
                    </div>
                </div>

            </div>
        </div>
    </div>
</section>

@endsection