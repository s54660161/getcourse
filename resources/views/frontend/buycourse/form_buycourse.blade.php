<form method="POST" action="{{ route('buyCourse') }}">
    @csrf

    <div class="form-group row">
        <label for="name" class="col-md-4 col-form-label text-md-right">{{ __('Name') }}</label>

        <div class="col-md-6">
            <input id="name" type="text" class="form-control @error('name') is-invalid @enderror" name="name"
                value="{{ !empty($member->name) ? $member->name : ""  }}" required autocomplete="name" autofocus>

        </div>
    </div>

    <div class="form-group row">
        <label for="email" class="col-md-4 col-form-label text-md-right">{{ __('E-Mail Address') }}</label>

        <div class="col-md-6">
            <input id="email" type="email" class="form-control @error('email') is-invalid @enderror" name="email"
                value="{{ !empty($member->email) ? $member->email : ""  }}" required>
            <div class=" invalid-feedback">
                อีเมลนี้มีผู้ใช้งานแล้ว
            </div>
        </div>
    </div>


    <div class="form-group row">
        <label for="tel" class="col-md-4 col-form-label text-md-right">{{ __('Tel') }}</label>

        <div class="col-md-6">
            <input id="tel" type="text" class="form-control @error('tel') is-invalid @enderror" name="tel"
                value="{{ !empty($member->tel) ? $member->tel : ""  }}" required autocomplete="tel">
        </div>
    </div>




    <div class="form-group row mb-0">
        <div class="col-md-6 offset-md-4">

            <input type="hidden" name="product_id" value="{{$detail->id}}">
            <button type="submit" class="btn btn-primary">
                {{ __('สั่งซื้อสินค้า') }}
            </button>
        </div>
    </div>
</form>