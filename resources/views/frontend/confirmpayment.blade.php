@extends('frontend.layout.main')



@section('content')

<section id="blog-herb">
    <div class="container">
        <div class="row">
            <div class="col-12  pt-4">

                <h3 class="color-sky">แจ้งการชำระเงิน</h3>
                <!-- <p><strong>Line id : </strong>
                    <a href="http://line.me/ti/p/{{ $keys_global->line_id }}">{{ $keys_global->line_id}}</a>
                </p>
                <a href="http://line.me/ti/p/{{ $keys_global->line_id }}"><img
                        src="{{URL::asset($keys_global->line_qrcode)}}" class="img-fluid py-3" alt="">
                </a> -->
            </div>
        </div>
    </div>
</section>

<section class="py-4">
    <div class="container">
        <div class="row py-4">

            <div class="col-lg-9  col-12">

                <form action="{{ url('createNewNotice') }}" method="post" enctype="multipart/form-data">
                    @csrf
                    <div class="form-group row">
                        <label for="staticEmail" class="col-sm-4 col-form-label">บัญชีที่โอนเงิน <span
                                class="text-danger">*</span></label>
                        <div class="col-sm-8">
                            @foreach($account_pay as $key=> $value)
                            <div class="form-check pb-3">
                                <input class="form-check-input" type="radio" name="bank_name"
                                    value="{{$value->bank_name}}" required {{( $key == 0 ? "checked" : "" )}}>
                                <label class="form-check-label" for="exampleRadios1">
                                    <span class="pr-3"><img src="{{URL::asset($value->picture)}}" width="35px" alt="">
                                    </span>
                                    <span class="pr-3">{{$value->name_owner}} </span>
                                    <span class="pr-3">{{$value->bank_name}} </span>
                                    <span class="pr-3">{{$value->bank_branch}}</span>
                                    <span class="pr-3">{{$value->account_number}}</span>
                                </label>
                            </div>
                            @endforeach
                        </div>
                    </div>

                    <div class="form-group row">
                        <label for="staticEmail" class="col-sm-4 col-form-label">เลขที่ใบสั่งซื้อ <span
                                class="text-danger">*</span> </label>
                        <div class="col-sm-8">
                            <input type="text" class="form-control" placeholder="ORD_12345678"
                                value="{{ !empty($order_id) ? $order_id : ""  }}" name="order_id" required>
                        </div>
                    </div>

                    <div class="form-group row">
                        <label for="inputPassword" class="col-sm-4 col-form-label">วันที่ชำระเงิน <span
                                class="text-danger">*</span></label>
                        <div class="col-sm-8">
                            <input type="date" class="form-control" placeholder="" name="date_pay" required>
                        </div>
                    </div>

                    <div class="form-group row">
                        <label for="inputPassword" class="col-sm-4 col-form-label">เวลา(ระบุโดยประมาณ) <span
                                class="text-danger">*</span></label>
                        <div class="col-sm-8">
                            <input type="time" class="form-control" placeholder="" name="time_pay" required>
                        </div>
                    </div>

                    <div class="form-group row">
                        <label for="inputPassword" class="col-sm-4 col-form-label">จำนวนเงิน <span
                                class="text-danger">*</span></label>
                        <div class="col-sm-8">
                            <input type="number" class="form-control" placeholder="จำนวนเงิน..1234" name="amount"
                                required>
                        </div>
                    </div>

                    <div class="form-group row">
                        <label class="col-sm-4 col-form-label">หลักฐานการโอนเงิน <span
                                class="text-danger">*</span></label>
                        <div class="col-sm-8">
                            <input type="file" class="form-control-file" id="chooseFile" name="chooseFile" required>
                            <img class="img-fluid" id="blah" src="" />
                        </div>
                    </div>

                    <div class="form-group row">
                        <label class="col-sm-4 col-form-label">รายละเอียดอื่นๆ </label>
                        <div class="col-sm-8">
                            <textarea rows="4" type="text" class="form-control" name="description"
                                placeholder="รายละเอียดการโอนเงิน..."></textarea>
                        </div>
                    </div>

                    <div class="form-group row">
                        <label class="col-sm-4 col-form-label">ชื่อผู้แจ้งการชำระเงิน </label>
                        <div class="col-sm-8">
                            <input type="text" class="form-control" placeholder="" name="member_name"
                                value="{{ !empty($member->name) ? $member->name : ""  }}" required>
                        </div>
                    </div>

                    <div class="form-group row">
                        <label class="col-sm-4 col-form-label">อีเมล </label>
                        <div class="col-sm-8">
                            <input type="email" class="form-control" placeholder="email@example.com"
                                value="{{ !empty($member->email) ? $member->email : ""  }}" required name="email">
                        </div>
                    </div>

                    <div class="form-group row">
                        <label class="col-sm-4 col-form-label">เบอร์ติดต่อ </label>
                        <div class="col-sm-8">
                            <input type="text" class="form-control" placeholder="081-123-4567"
                                value="{{ !empty($member->tel) ? $member->tel : ""  }}" name="tel" required>
                        </div>
                    </div>

                    <button type="submit" class="btn btn-success mt-5">แจ้งการชำระเงิน</button>
                </form>
            </div>
        </div>
    </div>
</section>
@endsection


@section('script')
<script type="text/javascript">
function readURL(input) {

    if (input.files && input.files[0]) {
        var reader = new FileReader();

        reader.onload = function(e) {
            $('#img_old').hide();
            $('#blah').attr('src', e.target.result);
        }
        reader.readAsDataURL(input.files[0]);
    }
}
$("#chooseFile").change(function() {
    readURL(this);
});
$('#chooseFile').bind('change', function() {
    var filename = $("#chooseFile").val();
    if (/^\s*$/.test(filename)) {
        $(".file-upload").removeClass('active');
        $("#noFile").text("No file chosen...");
    } else {
        $(".file-upload").addClass('active');
        $("#noFile").text(filename.replace("C:\\fakepath\\", ""));
    }
});
</script>
@endsection