@extends('frontend.layout.main')



@section('content')


<section id="top_content" class="d-none d-sm-block">
    <div class="container-fluid">
        <div class="row">
            <div class="col-lg-12 px-0">
                <div id="carouselExampleControls" class="carousel slide " data-ride="carousel">

                    <div class="carousel-inner ">
                        @foreach($slideshows as $key => $slideshow)
                        <div class="carousel-item {{ $key == 0 ? "active" : ""}} ">
                            <img class="d-block w-100" src="{{URL::asset($slideshow->picture)}}" alt="First slide">
                        </div>
                        @endforeach
                    </div>

                    <a class="carousel-control-prev" href="#carouselExampleControls" role="button" data-slide="prev">
                        <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                        <span class="sr-only">Previous</span>
                    </a>
                    <a class="carousel-control-next" href="#carouselExampleControls" role="button" data-slide="next">
                        <span class="carousel-control-next-icon" aria-hidden="true"></span>
                        <span class="sr-only">Next</span>
                    </a>
                </div>

            </div>
        </div>
    </div>
</section>

<section id="top_content" class="d-block d-sm-none">
    <div class="container-fluid">
        <div class="row">
            <div class="col-lg-12 px-0">

                <div id="carouselExampleControlsd" class="carousel slide " data-ride="carousel">

                    <div class="carousel-inner ">
                        @foreach($slideshows as $key => $slideshow)
                        <div class="carousel-item {{ $key == 0 ? "active" : ""}}">
                            <img class="d-block w-100" src="{{URL::asset($slideshow->picture_mb)}}" alt="First slide">
                        </div>
                        @endforeach
                    </div>
                    <a class="carousel-control-prev" href="#carouselExampleControlsd" role="button" data-slide="prev">
                        <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                        <span class="sr-only">Previous</span>
                    </a>
                    <a class="carousel-control-next" href="#carouselExampleControlsd" role="button" data-slide="next">
                        <span class="carousel-control-next-icon" aria-hidden="true"></span>
                        <span class="sr-only">Next</span>
                    </a>
                </div>
            </div>
        </div>
    </div>
</section>

<section id="course_rec">
    <div class="container">
        <div class="row">
            <div class="col-12 py-2">
                <h4 class="color-sky">
                    หลักสูตรแนะนำ
                </h4>
            </div>
        </div>

        <div class="row">
            @if(count($products) === 0)
            <div class="col-lg-12 col-12 py-2">
                <div class="alert alert-primary text-center" role="alert">
                    ไม่มีหลักสูตรแนะนำ
                </div>
            </div>
            @endif
            @foreach($products as $value)
            <div class="col-lg-3 col-12">
                <div class="card card-wrapper my-4">
                    <a href="{{ route('product_detail.show', $value->tc_slug_name) }}">

                        <img src="{{ URL::asset(!empty($value->tc_picture) ? $value->tc_picture : '/images/tutor_banner2.png' ) }}"
                            class="img-fluid card-img-top" alt="">

                        <!-- teachers -->
                        <?php $teacher_arr = json_decode($value->tc_nameteacher); ?>
                        @foreach($teachers as $teacher)
                        @if($teacher->id == $teacher_arr['0'])
                        <div class="avatar mx-auto white"><img
                                src="{{ URL::asset(!empty($teacher->picture) ? $teacher->picture :'/images/blank_page.jpg' ) }}"
                                class="img-fluid rounded-circle " alt="Sample avatar image.">
                        </div>
                        @endif
                        @endforeach

                        <div class="card-body px-lg-3 py-lg-4 px-0 text-center">
                            <h4 class="card-title font-weight-bold">{{$value->tc_namecourse}}</h4>
                        </div>

                        <div class="card-footer d-flex">
                            <div class="mr-auto">

                                <p>{{$value->sum_count_lec}} บทเรียน</p>

                            </div>
                            <div class="ml-auto">

                                @if($value->tc_free === 0)
                                @if($value->tc_price === 0)
                                <h4 class="text-danger font-weight-bold">
                                    ฿ {{ number_format($value->tc_discount)}}
                                </h4>
                                @else
                                <h4 class="text-danger font-weight-bold">
                                    <span>
                                        <small>
                                            <s class="text-muted pl-1"> ฿
                                                {{ number_format($value->tc_price) }}</s>
                                        </small>
                                    </span>
                                    ฿ {{ number_format($value->tc_discount)}}

                                </h4>
                                @endif
                                @else
                                <h4 class="text-danger font-weight-bold">
                                    ฟรี
                                </h4>
                                @endif


                            </div>
                        </div>
                    </a>
                </div>
            </div>
            @endforeach
        </div>
    </div>
</section>
<!-- <section id="category">
    <div class="container py-4">
        <div class="col-12 px-0">
            <h3>
                หมวดหมู่ยอดนิยม
            </h3>
        </div>
        <div class="row pt-4">
            @foreach($categorys as $value)
            <div class="col-lg-3 col-6 p-2">
                <div class="p-3 border ">
                    <h4><i class="fas fa-street-view pr-3 fa-lg"></i> {{$value->category_name}}</h4>
                </div>
            </div>
            @endforeach

        </div>
    </div>
</section> -->

<section id="bg-about" class="">
    <div class="container">
        <div class="row  ">
            <div class="col-12 col-lg-6">
                <img src="{{ URL::asset('/images/1.png') }}" class="img-fluid d-none d-sm-block" alt="">
                <img src="{{ URL::asset('/images/1.png') }}" class="img-fluid d-block d-sm-none" alt="">
            </div>
            <div class="col-12 col-lg-6 align-self-center">
                <h2 class="font-weight-bold pt-lg-0 pt-4 color-sky">ปลดล็อคศักยภาพสำหรับผู้บริหาร</h2>
                <p class="pt-4">
                    จุดมุ่งหมายของการทำธุรกิจคือทำให้เกิดความยั่งยืน ประสบความสำเร็จ ซึ่งจะมาจากการมอบสินค้า หรือบริการ
                    ที่ตอบโจทย์ให้กับลูกค้า ช่วยให้ลูกค้ามีความสุข ดังนั้นการช่วยเหลือให้ธุรกิจของคนไทยประสบความสำเร็จ
                    ก็เป็นการสร้างความสุขและความมั่งคั่งให้กับคนไทย และประเทศไทยของเรา
                </p>

                <a href="http://line.me/ti/p/{{ $keys_global->line_id }}" target="_blank"
                    class="btn btn-success my-4">สอบถามข้อมูลทาง LINE</a>
            </div>
        </div>
    </div>
</section>
<section id="teacher">
    <div class="container">
        <div class="row pb-4 ">
            <div class="col-12 py-4">
                <h4 class="color-sky">
                    ทำความรู้จัก Mentors ที่ปรึกษาธุรกิจ
                </h4>
            </div>
            @foreach($teachers as $value)
            <div class="col-lg-6 col-md-6 col-12 py-3">
                <div class="row">
                    <div class="col-lg-3 col-3">
                        <img src="{{ URL::asset(!empty($value->picture) ? $value->picture : '/images/blank_page.jpg' ) }}"
                            class="img-fluid rounded-circle" alt="">
                    </div>
                    <div class="col-lg-9 col-9">
                        <p class="font-weight-bold p-lg-0 py-1">
                            {{$value->name}}
                        </p>

                        <p class="pt-2">
                            {{$value->title}}
                        </p>
                    </div>
                </div>
            </div>
            @endforeach
        </div>
    </div>

</section>

@endsection

@section('script')
<script>
// $(document).ready(function() {
//     @foreach($products as $user)
//     console.log("{{$user->tc_namecourse}}")
//     $("#free_{{$user->id}}").text("{{$user->tc_namecourse}}");
//     @endforeach
// });
</script>

@endsection