<!DOCTYPE html>

<html lang="en" class="no-js">

<head>

    <!-- Mobile Specific Meta -->
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">


    <!-- Favicon-->
    <link rel="shortcut icon" href="{{ URL::asset(!empty($keys_global->favicon) ? $keys_global->favicon :  "") }} ">

    <!-- Author Meta -->
    <meta name="author" content="codepixer">
    <!-- Meta Description -->
    <meta name="description"
        content="<?php echo (!empty($keys_global->description) ? $keys_global->description : "") ?>">
    <!-- Meta Keyword -->
    <meta name="keywords" content="<?php echo (!empty($keys_global->keyword) ? $keys_global->keyword : "") ?>">
    <!-- meta character set -->
    <meta charset="UTF-8">
    <!-- Site Title -->
    @if(isset($product->pd_title))
    <title><?php echo (!empty($product->pd_title) ? $product->pd_title :  "" ) ?> </title>
    @else
    <title><?php echo (!empty($keys_global->title) ? $keys_global->title :  "" ) ?> </title>
    @endif

    <!-- Google analytics -->

    <?php if (!empty($keys_global->google_analytics)): ?>
    <?php echo $keys_global->google_analytics; ?>
    <?php endif; ?>

    <!-- Google analytics -->

    <?php if (!empty($keys_global->facebook_pixel)): ?>
    <?php echo $keys_global->facebook_pixel; ?>
    <?php endif; ?>
    <link href="https://fonts.googleapis.com/css?family=Sarabun:100,400,700" rel="stylesheet">
    <!--
    CSS
    ============================================= -->
    <link rel="stylesheet" href="{{ URL::asset('css/linearicons.css')}}">
    <link rel="stylesheet" href="{{ URL::asset('css/font-awesome.min.css')}} ">
    <link rel="stylesheet" href="{{ URL::asset('css/bootstrap.css')}} ">
    <link rel="stylesheet" href="{{ URL::asset('css/magnific-popup.css')}} ">
    <link rel="stylesheet" href="{{ URL::asset('css/nice-select.css')}} ">
    <link rel="stylesheet" href="{{ URL::asset('css/animate.min.css')}} ">
    <link rel="stylesheet" href="{{ URL::asset('css/owl.carousel.css')}} ">
    <link rel="stylesheet" href="{{ URL::asset('css/main.css')}} ">
    <link rel="stylesheet" href="{{ URL::asset('css/style.css')}} ">
    <link rel="stylesheet" href="{{ URL::asset('css/jquery-ui.css')}} ">

    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.2.0/css/all.css"
        integrity="sha384-hWVjflwFxL6sNzntih27bfxkr27PmbbK/iSvJ+a4+0owXq79v+lsFkW54bOGbiDQ" crossorigin="anonymous">

    <script src="{{ URL::asset('js/vendor/jquery-2.2.4.min.js') }} "></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js"
        integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous">
    </script>
    <script src="{{ URL::asset('js/vendor/bootstrap.min.js') }} "></script>

    <script src="{{ URL::asset('js/easing.min.js') }} "></script>
    <script src="{{ URL::asset('js/hoverIntent.js') }} "></script>
    <script src="{{ URL::asset('js/superfish.min.js') }} "></script>
    <script src="{{ URL::asset('js/jquery.ajaxchimp.min.js') }} "></script>
    <script src="{{ URL::asset('js/jquery.magnific-popup.min.js') }} "></script>
    <script src="{{ URL::asset('js/owl.carousel.min.js') }} "></script>
    <script src="{{ URL::asset('js/jquery.sticky.js') }} "></script>
    <script src="{{ URL::asset('js/jquery.nice-select.min.js') }} "></script>
    <script src="{{ URL::asset('js/parallax.min.js') }} "></script>
    <script src="{{ URL::asset('js/mail-script.js') }} "></script>
    <script src="{{ URL::asset('js/main.js') }} "></script>

    <script type="text/javascript" src="{{ URL::asset('js/jquery.dotdotdot.js')}}"></script>
    <script async src="https://static.addtoany.com/menu/page.js"></script>

    {!! NoCaptcha::renderJs() !!}

    @if(isset($facebook))
    <?php echo $facebook; ?>
    @endif
    <script src="{{ URL::asset('js/jquery-ui.js') }} "></script>


</head>

<body>


    <div class="container-fluid px-0" id="header-menu">
        <nav class=" navbar navbar-expand-lg  navbar-light py-lg-0 pb-3 " id="header-menu">
            <div class="container">
                <a class="navbar-brand" href="{{urlGetBiz()}}"> <img src="{{ URL::asset($keys_global->logo) }}"
                        class="img-fluid" alt=""></a>
                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav"
                    aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
                    <span class="navbar-toggler-icon"></span>
                </button>
                <div class="collapse navbar-collapse" id="navbarNav">
                    <ul class="navbar-nav   ml-auto">
                        <li class="nav-item ">
                            <a class="nav-link " href="https://www.getbiz.co/">หน้าแรก <span
                                    class="sr-only">(current)</span></a>
                        </li>
                        <li class="nav-item ">
                            <a class="nav-link" href="https://www.getbiz.co/course">คอร์สเรียน</a>
                        </li>

                        <li class="nav-item ">
                            <a class="nav-link" href="https://www.getbiz.co/events">เวิร์คช้อป</a>
                        </li>

                        <li class="nav-item ">
                            <a class="nav-link" href="https://www.getbiz.co/news">ข่าว</a>
                        </li>

                        <li class="nav-item ">
                            <a class="nav-link" href="https://www.getbiz.co/blogs">บทความ</a>
                        </li>
                        @guest
                        <li class="nav-item {{ Request::is('register') ? 'active' : '' }}">
                            <a class="nav-link " href="{{ url('/register') }}">ลงทะเบียน</a>
                        </li>
                        <li class="nav-item  {{ Request::is('login') ? 'active' : '' }}">
                            <a class="nav-link" href="{{ url('/login') }}">เข้าสู่ระบบ</a>
                        </li>

                        @endguest

                        @unless (!Auth::check())
                        <li class="nav-item dropdown">
                            <a class=" dropdown-toggle" href="#" id="navbarDropdownMenuLink" data-toggle="dropdown"
                                aria-haspopup="true" aria-expanded="false">
                                ข้อมูลสมาชิก
                            </a>
                            <div class="dropdown-menu" aria-labelledby="navbarDropdownMenuLink">
                                <a class="dropdown-item " href="{{ url('/profile') }}">ประวัติส่วนตัว
                                </a>
                                <a class="dropdown-item d-block d-sm-none" href="{{ url('/courseme') }}">คลาสเรียนของฉัน
                                </a>
                                <a class="dropdown-item " href="{{ url('/myorder') }}">แจ้งชำระเงิน
                                </a>
                                <a class="dropdown-item" href="{{ route('logout') }}" onclick="event.preventDefault();
                                document.getElementById('logout-form').submit();">
                                    ออกจากระบบ
                                </a>
                            </div>
                        </li>


                        <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                            @csrf
                        </form>
                        @endunless

                        <li class="nav-item {{ Request::is('contact') ? 'active' : '' }}">
                            <a class="nav-link" href="https://www.getbiz.co/contact">ติดต่อ</a>
                        </li>
                    </ul>
                </div>

                @unless (!Auth::check())
                <div class=" d-none d-sm-block pl-5">
                    <a href="{{ url('/courseme') }}" class="btn btn-header">คลาสเรียนของฉัน</a>
                </div>
                @endunless

            </div>
        </nav>
        <hr class="m-0">
    </div>

    @yield('content')


    <footer id="footer-menu" class="footer">
        <div class="container">
            <div class="row">
                <div class="col-lg-3 col-12 text-center">
                    <a href="{{ urlGetBiz() }}"> <img src="{{ URL::asset('images/18.jpg') }}" class="img-fluid"
                            alt=""></a>
                </div>
                <div class="col-lg-3 col-12 ">
                    @if($keys_global->name_office != "")
                    <h4 class="text-light">{{ $keys_global->name_office}}</h4>
                    @endif

                    @if($keys_global->address != "")
                    <p>
                        {{ $keys_global->address}}
                    </p>
                    @endif
                    <p class="copy">Copyright © 2019 All rights reserved</p>
                </div>
                <div class="col-lg-6 col-12 px-0 ">
                    <div class="container">
                        <div class="row">
                            <div class=" col-lg-6">
                                @if($keys_global->email != "")
                                <p>
                                    <img src="{{ URL::asset('images/19.jpg') }}" class="img-fluid">
                                    Email : {{ $keys_global->email}}
                                </p>
                                @endif

                                @if($keys_global->tel != "")
                                <a href="tel: {{ $keys_global->tel}}">
                                    <p>
                                        <img src="{{ URL::asset('images/20.jpg') }}" class="img-fluid">
                                        Phone : {{ $keys_global->tel}}
                                    </p>
                                </a>
                                @endif

                                @if($keys_global->line_id != "")
                                <a target="_blank" href="http://line.me/ti/p/{{ $keys_global->line_id }}">
                                    <p>
                                        <img src="{{ URL::asset('images/21.jpg') }}" class="img-fluid">
                                        Line : {{ $keys_global->line_id}}
                                    </p>
                                </a>
                                @endif

                            </div>
                            <div class="col-lg-6">
                                @if($keys_global->facebook_page != "")
                                <a target="_blank" href="https://{{ $keys_global->facebook_page}}">
                                    <p>
                                        <img src="{{ URL::asset('images/22.jpg') }}" class="img-fluid">
                                        Facebook : {{ $keys_global->facebook_page}}
                                    </p>
                                </a>
                                @endif
                                <a target="_blank" href="https://www.youtube.com/channel/UCYUjnw0Ni0oR3N1I9aWY88g">
                                    <p>
                                        <img src="{{ URL::asset('images/23.jpg') }}" class="img-fluid">
                                        Get Biz Channel Youtube
                                    </p>
                                </a>
                            </div>
                        </div>
                    </div>

                </div>


            </div>
        </div>
    </footer>

    <!-- start footer Area -->

    <!-- End footer Area -->
    @yield('script')
    @if(session('msg'))
    <script>
        setTimeout(function () {
            $('.alert-success').fadeOut(1000);
        }, 4000);

    </script>
    @endif
</body>

</html>
