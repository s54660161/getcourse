@extends('frontend.layout.main')



@section('content')

<section id="contact_page">

    <div class="container">
        <div class="row">
            <div class="col-lg-6 col-12 contact-address">
                @if($keys_global->name_office != "")
                <h1 class="py-4 color-sky">
                    <strong>{{ $keys_global->name_office}}</strong>
                </h1>
                @endif
                @if($keys_global->address != "")
                <p class="pb-3">
                    <strong>ที่อยู่ </strong>{{ $keys_global->address}}
                </p>
                @endif
                @if($keys_global->tel != "")
                <p>
                    <strong> โทร : </strong>
                    <a href="tel: {{ $keys_global->tel}}">{{ $keys_global->tel}}</a>
                </p>
                @endif

                <p><strong>เว็บไซต์ : </strong><a href="{{  url('/')}}">{{  url('/')}}</a></p>

                @if($keys_global->email != "")
                <p>
                    <strong>อีเมล : </strong>{{ $keys_global->email}}
                </p>
                @endif
                @if($keys_global->facebook_page != "")
                <p>
                    <strong>Facebook :</strong>
                    <a href="https://{{ $keys_global->facebook_page}}">
                        {{ $keys_global->facebook_page}}
                    </a>
                </p>
                @endif

                @if($keys_global->line_id != "")
                <p>
                    <strong>Line id : </strong>
                    <a href="http://line.me/ti/p/{{ $keys_global->line_id }}">
                        {{ $keys_global->line_id}}
                    </a>
                </p>
                @endif
                <a href="http://line.me/ti/p/{{ $keys_global->line_id }}">
                    <img src="{{URL::asset($keys_global->line_qrcode)}}" class="img-fluid py-3" alt="">
                </a>
            </div>
            <div class="col-lg-6 col-12" id="contact_success">
                @if(session('msg'))
                <div class="alert alert-success" role="alert">
                    <p>{{session('msg')}}</p>
                </div>
                @endif

                @include('frontend._formcontact')
            </div>
        </div>
    </div>
</section>

@ENDSECTION


@section('script')


@endsection