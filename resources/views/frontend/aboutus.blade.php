@extends('frontend.layout.main')



@section('content')

<section id="about-page">
    <div class="bg-top"></div>
    <div class="bg-global">
        <div class="container py-5">
            <div class="row">
                <div class="col-12">
                    @if(!empty($aboutus->picture))
                    <img src="{{ URL::asset($aboutus->picture)}}" class="img-fluid pb-5" alt="">
                    @endif
                    @if(!empty($aboutus->description))
                    {!! $aboutus->description !!}
                    @endif
                </div>
            </div>
        </div>
    </div>
</section>
@endsection