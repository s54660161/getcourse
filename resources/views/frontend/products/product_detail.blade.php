@extends('frontend.layout.main')



@section('content')

<section id="product_detail">

    <div class="tab_breadcrumb">
        <div class="container">

            <p class="py-2">
                @foreach($category as $cate)
                <a href="" class="color-sky">
                    {{ !empty($category_array) ? (in_array( $cate->id,$category_array) ? $cate->category_name  : "") : ""}}
                </a>
                @endforeach
            </p>

        </div>
    </div>


    <div class="tab_title py-3 mb-lg-4 mb-0">
        <div class="container ">
            <div class="row">
                <div class="col-lg-12">

                    <h3 class="">{{ $detail->tc_namecourse}}</h3>
                </div>
            </div>
        </div>
    </div>

    <div class="container">

        <div class="row">

            <!-- คอลั่มซ้าย -->
            <div class="col-lg-8 col-12">

                <div class="col-lg-12 px-0">

                    <div class="embed-responsive embed-responsive-16by9 pb-4">
                        <iframe src="{{$video_preview}}{{$detail->tc_video_preview}}" class="embed-responsive-item"
                            frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>
                    </div>

                    <!-- call function  -->
                    <div class="col-12 px-0">
                        <div id="accordion">
                            <p class="py-4">ประเภท :
                                @foreach($category as $cate)

                                {{ !empty($category_array) ? (in_array( $cate->id,$category_array) ? $cate->category_name  : "") : ""}}
                                @endforeach

                                <span class="pull-right">
                                    <i class="far fa-play-circle"></i>
                                    จำนวนบทเรียน : {{($count_lecture)}}
                                </span>
                            </p>
                            @foreach($sections as $sec)
                            <div class="card mb-2">
                                <div class="card-header {{(count($sections) == 1 ? "collapsed" : "")}}"
                                    id="headin{{$sec->id}}" data-toggle="collapse" data-target="#collapse_{{$sec->id}}"
                                    aria-expanded="true" aria-controls="collapse_{{$sec->id}}">

                                    <h5 class="">
                                        <span class="">
                                            {{$sec->sec_title}}
                                        </span>
                                        <span class="pull-right"> {{count($sec->lecture)}} บทเรียน
                                            @if(count($sections) == 1)
                                            <i class="fas fa-chevron-circle-up"></i>
                                            @else
                                            <i class="fas fa-chevron-circle-down"></i>
                                            @endif
                                        </span>
                                    </h5>
                                </div>
                                <div id="collapse_{{$sec->id}}"
                                    class="collapse {{(count($sections) == 1 ? "show" : "")}}"
                                    aria-labelledby="heading{{$sec->id}}" data-parent="#accordion">
                                    <div class="card-body p-0">
                                        <ul class="list-group list-group-flush">
                                            @foreach($sec->lecture as $key=>$lec)
                                            <li class="list-group-item ">
                                                {{$lec->lecture_title}}

                                                <span class=" pull-right">
                                                    @if($lec->lecture_video_link != null &&
                                                    $lec->lecture_free_video == 0)
                                                    <button
                                                        class="btn btn-outline-success btn-sm mr-4 py-0 clickToViewVideo"
                                                        lecture_id="{{$lec->id}}" course_free="{{ $detail->tc_free }}">
                                                        <i class="far fa-play-circle"></i>

                                                    </button>
                                                    @endif

                                                    @if($lec->lecture_video_link != null &&
                                                    $lec->lecture_free_video == 1)
                                                    <a href="/courses/section/{{$detail->id}}/{{$lec->id}}"
                                                        class="btn btn-outline-success btn-sm mr-4">
                                                        <i class="far fa-play-circle ">
                                                            ดูฟรี</i>
                                                    </a>
                                                    @endif

                                                    <span id="duration_{{$lec->id}}"></span>
                                                </span>
                                            </li>
                                            @endforeach

                                        </ul>
                                    </div>
                                </div>
                            </div>
                            @endforeach
                        </div>
                    </div>

                    <ul class="nav nav-tabs pt-5" id="myTab" role="tablist">
                        <li class="nav-item">
                            <a class="nav-link active" id="home-tab" data-toggle="tab" href="#home" role="tab"
                                aria-controls="home" aria-selected="true">รายละเอียดคลาส</a>
                        </li>
                        <!-- <li class="nav-item">
                            <a class="nav-link" id="profile-tab" data-toggle="tab" href="#profile" role="tab"
                                aria-controls="profile" aria-selected="false">วิธีการชำระเงิน</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" id="contact-tab" data-toggle="tab" href="#contact" role="tab"
                                aria-controls="contact" aria-selected="false">รีวิว</a>
                        </li> -->
                    </ul>


                    <div class="tab-content" id="myTabContent">
                        <div class="tab-pane fade show active" id="home" role="tabpanel" aria-labelledby="home-tab">
                            <div class="col-12 py-4">
                                {!! $detail->tc_description!!}
                            </div>



                        </div>
                        <!-- <div class="tab-pane fade" id="profile" role="tabpanel" aria-labelledby="profile-tab">...</div>
                        <div class="tab-pane fade" id="contact" role="tabpanel" aria-labelledby="contact-tab">...</div> -->
                    </div>

                </div>
            </div>
            <!-- คอลั่มซ้าย -->

            <!-- คอลั่มขวา -->
            <div class="col-lg-4 col-12">

                <a href="{{ $detail->tc_picture }}" class="img-pop-up ">
                    <img src="{{ URL::asset($detail->tc_picture)}}" class="pb-4 img-fluid mx-auto d-block"
                        alt="{{$detail->tc_namecourse}}">
                </a>

                <div class="row">
                    <div class="col-12 text-center py-3">
                        <!-- ราคาคลาส -->
                        @if($detail->tc_free == 1)
                        <!-- <p class="tc_discount text-danger"> ฿ {{ number_format($detail->tc_discount)}}</p> -->
                        <p class="tc_discount text-danger"> FREE</p>
                        @else
                        <p class="tc_discount text-danger ">
                            ฿ {{ number_format($detail->tc_discount)}}
                        </p>
                        <p class="tc_discount  pt-3">
                            <span>
                                <small>
                                    <s class="text-muted "> ฿ {{ number_format($detail->tc_price) }}</s>
                                </small>
                            </span>
                        </p>
                        @endif

                        <!-- ราคาคลาส -->
                    </div>
                    <div class=" col-12 py-3">



                        <button type="button" tabindex="-1" class="btn-block btn btn-global-red buy-course"
                            name="button" course_free="{{ $detail->tc_free }}" course_id="{{ $detail->id }}"
                            course_name="{{ $detail->tc_discount }}" id="buyCourse">
                            สั่งซื้อคอร์สเรียน
                        </button>

                        @unless (Auth::check())
                        <a href="{{ url('/login') }}" class="btn btn-block btn-global-nocolor">
                            เข้าสู่ระบบ
                        </a>
                        @endunless
                    </div>
                    <div class="col-12 ">
                        <div class="text-center text-danger" id="qty-alert"></div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-12 teacher">
                        <hr>
                        <!-- ครูผู้สอน -->
                        @foreach($teachers as $teacher)
                        @if(!empty($teacher_array))
                        @if(in_array($teacher->id,$teacher_array))
                        <div class="col-lg-12 col-md-12 col-12 py-3 px-0">
                            <div class="card">
                                <div class="card-body list-group-item">
                                    <div class="row">
                                        <div class="col-lg-5 col-5 px-1">
                                            <img src="{{ URL::asset(!empty($teacher->picture) ? $teacher->picture : '/images/blank_page.jpg' ) }}"
                                                class="img-fluid rounded-circle" alt="">
                                        </div>
                                        <div class="col-lg-7 col-7  px-1">
                                            <h4 class="font-weight-bold p-lg-0 py-1 color-sky">
                                                {{$teacher->name}}
                                            </h4>
                                            <p class="pt-1"> {{$teacher->title}}</p>
                                        </div>
                                    </div>
                                </div>

                                @php $experience_all = unserialize($teacher->experience) @endphp
                                @if(is_array($experience_all)&&count($experience_all) != 0)
                                <div class="card-body list-group-item ">
                                    <p>ประสบการณ์ผู้สอน</p>
                                    <ul>
                                        @for($i = 0 ; $i < count($experience_all); $i++) <span></span>
                                            <li class="pt-1">
                                                {{$experience_all[$i]}}
                                            </li>
                                            @endfor
                                    </ul>
                                </div>
                                @endif

                                @php $portfolio_all = unserialize($teacher->portfolio) @endphp
                                @if(is_array($portfolio_all)&&count($portfolio_all) != 0)
                                <div class="card-body list-group-item ">
                                    <p>ผลงานเเละรางวัลที่ได้รับ</p>
                                    <ul>
                                        @for($i = 0 ; $i < count($portfolio_all); $i++) <span></span>
                                            <li class="pt-1">
                                                {{$portfolio_all[$i]}}
                                            </li>
                                            @endfor
                                    </ul>
                                </div>
                                @endif


                            </div>
                        </div>
                        @endif
                        @endif

                        @endforeach
                        <!-- ครูผู้สอน -->
                    </div>
                </div>


                <div class="row">
                    <div class="col-12 pt-5">
                        <h3 class="color-sky">
                            หลักสูตรแนะนำ
                        </h3>
                        <hr>
                    </div>

                    @if(count($products) === 0)
                    <div class="col-lg-12 col-12 py-2">
                        <div class="alert alert-primary text-center" role="alert">
                            ไม่มีหลักสูตรแนะนำ
                        </div>
                    </div>
                    @endif
                    @foreach($products as $value)
                    <div class="col-lg-12 col-12">
                        <div class="card card-wrapper my-2">
                            <a href="{{ route('product_detail.show', $value->tc_slug_name) }}">

                                <img src="{{ URL::asset(!empty($value->tc_picture) ? $value->tc_picture : '/images/tutor_banner2.png' ) }}"
                                    class="img-fluid card-img-top" alt="">

                                <!-- teachers -->
                                <?php $teacher_arr = json_decode($value->tc_nameteacher); ?>
                                @foreach($teachers as $teacher)
                                @if($teacher->id == $teacher_arr['0'])
                                <div class="avatar mx-auto white"><img
                                        src="{{ URL::asset(!empty($teacher->picture) ? $teacher->picture :'/images/blank_page.jpg' ) }}"
                                        class="img-fluid rounded-circle " alt="Sample avatar image.">
                                </div>
                                @endif
                                @endforeach

                                <div class="card-body px-lg-3 py-lg-4 px-0 text-center">
                                    <h4 class="card-title font-weight-bold">{{$value->tc_namecourse}}</h4>
                                </div>

                                <div class="card-footer d-flex">
                                    <div class="mr-auto">

                                        <p>{{$value->sum_count_lec}} บทเรียน</p>

                                    </div>
                                    <div class="ml-auto">
                                        @if($value->tc_free === 0)
                                        @if($value->tc_price === 0)
                                        <h4 class="text-danger font-weight-bold">
                                            ฿ {{ number_format($value->tc_discount)}}
                                        </h4>
                                        @else
                                        <h4 class="text-danger font-weight-bold">
                                            <span>
                                                <small>
                                                    <s class="text-muted pl-1"> ฿
                                                        {{ number_format($value->tc_price) }}</s>
                                                </small>
                                            </span>
                                            ฿ {{ number_format($value->tc_discount)}}

                                        </h4>
                                        @endif
                                        @else
                                        <h4 class="text-danger font-weight-bold">
                                            ฟรี
                                        </h4>
                                        @endif


                                    </div>
                                </div>
                            </a>
                        </div>

                    </div>
                    @endforeach
                </div>
                <!-- row recommended product -->

            </div>
        </div>
        <!-- คอลั่มขวา -->


        <!-- <div class="row">
            <div class="col-12 py-4">
                <hr>
                @if($tags != "")
                <p> TAGS :
                    @for($i = 0 ; $i < count($tags); $i++) <a href="/searchprod/{{ $tags[$i] }}">
                        {{ $tags[$i]."," }}</a>
                        @endfor
                </p>
                @endif
            </div>
        </div> -->
    </div>

    <!-- Modal  -->
    <div class="modal fade" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true"
        id="myModal">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">{{ __('Login') }}</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    @include('frontend.auth.form_login')
                </div>

            </div>
        </div>
    </div>

    <div class="modal fade " tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true"
        id="formBuy">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">{{ __('สรุปข้อมูลการสั่งซื้อ') }}</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body" id="page-cart">

                    <div class="col-12 table-responsive tableresponsive">
                        <table class="table tabledata">
                            <thead>
                                <tr>
                                    <th scope="col">คลาส</th>
                                    <th class="text-center" scope="col">ราคารวม</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>

                                    <td data-title="คลาส">{{$detail->tc_namecourse}}</td>
                                    <td data-title="ราคารวม" class="text-center">
                                        {{number_format($detail->tc_discount)}}
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                    <div class="col-12 ml-auto">
                        <div class="table-responsive">
                            <table class="table cart-summary">
                                <tbody style="">
                                    <tr class="cart-summary-head">
                                        <td colspan="4" class="cart_summary_title" align="center">
                                            สรุปยอดเงินที่ต้องชำระ
                                        </td>
                                    </tr>

                                    <tr class="cart_summary_title">
                                        <td colspan="2" align="center">ราคารวม</td>
                                        <td colspan="2"> {{number_format($detail->tc_discount)}}</td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                        <hr>
                    </div>
                    <div class="py-4">
                        <h4 class="text-center pb-4">ข้อมูลผู้ซื้อ</h4>
                        @include('frontend.buycourse.form_buycourse')
                    </div>
                </div>

            </div>
        </div>
    </div>

    <!-- Modal  -->

</section>

@ENDSECTION


@section('script')

<script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
<script src="{{ URL::asset('js/input-spinner.js') }} "></script>
@error('email')
<script>
    $('#formBuy').modal('toggle');

</script>
@enderror
@unless (!Auth::check())
<script>
    $(document).ready(function () {
        var courseID = "{{$detail->id}}";

        if (courseID) {
            $.ajax({
                url: "/checkCourseMember",
                type: 'post',
                data: {
                    _token: CSRF_TOKEN,
                    data: courseID
                },
                success: function (result) {
                    console.log(result.status)
                    if (result.status == "success") {
                        $("#myBtn").hide();
                        document.getElementById("buyCourse").disabled = true;
                        document.getElementById("buyCourse").innerHTML = 'คุณมีคลาสนี้แล้ว';
                    }
                    if (result.status == "pending") {
                        $("#myBtn").hide();
                        document.getElementById("buyCourse").disabled = true;
                        document.getElementById("buyCourse").innerHTML = 'รอการอนุมัติการสั่งซื้อ';
                    }
                }
            });

        }
    });

</script>
@endunless
<script src="https://player.vimeo.com/api/player.js"></script>

<script>
    var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');
    var toHHMMSS = (secs) => {
        var sec_num = parseInt(secs, 10)
        var hours = Math.floor(sec_num / 3600)
        var minutes = Math.floor(sec_num / 60) % 60
        var seconds = sec_num % 60

        return [hours, minutes, seconds]
            .map(v => v < 10 ? "0" + v : v)
            .filter((v, i) => v !== "00" || i > 0)
            .join(":")
    }

    function YTDurationToSeconds(duration) {
        var match = duration.match(/PT(\d+H)?(\d+M)?(\d+S)?/);

        match = match.slice(1).map(function (x) {
            if (x != null) {
                return x.replace(/\D/, '');
            }
        });

        var hours = (parseInt(match[0]) || 0);
        var minutes = (parseInt(match[1]) || 0);
        var seconds = (parseInt(match[2]) || 0);

        return hours * 3600 + minutes * 60 + seconds;
    }
    $(document).ready(function () {

        @foreach($sections as $sec)
        "@foreach($sec->lecture as $lec)"
        "@if($lec->lecture_video_link)"

        //check tc_video_platform
        "@if($detail->tc_video_platform == 0)"
        //console.log(data)
        $.get("https://vimeo.com/api/oembed.json?url=https%3A//vimeo.com/{{$lec->lecture_video_link}}",
            function (
                data) {
                $("#duration_{{$lec->id}}").text(toHHMMSS(data.duration));
            });

        "@else"
        $.get("https://www.googleapis.com/youtube/v3/videos?part=snippet%2CcontentDetails%2Cstatistics&id={{$lec->lecture_video_link}}&key=AIzaSyDvgmHOKwECUoFs72o385k9--o-CeCNOhE",
            function (
                data) {
                var convertYtTime = YTDurationToSeconds(data.items['0'].contentDetails.duration)
                $("#duration_{{$lec->id}}").text(toHHMMSS(convertYtTime));
            });
        "@endif"
        //check tc_video_platform

        "@else"
        $("#duration_{{$lec->id}}").text("-");
        "@endif"

        "@endforeach"
        @endforeach
    });

    $('.buy-course').click(function () {

        var authCheck = "{{ Auth::check() }}";
        var course_free = $(this).attr('course_free');
        var course_id = $(this).attr('course_id');

        if (authCheck) {
            if (course_free == 0) {
                $('#formBuy').modal('toggle')
            } else {
                addFreeCourse(course_id, course_free)
            }
        } else {
            checkLogin()
        }


    });


    $('.clickToViewVideo').click(function () {
        var authCheck = "{{ Auth::check() }}";
        var courseID = "{{$detail->id}}";
        var lecture_id = $(this).attr('lecture_id');
        var course_free = $(this).attr('course_free');
        // console.log(course_free)
        if (authCheck) {
            if (courseID) {
                $.ajax({
                    url: "/checkCourseMember",
                    type: 'post',
                    data: {
                        _token: CSRF_TOKEN,
                        data: courseID

                    },
                    success: function (result) {
                        if (result.status == "failed") {
                            swal({
                                title: "คุณต้องการดูเนื้อหาคลาสใช่หรือไม่?",
                                text: "คุณจำเป็นต้องสั่งซื้อคลาสก่อนเพื่อเข้าเรียน",
                                icon: "warning",
                                buttons: ["ยกเลิก", "สั่งซื้อคลาสเรียน"],
                                dangerMode: true,
                            }).then((willDelete) => {
                                if (willDelete) {
                                    if (course_free == 1) {
                                        addFreeCourse(courseID, course_free)
                                    } else {
                                        $('#formBuy').modal('toggle')
                                    }
                                }
                            });
                        } else if (result.status == "pending") {
                            swal({
                                title: "รอการอนุมัติจากแอดมิน",
                                text: "คุณได้จัดเก็บคลาสนี้เรียบร้อย และรอการอนุมัติการจ่ายเงินจากแอดมิน",
                                icon: "warning",
                                button: "ตกลง",
                            });
                        } else {
                            window.location.href = "/courses/section/" + courseID + "/" +
                                lecture_id;
                        }

                    }
                });

            }
        } else {
            checkLogin()
        }
    });

</script>

<script>
    function toggleChevron(e) {
        $(e.target)
            .prev('.card-header')
            .find("i.fas")
            .toggleClass('fa-chevron-circle-down fa-chevron-circle-up');
    }
    $('#accordion').on('hidden.bs.collapse', toggleChevron);
    $('#accordion').on('shown.bs.collapse', toggleChevron);

    function addFreeCourse(courseID, course_free) {
        $.ajax({
            url: "/addCourseFree",
            type: 'post',
            data: {
                _token: CSRF_TOKEN,
                course_free: course_free,
                course_id: courseID,
            },
            success: function (result) {
                if (result.status == "success") {
                    swal({
                        title: "Good job!",
                        text: "คุณได้จัดเก็บคลาสนี้เรียบร้อย",
                        icon: "success",
                        button: "ตกลง",
                    }).then((ok) => {
                        if (ok) {
                            window.location
                                .href =
                                "/courseme";
                        }
                    });
                }
            }

        });
    }

    function checkLogin() {
        window.location
            .href =
            "/login";
        // swal({
        //         title: "คุณต้องการดูเนื้อหาคลาสใช่หรือไม่?",
        //         text: "คุณจำเป็นต้องเข้าสู่ระบบเพื่อเข้าเรียนทุกครั้ง",
        //         icon: "warning",
        //         buttons: ["ยกเลิก", "เข้าสู่ระบบ"],
        //         dangerMode: true,
        //     })
        //     .then((willDelete) => {
        //         if (willDelete) {
        //             $('#myModal').modal('toggle')
        //         }
        //     });
    }

</script>
@endsection
