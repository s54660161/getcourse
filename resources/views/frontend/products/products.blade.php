@extends('frontend.layout.main')



@section('content')

<section id="products_page">
    <div class="" id="productall">
        <div class="container">
            <div class="row">
                <div class="col-lg-12 col-12 py-3">
                    <h3 class="text-light"><i class="fas fa-list-ul "></i> คอร์สเรียนทั้งหมด</h3>
                </div>
            </div>
        </div>
    </div>

    <div class="bg-global">
        <div class="container">
            <div class="row">


                <div class="col-lg-12 col-12">
                    <div class="row">
                        @if(count($products) === 0)
                        <div class="col-lg-12 col-12 py-5">
                            <div class="alert alert-primary text-center" role="alert">
                                ไม่มีสินค้า
                            </div>
                        </div>
                        @endif

                        @foreach($products as $value)
                        <div class="col-lg-3 col-12">
                            <div class="card card-wrapper my-4">
                                <a href="{{ route('product_detail.show', $value->tc_slug_name) }}">

                                    <img src="{{ URL::asset(!empty($value->tc_picture) ? $value->tc_picture : '/images/tutor_banner2.png' ) }}"
                                        class="img-fluid card-img-top" alt="">

                                    <!-- teachers -->
                                    <?php $teacher_arr = json_decode($value->tc_nameteacher); ?>
                                    @foreach($teachers as $teacher)
                                    @if($teacher->id == $teacher_arr['0'])
                                    <div class="avatar mx-auto white"><img
                                            src="{{ URL::asset(!empty($teacher->picture) ? $teacher->picture :'/images/blank_page.jpg' ) }}"
                                            class="img-fluid rounded-circle " alt="Sample avatar image.">
                                    </div>
                                    @endif
                                    @endforeach

                                    <div class="card-body px-lg-3 py-lg-4 px-0 text-center">
                                        <h4 class="card-title font-weight-bold">{{$value->tc_namecourse}}</h4>
                                    </div>

                                    <div class="card-footer d-flex">
                                        <div class="mr-auto">
                                            <p>{{$value->sum_count_lec}} บทเรียน</p>

                                        </div>
                                        <div class="ml-auto">
                                            @if($value->tc_free === 0)
                                            @if($value->tc_price === 0)
                                            <h4 class="text-danger font-weight-bold">
                                                ฿ {{ number_format($value->tc_discount)}}
                                            </h4>
                                            @else
                                            <h4 class="text-danger font-weight-bold">
                                                <span>
                                                    <small>
                                                        <s class="text-muted pl-1"> ฿
                                                            {{ number_format($value->tc_price) }}</s>
                                                    </small>
                                                </span>
                                                ฿ {{ number_format($value->tc_discount)}}

                                            </h4>
                                            @endif
                                            @else
                                            <h4 class="text-danger font-weight-bold">
                                                ฟรี
                                            </h4>
                                            @endif

                                        </div>
                                    </div>
                                </a>
                            </div>
                        </div>
                        @endforeach


                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

@ENDSECTION


@section('script')


@endsection