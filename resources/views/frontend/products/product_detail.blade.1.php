@extends('frontend.layout.main')



@section('content')

<section id="product_detail">

    <div class="tab_breadcrumb">
        <div class="container">
            <nav aria-label="breadcrumb ">
                <ol class="breadcrumb mb-0">
                    <li class="breadcrumb-item">
                        <p class="py-2">
                            @foreach($category as $cate)
                            <a href="" class="color-sky">
                                {{ !empty($category_array) ? (in_array( $cate->id,$category_array) ? $cate->category_name  : "") : ""}}
                            </a>
                            @endforeach
                        </p>
                    </li>

                </ol>
            </nav>
        </div>
    </div>


    <div class="tab_title py-3 mb-5">
        <div class="container ">
            <div class="row">
                <div class="col-lg-12">

                    <h1 class="">{{ $detail->tc_namecourse}}</h1>
                </div>
            </div>
        </div>
    </div>

    <div class="container">

        <div class="row">

            <!-- คอลั่มซ้าย -->
            <div class="col-lg-8 col-12">
                <div class="col-lg-12 px-0">

                    <div class="embed-responsive embed-responsive-16by9 pb-4">
                        <iframe src="{{$video_preview}}{{$detail->tc_video_preview}}" class="embed-responsive-item"
                            frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>
                    </div>

                    <ul class="nav nav-tabs pt-5" id="myTab" role="tablist">
                        <li class="nav-item">
                            <a class="nav-link active" id="home-tab" data-toggle="tab" href="#home" role="tab"
                                aria-controls="home" aria-selected="true">รายละเอียดคอร์ส</a>
                        </li>
                        <!-- <li class="nav-item">
                            <a class="nav-link" id="profile-tab" data-toggle="tab" href="#profile" role="tab"
                                aria-controls="profile" aria-selected="false">วิธีการชำระเงิน</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" id="contact-tab" data-toggle="tab" href="#contact" role="tab"
                                aria-controls="contact" aria-selected="false">รีวิว</a>
                        </li> -->
                    </ul>


                    <div class="tab-content" id="myTabContent">
                        <div class="tab-pane fade show active" id="home" role="tabpanel" aria-labelledby="home-tab">
                            <div class="col-12 py-4">
                                {!! $detail->tc_description!!}
                            </div>

                            <!-- call function  -->
                            <div class="col-12 ">
                                <ul class="list-group">
                                    @foreach($sections as $sec)
                                    <li class="list-group-item active">
                                        {{$sec->sec_title}}
                                    </li>
                                    @foreach($sec->lecture as $key=>$lec)
                                    <li class="list-group-item ">
                                        {{$lec->lecture_title}}

                                        <span class="pull-right">
                                            @if($lec->lecture_video_link != null && $lec->lecture_free_video == 0)
                                            <button class="btn btn-outline-success btn-sm mr-4 py-0 clickToViewVideo">
                                                <i class="far fa-play-circle"></i>
                                            </button>
                                            @endif

                                            @if($lec->lecture_video_link != null && $lec->lecture_free_video == 1)
                                            <a href="/courses/section/{{$detail->id}}/{{$lec->id}}/{{$detail->tc_slug_name}}"
                                                class="btn btn-outline-success btn-sm mr-4">
                                                <i class="far fa-play-circle "> ดูตัวอย่างฟรี</i>
                                            </a>
                                            @endif

                                            {{ vimeoGetDetailVideo($lec->lecture_video_link,$detail->tc_video_platform) }}
                                        </span>
                                    </li>
                                    @endforeach
                                    @endforeach
                                </ul>
                            </div>


                        </div>
                        <!-- <div class="tab-pane fade" id="profile" role="tabpanel" aria-labelledby="profile-tab">...</div>
                        <div class="tab-pane fade" id="contact" role="tabpanel" aria-labelledby="contact-tab">...</div> -->
                    </div>

                </div>
            </div>
            <!-- คอลั่มซ้าย -->

            <!-- คอลั่มขวา -->
            <div class="col-lg-4 col-12">

                <a href="{{ $detail->tc_picture }}" class="img-pop-up">
                    <img src="{{ URL::asset($detail->tc_picture)}}" class="pb-4 img-fluid"
                        alt="{{$detail->tc_namecourse}}">
                </a>

                <div class="row">
                    <div class="col-lg-6 col-12 text-center py-3">
                        <!-- ราคาคอร์ส -->
                        @if($detail->tc_free === 0)
                        @if($detail->tc_price === 0)
                        <p class="tc_discount text-danger"> ฿ {{ number_format($detail->tc_discount)}}</p>
                        @else
                        <button class="btn-block btn btn-global-sky">
                            <p class="tc_discount text-danger ">
                                ฿ {{ number_format($detail->tc_discount)}}
                            </p>
                            <p class="tc_discount  pt-3">
                                <span>
                                    <small>
                                        <s class="text-muted "> ฿ {{ number_format($detail->tc_price) }}</s>
                                    </small>
                                </span>
                            </p>
                        </button>
                        @endif
                        @else
                        <button class="btn-block btn btn-global-sky">
                            <p class="tc_discount text-danger"> Free</p>
                        </button>
                        @endif
                        <!-- ราคาคอร์ส -->
                    </div>
                    <div class="col-lg-6 col-12 py-3">

                        <button type="button" class=" btn-block btn btn-global-nocolor add-to-cart " name="button"
                            product_id="{{ $detail->id }}" product_name="{{ $detail->tc_namecourse }}"
                            product_price="{{ $detail->tc_discount }}">สั่งซื้อสินค้า</button>
                    </div>
                    <div class="col-12 ">
                        <div class="text-center text-danger" id="qty-alert"></div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-12">
                        <hr>
                        <!-- ครูผู้สอน -->
                        <p class="py-3"> ครูผู้สอน :
                            @foreach($teachers as $teacher)
                            {{ !empty($teacher_array) ? (in_array( $teacher->id,$teacher_array) ? $teacher->name ."," : "") : ""}}
                            @endforeach
                        </p>
                        <!-- ครูผู้สอน -->
                    </div>
                </div>


                <div class="row">
                    <div class="col-12 py-4">
                        <h3>
                            หลักสูตรแนะนำ
                        </h3>
                        <hr>
                    </div>
                    @foreach($products as $value)
                    <div class="col-lg-12 col-12">
                        <div class="card border-light my-4">
                            <a href="{{ route('product_detail.show', $value->tc_slug_name) }}">
                                <div class="row no-gutters">
                                    <div class="col-md-5">
                                        <img src="{{ URL::asset(!empty($value->tc_picture) ? $value->tc_picture : '/images/blank_page.jpg' ) }}"
                                            class="img-fluid" alt="">
                                    </div>
                                    <div class="col-md-7">
                                        <div class="card-body px-lg-3 pt-lg-0 px-0">
                                            <h4 class="card-title font-weight-bold">{{$value->tc_namecourse}}</h4>

                                            @if($value->tc_free == 0)
                                            @if($value->tc_price === 0)
                                            <h4 class=" text-danger font-weight-bold">
                                                ฿ {{ number_format($value->tc_discount)}}
                                            </h4>
                                            @else
                                            <h4 class=" text-danger font-weight-bold">
                                                ฿ {{ number_format($value->tc_discount)}}
                                                <span>
                                                    <small>
                                                        <s class="text-muted "> ฿
                                                            {{ number_format($value->tc_price) }}</s>
                                                    </small>
                                                </span>
                                            </h4>
                                            @endif
                                            @else
                                            <h3 class=" text-danger ">Free</h3>
                                            @endif

                                        </div>

                                    </div>
                                </div>
                            </a>
                        </div>

                    </div>
                    @endforeach

                </div>
                <hr>

                <p class="py-3">{{$detail->tc_title}}</p>
            </div>
        </div>
        <!-- คอลั่มขวา -->


        <div class="row">
            <div class="col-12 py-4">
                <hr>
                @if($tags != "")
                <p> TAGS :
                    @for($i = 0 ; $i < count($tags); $i++) <a href="/searchprod/{{ $tags[$i] }}">
                        {{ $tags[$i]."," }}</a>
                        @endfor
                </p>
                @endif
            </div>
        </div>
    </div>

    <div class="modal fade" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true"
        id="myModal">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">{{ __('Login') }}</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    @include('frontend.auth.form_login')
                </div>

            </div>
        </div>
    </div>
    <div class="container">
        <div id="accordion">
            @foreach($sections as $sec)
            <div class="card">
                <div class="card-header" id="headin{{$sec->id}}">
                    <h5 class="mb-0">
                        <button class="btn btn-link" data-toggle="collapse" data-target="#collapse_{{$sec->id}}"
                            aria-expanded="true" aria-controls="collapse_{{$sec->id}}">
                            {{$sec->sec_title}}
                        </button>
                    </h5>
                </div>
                <div id="collapse_{{$sec->id}}" class="collapse " aria-labelledby="heading{{$sec->id}}"
                    data-parent="#accordion">
                    <div class="card-body">
                        <ul class="list-group list-group-flush">
                            @foreach($sec->lecture as $key=>$lec)
                            <li class="list-group-item ">
                                {{$lec->lecture_title}}

                                <span class="pull-right">
                                    @if($lec->lecture_video_link != null && $lec->lecture_free_video == 0)
                                    <button class="btn btn-outline-success btn-sm mr-4 py-0 clickToViewVideo">
                                        <i class="far fa-play-circle"></i>
                                    </button>
                                    @endif

                                    @if($lec->lecture_video_link != null && $lec->lecture_free_video == 1)
                                    <a href="/courses/section/{{$detail->id}}/{{$lec->id}}/{{$detail->tc_slug_name}}"
                                        class="btn btn-outline-success btn-sm mr-4">
                                        <i class="far fa-play-circle "> ดูตัวอย่างฟรี</i>
                                    </a>
                                    @endif

                                    {{ vimeoGetDetailVideo($lec->lecture_video_link,$detail->tc_video_platform) }}
                                </span>
                            </li>
                            @endforeach

                        </ul>
                    </div>
                </div>
            </div>
            @endforeach
        </div>
    </div>
</section>

@ENDSECTION


@section('script')
<script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
<script src="{{ URL::asset('js/input-spinner.js') }} "></script>
<script>
var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');
//var value_products = $("#value_products").val();
$("input[type='number']").inputSpinner();
$('#value_products').on("change", function(event) {
    var value_products = $("#value_products").val();
    if (value_products != "0") {
        return value_products;
    }
});


$('.add-to-cart').click(function() {
    var value_qty = $("#value_products").val();
    var value_id = $(this).attr("product_id");
    var value_name = $(this).attr("product_name");
    var value_price = $(this).attr("product_price");

    if (value_qty != 0) {
        var text_alert = '<p style="display:none"></p>';
        $('#qty-alert').html(text_alert);
        $.ajax({
            type: "POST",
            url: "/add-to-cart",
            data: {
                _token: CSRF_TOKEN,
                value_id: value_id,
                value_name: value_name,
                value_qty: value_qty,
                value_price: value_price
            },
            dataType: 'JSON'
        });
    } else {
        var text_alert = '<p class="text-danger  py-3">กรุณาเลือกจำนวนสินค้า</p>';

        $('#qty-alert').html(text_alert);
    }
    alertAddcart();
});

function alertAddcart() {
    $('.alert-cart').fadeIn(1000);
    setTimeout(function() {
        $('.alert-cart').fadeOut(1000);
    }, 4000);
}

$('.clickToViewVideo').click(function() {
    var authCheck = "{{ Auth::check() }}";
    var courseID = "{{$detail->id}}";
    //console.log(courseID)
    if (authCheck) {
        if (courseID) {
            $.ajax({
                url: "/checkCourseMember",
                type: 'post',
                data: {
                    _token: CSRF_TOKEN,
                    data: courseID

                },
                success: function(result) {
                    if (result.status == "failed") {
                        swal({
                            title: "คุณต้องการดูเนื้อหาคอร์สใช่หรือไม่?",
                            text: "คุณจำเป็นต้องสั่งซื้อคอร์สก่อนเพื่อเข้าเรียน",
                            icon: "warning",
                            buttons: ["ยกเลิก", "สั่งซื้อคอร์สเรียน"],
                            dangerMode: true,
                        }).then((willDelete) => {
                            if (willDelete) {
                                $('#myModal').modal('toggle')
                            }
                        });
                    } else {

                    }

                }
            });

        }
    } else {
        swal({
                title: "คุณต้องการดูเนื้อหาคอร์สใช่หรือไม่?",
                text: "คุณจำเป็นต้องเข้าสู่ระบบเพื่อเข้าเรียนทุกครั้ง",
                icon: "warning",
                buttons: ["ยกเลิก", "เข้าสู่ระบบ"],
                dangerMode: true,
            })
            .then((willDelete) => {
                if (willDelete) {
                    $('#myModal').modal('toggle')
                }
            });

    }
});
</script>

@endsection