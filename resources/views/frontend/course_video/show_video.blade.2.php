@extends('frontend.layout.main')


@section('content')
<section class="bg-dark">
    <div class="container">
        <div class="row">
            @if($product->tc_video_platform == 0)
            <div class="col-12  text-center">
                <div id="made-in-ny"></div>
            </div>
            @else
            <div class="col-12  text-center video-container">
                <div id="player"></div>
            </div>
            @endif

        </div>
    </div>
</section>

<div class="container">

    <div class="row">

        <div class="col-12 py-4" id="accordion" role="tablist" aria-multiselectable="true">

            @foreach($sections as $sec)
            <div class="card mb-2">
                <div class="card-header" id="headin{{$sec->id}}" data-toggle="collapse"
                    data-target="#collapse_{{$sec->id}}" data-parent="#accordion" aria-expanded="true"
                    aria-controls="collapse_{{$sec->id}}">

                    <h5 class="">
                        <span class="">
                            {{$sec->sec_title}}
                        </span>
                        <span class="pull-right"> {{count($sec->lecture)}} บทเรียน
                            <i class="fas fa-chevron-circle-up"></i>
                        </span>
                    </h5>
                </div>
                <div id="collapse_{{$sec->id}}" class="collapse show in" aria-labelledby="heading{{$sec->id}}"
                    data-parent="#accordion">
                    <div class="card-body p-0">
                        <ul class="list-group list-group-flush">
                            @foreach($sec->lecture as $key=>$lec)
                            <li class="list-group-item checkPermission" Lecture_free="{{ $lec->lecture_free_video }}"
                                Lecture_code="{{ $lec->lecture_video_link }}" Lecture_id="{{ $lec->id }}"
                                id="link_{{ $lec->id }}">
                                {{$lec->lecture_title}}

                                <span class=" pull-right">
                                    @if($lec->lecture_video_link != null &&
                                    $lec->lecture_free_video == 0)
                                    <button class="btn btn-outline-success btn-sm mr-4 py-0 clickToViewVideo"
                                        lecture_id="{{$lec->id}}">
                                        <i class="far fa-play-circle"></i>
                                    </button>
                                    @endif

                                    @if($lec->lecture_video_link != null &&
                                    $lec->lecture_free_video == 1)
                                    <button class="btn btn-outline-success btn-sm mr-4">
                                        <i class="far fa-play-circle ">
                                            ดูฟรี
                                        </i>
                                    </button>
                                    @endif

                                    <span id="duration_{{$lec->id}}"></span>
                                </span>
                            </li>
                            @endforeach

                        </ul>
                    </div>
                </div>
            </div>
            @endforeach
        </div>
    </div>
    <!-- /#page-content-wrapper -->
    <div class="modal fade" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true"
        id="myModal">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">{{ __('Login') }}</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    @include('frontend.auth.form_login')
                </div>

            </div>
        </div>
    </div>
</div>


<style>
.video-container {
    position: relative;
    padding-bottom: 56.25%;
    padding-top: 30px;
    height: 0;
    overflow: hidden;
}

.video-container iframe,
.video-container object,
.video-container embed {
    position: absolute;
    top: 0;
    left: 0;
    width: 100%;
    height: 100%;
}

li {
    cursor: pointer;
}
</style>

@endsection

@section('script')

<!-- Bootstrap core JavaScript -->

<script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
<script>
function toggleChevron(e) {
    $(e.target)
        .prev('.card-header')
        .find("i.fas")
        .toggleClass('fa-chevron-circle-down fa-chevron-circle-up');
}
$('#accordion').on('hidden.bs.collapse', toggleChevron);
$('#accordion').on('shown.bs.collapse', toggleChevron);
//active li
$('li').click(function() {
    var idvideo = $(this).attr('Lecture_id');
    $('li').removeClass('active');
    $('#link_' + idvideo).addClass('active');
});
</script>
<!-- Menu Toggle Script -->
<script>
var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');
var toHHMMSS = (secs) => {
    var sec_num = parseInt(secs, 10)
    var hours = Math.floor(sec_num / 3600)
    var minutes = Math.floor(sec_num / 60) % 60
    var seconds = sec_num % 60

    return [hours, minutes, seconds]
        .map(v => v < 10 ? "0" + v : v)
        .filter((v, i) => v !== "00" || i > 0)
        .join(":")
}

function YTDurationToSeconds(duration) {
    var match = duration.match(/PT(\d+H)?(\d+M)?(\d+S)?/);

    match = match.slice(1).map(function(x) {
        if (x != null) {
            return x.replace(/\D/, '');
        }
    });

    var hours = (parseInt(match[0]) || 0);
    var minutes = (parseInt(match[1]) || 0);
    var seconds = (parseInt(match[2]) || 0);

    return hours * 3600 + minutes * 60 + seconds;
}

$(document).ready(function() {

    @foreach($sections as $sec)
    "@foreach($sec->lecture as $lec)"
    "@if($lec->lecture_video_link)"

    //check tc_video_platform
    "@if($product->tc_video_platform == 0)"
    //console.log(data)
    $.get("https://vimeo.com/api/oembed.json?url=https%3A//vimeo.com/{{$lec->lecture_video_link}}",
        function(
            data) {
            $("#duration_{{$lec->id}}").text(toHHMMSS(data.duration));
        });

    "@else"
    $.get("https://www.googleapis.com/youtube/v3/videos?part=snippet%2CcontentDetails%2Cstatistics&id={{$lec->lecture_video_link}}&key=AIzaSyDvgmHOKwECUoFs72o385k9--o-CeCNOhE",
        function(
            data) {
            var convertYtTime = YTDurationToSeconds(data.items['0'].contentDetails.duration)
            $("#duration_{{$lec->id}}").text(toHHMMSS(convertYtTime));
        });
    "@endif"
    //check tc_video_platform

    "@else"
    $("#duration_{{$lec->id}}").text("-");
    "@endif"

    "@endforeach"
    @endforeach
});




$(".checkPermission").click(function(e) {
    var Lecture_free = $(this).attr('Lecture_free');
    var Lecture_code = $(this).attr('Lecture_code');
    // var Lecture_id = $(this).attr('Lecture_id');
    if (Lecture_free == 1) {
        loadVideoById(Lecture_code)
    } else {
        var authCheck = "{{ Auth::check() }}";
        var courseID = "{{$product->id}}";

        if (authCheck) {
            if (courseID) {
                $.ajax({
                    url: "/checkCourseMember",
                    type: 'post',
                    data: {
                        _token: CSRF_TOKEN,
                        data: courseID

                    },
                    success: function(result) {
                        if (result.status == "failed") {
                            swal({
                                title: "คุณต้องการดูเนื้อหาคอร์สใช่หรือไม่?",
                                text: "คุณจำเป็นต้องสั่งซื้อคอร์สก่อนเพื่อเข้าเรียน",
                                icon: "warning",
                                buttons: ["ยกเลิก", "สั่งซื้อคอร์สเรียน"],
                                dangerMode: true,
                            }).then((willDelete) => {
                                if (willDelete) {
                                    window.location.href =
                                        "{{ route('product_detail.show', $product->tc_slug_name) }}";
                                }
                            });
                        } else if (result.status == "pending") {
                            swal({
                                title: "รอการอนุมัติจากแอดมิน",
                                text: "คุณได้จัดเก็บคอร์สนี้เรียบร้อย และรอการอนุมัติการจ่ายเงินจากแอดมิน",
                                icon: "warning",
                                button: "ตกลง",
                            });
                        } else {
                            loadVideoById(Lecture_code)
                        }

                    }
                });

            }
        } else {
            swal({
                    title: "คุณต้องการดูเนื้อหาคอร์สใช่หรือไม่?",
                    text: "คุณจำเป็นต้องเข้าสู่ระบบเพื่อเข้าเรียนทุกครั้ง",
                    icon: "warning",
                    buttons: ["ยกเลิก", "เข้าสู่ระบบ"],
                    dangerMode: true,
                })
                .then((willDelete) => {
                    if (willDelete) {
                        $('#myModal').modal('toggle')
                    }
                });
        }
    }

});
</script>

@if($product->tc_video_platform == 0)

<script src="https://player.vimeo.com/api/player.js"></script>
<script>
// Vimeo API
var id_video = "{{ $lecture_find->id}}"
var code_video = "{{ $lecture_find->lecture_video_link}}"
var options = {
    id: code_video,
    loop: true,
    responsive: true
};
var player = new Vimeo.Player('made-in-ny', options);
$('#link_' + id_video).addClass('active');

function loadVideoById(videoId) {
    player.loadVideo(videoId).then(function(id) {});
}
</script>
@endif


@if($product->tc_video_platform == 1)
<script>
// Youtube API
var tag = document.createElement('script');
var id_video = "{{ $lecture_find->id}}"
var code_video = "{{ $lecture_find->lecture_video_link}}"

tag.src = "https://www.youtube.com/iframe_api";
var firstScriptTag = document.getElementsByTagName('script')[0];
firstScriptTag.parentNode.insertBefore(tag, firstScriptTag);

// 3. This function creates an <iframe> (and YouTube player)
//    after the API code downloads.
var player;

$('#link_' + id_video).addClass('active');

function onYouTubeIframeAPIReady() {
    player = new YT.Player('player', {
        height: '100%',
        width: '100%',
        videoId: code_video,
    });
}

function loadVideoById(videoId) {
    player.loadVideoById({
        'videoId': videoId,
        'suggestedQuality': 'large'
    });
}
</script>
@endif

@endsection