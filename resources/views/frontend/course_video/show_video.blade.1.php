<!DOCTYPE html>
<html lang="en">

<head>

    <!-- Mobile Specific Meta -->
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">


    <!-- Favicon-->
    <link rel="shortcut icon" href="{{ URL::asset('images/fav.png') }} ">
    <!-- Author Meta -->
    <meta name="author" content="codepixer">
    <!-- Meta Description -->
    <meta name="description"
        content="<?php echo (!empty($keys_global->description) ? $keys_global->description : "") ?>">
    <!-- Meta Keyword -->
    <meta name="keywords" content="<?php echo (!empty($keys_global->keyword) ? $keys_global->keyword : "") ?>">
    <!-- meta character set -->
    <meta charset="UTF-8">
    <!-- Site Title -->
    @if(isset($product->pd_title))
    <title><?php echo (!empty($product->pd_title) ? $product->pd_title :  "" ) ?> </title>
    @else
    <title><?php echo (!empty($keys_global->title) ? $keys_global->title :  "" ) ?> </title>
    @endif

    <!-- Google analytics -->

    <?php if (!empty($keys_global->google_analytics)): ?>
    <?php echo $keys_global->google_analytics; ?>
    <?php endif; ?>

    <!-- Google analytics -->

    <?php if (!empty($keys_global->facebook_pixel)): ?>
    <?php echo $keys_global->facebook_pixel; ?>
    <?php endif; ?>

    <title>Simple Sidebar - Start Bootstrap Template</title>

    <!-- Bootstrap core CSS -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css"
        integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">

    <!-- Custom styles for this template -->
    <link rel="stylesheet" href="{{ URL::asset('css/style.css')}} ">

    <link rel="stylesheet" href="{{ URL::asset('css/jquery-ui.css')}} ">

    <link rel="stylesheet" type="text/css" href="{{ URL::asset('course_video/css/simple-sidebar.css')}}" />

</head>

<body>



    <div class="d-flex" id="wrapper">

        <!-- Sidebar -->
        <div class="bg-light border-right" id="sidebar-wrapper">
            <div class="sidebar-heading">{{$product->tc_namecourse}}</div>
            <div class="list-group list-group-flush">
                @foreach($sections as $sec)
                <h4 class="list-group-item bg-primary text-white"> {{$sec->sec_title}}</h4>
                @foreach($sec->lecture as $key=>$lec)
                <a class="list-group-item list-group-item-action bg-light checkPermission"
                    Lecture_free="{{ $lec->lecture_free_video }}" Lecture_code="{{ $lec->lecture_video_link }}"
                    id="link_{{ $lec->lecture_video_link }}">
                    {{$lec->lecture_title}}
                </a>
                @endforeach
                @endforeach
            </div>
        </div>
        <!-- /#sidebar-wrapper -->

        <!-- Page Content -->
        <div id="page-content-wrapper">

            <nav class="navbar navbar-expand-lg navbar-light bg-light border-bottom">
                <button class="btn btn-primary" id="menu-toggle">ปิดเมนู</button>
            </nav>

            <div class="container-fluid">
                <div class="row">
                    <div class="col-12  text-center video-container">
                        <div id="made-in-ny"></div>
                        <div id="player"></div>
                    </div>
                </div>
            </div>
        </div>
        <!-- /#page-content-wrapper -->
        <div class="modal fade" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true"
            id="myModal">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLabel">{{ __('Login') }}</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        @include('frontend.auth.form_login')
                    </div>

                </div>
            </div>
        </div>
        <!-- /#wrapper -->
        <style>
        .video-container {
            position: relative;
            padding-bottom: 56.25%;
            padding-top: 30px;
            height: 0;
            overflow: hidden;
        }

        .video-container iframe,
        .video-container object,
        .video-container embed {
            position: absolute;
            top: 0;
            left: 0;
            width: 100%;
            height: 100%;
        }
        </style>
        <!-- Bootstrap core JavaScript -->
        <script src="{{ URL::asset('js/vendor/jquery-2.2.4.min.js') }} "></script>


        <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js"
            integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49" crossorigin="anonymous">
        </script>
        <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js"
            integrity="sha384-ChfqqxuZUCnJSK3+MXmPNIyE6ZbWh2IMqE241rYiqJxyMiZ6OW/JmZQ5stwEULTy" crossorigin="anonymous">
        </script>
        <script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>

        <!-- Menu Toggle Script -->
        <script>
        var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');

        $("#menu-toggle").click(function(e) {
            e.preventDefault();
            $("#wrapper").toggleClass("toggled");
        });

        $(".checkPermission").click(function(e) {
            var Lecture_free = $(this).attr('Lecture_free');
            var Lecture_code = $(this).attr('Lecture_code');
            if (Lecture_free == 1) {
                loadVideoById(Lecture_code)
            } else {
                var authCheck = "{{ Auth::check() }}";
                var courseID = "{{$product->id}}";

                if (authCheck) {
                    if (courseID) {
                        $.ajax({
                            url: "/checkCourseMember",
                            type: 'post',
                            data: {
                                _token: CSRF_TOKEN,
                                data: courseID

                            },
                            success: function(result) {
                                if (result.status == "failed") {
                                    swal({
                                        title: "คุณต้องการดูเนื้อหาคอร์สใช่หรือไม่?",
                                        text: "คุณจำเป็นต้องสั่งซื้อคอร์สก่อนเพื่อเข้าเรียน",
                                        icon: "warning",
                                        buttons: ["ยกเลิก", "สั่งซื้อคอร์สเรียน"],
                                        dangerMode: true,
                                    }).then((willDelete) => {
                                        if (willDelete) {
                                            window.location.href =
                                                "{{ route('product_detail.show', $product->tc_slug_name) }}";
                                        }
                                    });
                                } else if (result.status == "pending") {
                                    swal({
                                        title: "รอการอนุมัติจากแอดมิน",
                                        text: "คุณได้จัดเก็บคอร์สนี้เรียบร้อย และรอการอนุมัติการจ่ายเงินจากแอดมิน",
                                        icon: "warning",
                                        button: "ตกลง",
                                    });
                                } else {
                                    loadVideoById(Lecture_code)

                                }

                            }
                        });

                    }
                } else {
                    swal({
                            title: "คุณต้องการดูเนื้อหาคอร์สใช่หรือไม่?",
                            text: "คุณจำเป็นต้องเข้าสู่ระบบเพื่อเข้าเรียนทุกครั้ง",
                            icon: "warning",
                            buttons: ["ยกเลิก", "เข้าสู่ระบบ"],
                            dangerMode: true,
                        })
                        .then((willDelete) => {
                            if (willDelete) {
                                $('#myModal').modal('toggle')
                            }
                        });

                }
            }
            //console.log(Lecture_code)
        });
        </script>

        @if($product->tc_video_platform == 0)

        <script src="https://player.vimeo.com/api/player.js"></script>
        <script>
        // Vimeo API
        var id_video = "{{ $lecture_find->lecture_video_link}}"
        var options = {
            id: id_video,
            loop: true,
            responsive: true
        };
        var player = new Vimeo.Player('made-in-ny', options);
        $('#link_' + id_video).addClass('active');

        function loadVideoById(videoId) {
            $('#link_' + videoId).addClass('active');

            player.loadVideo(videoId).then(function(id) {

            });
        }
        </script>
        @endif


        @if($product->tc_video_platform == 1)
        <script>
        // Youtube API
        var tag = document.createElement('script');
        var id_video = "{{ $lecture_find->lecture_video_link}}"

        tag.src = "https://www.youtube.com/iframe_api";
        var firstScriptTag = document.getElementsByTagName('script')[0];
        firstScriptTag.parentNode.insertBefore(tag, firstScriptTag);

        // 3. This function creates an <iframe> (and YouTube player)
        //    after the API code downloads.
        var player;
        $('#link_' + id_video).addClass('active');

        function onYouTubeIframeAPIReady() {
            player = new YT.Player('player', {

                height: '100%',
                width: '100%',
                videoId: id_video,
            });
        }

        function loadVideoById(videoId) {
            $('#link_' + videoId).addClass('active');

            console.log(videoId)
            player.loadVideoById({
                'videoId': videoId,
                'suggestedQuality': 'large'
            });
        }
        </script>
        @endif

</body>

</html>