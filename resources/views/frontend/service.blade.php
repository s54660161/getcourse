@extends('frontend.layout.main')



@section('content')

<section id="service-page">
    <div class="bg-top"></div>
    <div class="bg-global">
        <div class="container py-5">
            <div class="row">
                <div class="col-lg-12 col-12 pb-5">
                    <h3 class="color-sky">บริการของเรา</h3>
                </div>

                <div class="col-lg-7 col-12">

                    @if(!empty($service->picture))
                    <img src="{{ URL::asset($service->picture)}}" class="img-fluid pb-5" alt="">
                    @endif
                    @if(!empty($service->description))
                    {!! $service->description !!}
                    @endif
                </div>
                <div class="col-lg-5 col-12">
                    @if(!empty($service->video))
                    <div class="embed-responsive embed-responsive-16by9">
                        <iframe class="embed-responsive-item" src="https://www.youtube.com/embed/{{ $service->video }}"
                            allowfullscreen></iframe>
                    </div>
                    @endif
                </div>
            </div>
        </div>
    </div>
</section>
@endsection