@extends('frontend.layout.main')



@section('content')
<div class="container py-5">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">{{ __('Login') }}</div>

                <div class="card-body">
                    @include('frontend.auth.form_login')
                </div>
            </div>
        </div>
    </div>
</div>
@endsection