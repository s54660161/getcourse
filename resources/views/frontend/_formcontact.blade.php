<form action="{{ route('contactus.store')}}" class="py-4" method="post">
    @csrf
    <div class="form-group">
        <input type="text" class="form-control" placeholder="ชื่อ-นามสกุล" name="name" required>
    </div>
    <div class="form-group">
        <input type="text" class="form-control" placeholder="เบอร์โทรศัพท์" name="tel">
    </div>
    <div class="form-group">
        <input type="email" class="form-control" placeholder="อีเมล" name="email" required>
    </div>
    <div class="form-group">
        <input type="text" class="form-control" placeholder="หัวข้อ" name="title">
    </div>
    <div class="form-group">
        <textarea class="form-control" id="exampleFormControlTextarea1" rows="3" name="description"
            placeholder="รายละเอียด"></textarea>
    </div>
    <div class="form-group">
        <label for="captcha">Captcha</label>
        {!! NoCaptcha::display() !!}
        @if ($errors->has('g-recaptcha-response'))
        <span class="help-block">
            <strong class="text-danger">{{ $errors->first('g-recaptcha-response') }}</strong>
        </span>
        @endif
    </div>
    <button type="submit" class="btn  bg-grey">ยืนยันส่ง</button>
</form>