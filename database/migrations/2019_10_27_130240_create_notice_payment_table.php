<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateNoticePaymentTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('notice_payment', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('bank_name');
            $table->string('order_id');
            $table->dateTime('payment_datetime')->nullable($value = true);
            $table->integer('amount');
            $table->string('picture_slip');
            $table->text('description')->nullable($value = true);
            $table->string('member_name')->nullable($value = true);
            $table->string('email')->nullable($value = true);
            $table->string('tel')->nullable($value = true);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('notice_payment');
    }
}