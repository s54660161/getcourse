<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProductsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('products', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('tc_namecourse');
            $table->text('tc_nameteacher')->nullable($value = true);
            $table->string('tc_slug_name'); 
            $table->text('tc_facebook_group')->nullable($value = true);

            $table->integer('tc_woo_product_id')->nullable($value = true);
            $table->string('tc_landingpage_code')->nullable($value = true);
            
            $table->text('tc_tags')->nullable($value = true);
            $table->text('tc_title')->nullable($value = true);
            $table->text('tc_description')->nullable($value = true);
            $table->integer('tc_free')->comment('0=ไม่ฟรี,1=ฟรี')->nullable($value = true);
            $table->decimal('tc_price',11,2)->nullable($value = true);
            $table->decimal('tc_discount',11,2)->nullable($value = true);
            $table->integer('tc_video_platform')->comment('0=vimoe,1=youtube')->nullable($value = true);
            $table->text('tc_video_preview')->nullable($value = true);
            $table->string('tc_picture');
            $table->text('tc_category')->nullable($value = true);
            $table->integer('tc_subcategory')->nullable($value = true);
            $table->integer('tc_third_category')->nullable($value = true);
            $table->text('tc_rating')->nullable($value = true);

            $table->integer('tc_status')->comment('0=แสดง,1=ไม่แสดง')->nullable($value = true);
            $table->integer('tc_is_buy')->comment('0=สำหรับขาย,1=สินค้าสำหรับโชว์')->nullable($value = true);
            $table->integer('tc_promotion')->comment('0=สินค้าไม่โปรโมชั่น,1=สินค้าโปรโมชั่น')->nullable($value = true);
            $table->integer('tc_recommend')->comment('0=สินค้าไม่แนะนำ,1=สินค้าแนะนำ')->nullable($value = true);
            
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('products');
    }
}