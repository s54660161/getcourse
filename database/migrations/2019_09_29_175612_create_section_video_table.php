<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSectionVideoTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('section_video', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('sec_product_id');
            $table->integer('sec_sequence')->comment('ลำดับการแสดง')->nullable($value = true);
            $table->string('sec_title');
            $table->text('sec_description')->nullable($value = true);
            $table->integer('sec_status')->comment('0=แสดง,1=ไม่แสดง');
            $table->integer('sec_video_platform')->comment('0=vimoe,1=youtube')->nullable($value = true);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('section_video');
    }
}