<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTeacherTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('teacher', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('name');
            
            $table->string('name_en')->nullable($value = true);
            $table->string('nickname')->nullable($value = true);
            
            $table->string('title')->nullable($value = true);
            $table->string('course')->nullable($value = true);
            $table->text('experience')->nullable($value = true);
            $table->text('portfolio')->nullable($value = true);
            $table->text('description')->nullable($value = true);

            $table->text('position')->nullable($value = true);
            $table->string('tel')->nullable($value = true);
            $table->string('facebook_page')->nullable($value = true);
            $table->string('line_id')->nullable($value = true);

            $table->text('picture')->nullable($value = true);
            $table->string('code_video')->nullable($value = true);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('teacher');
    }
}