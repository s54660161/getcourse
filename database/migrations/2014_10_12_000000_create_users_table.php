<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('name');
            $table->string('provider')->nullable($value = true);
            $table->string('provider_id')->nullable($value = true);
            $table->string('email')->unique()->nullable($value = true);
            $table->timestamp('email_verified_at')->nullable($value = true);
            $table->string('password')->nullable($value = true);
            
            $table->text('address')->nullable($value = true);
            $table->integer('tambon')->nullable($value = true);
            $table->integer('amphur')->nullable($value = true);
            $table->integer('province')->nullable($value = true);
            $table->integer('postcode')->nullable($value = true);
            $table->string('tel')->nullable($value = true);
            
            $table->integer('status_buy_course')->comment('0=ไม่ได้ทำการซื้อ,1=ซื้อคอร์ส')->nullable($value = true);
            $table->rememberToken();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}