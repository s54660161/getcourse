<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateWebsettingTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('websetting', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('logo')->nullable($value = true);
            $table->string('favicon')->nullable($value = true);
            $table->string('tabheader_text')->nullable($value = true);
            $table->text('text_footer_left')->nullable($value = true);
            $table->text('text_footer_right')->nullable($value = true);
            $table->string('name_office')->nullable($value = true);
            $table->text('title')->nullable($value = true);
            $table->text('keyword')->nullable($value = true);
            $table->text('description')->nullable($value = true);
            $table->text('address')->nullable($value = true);
            $table->string('tel')->nullable($value = true);
            $table->string('fax')->nullable($value = true);
            $table->string('facebook_page')->nullable($value = true);
            $table->string('ig_page')->nullable($value = true);
            $table->string('line_id')->nullable($value = true);
            $table->string('line_qrcode')->nullable($value = true);
            $table->string('email')->nullable($value = true);
            $table->string('website')->nullable($value = true);
            $table->text('facebook_pixel')->nullable($value = true);
            $table->text('google_analytics')->nullable($value = true);
            $table->text('hitstat')->nullable($value = true);
        
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('websetting');
    }
}