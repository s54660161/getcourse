<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateLectureTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('lecture_video', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('lecture_section_id');
            $table->integer('lecture_sequence')->comment('ลำดับการแสดง')->nullable($value = true);
            $table->string('lecture_title')->nullable($value = true);
            $table->text('lecture_description')->nullable($value = true);
            $table->integer('lecture_video_platform')->comment('0=vimoe,1=youtube')->nullable($value = true);
            $table->text('lecture_video_link')->nullable($value = true);
            $table->integer('lecture_status')->comment('0=แสดง,1=ไม่แสดง')->nullable($value = true);
            $table->integer('lecture_free_video')->comment('0=ไม่ฟรี,1=ฟรี')->nullable($value = true);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('lecture_video');
    }
}