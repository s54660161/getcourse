<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateFilesCourseTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('files_course', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('product_id')->nullable($value = true);
            $table->integer('section_id')->nullable($value = true);
            $table->integer('lecture_id')->nullable($value = true);
            $table->string('title')->nullable($value = true);
            $table->text('description')->nullable($value = true);
            $table->text('file')->nullable($value = true);
            $table->integer('status')->comment('0=notactive,1=active')->default(1)->nullable($value = true);

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('files_course');
    }
}