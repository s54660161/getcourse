<?php

use Illuminate\Database\Seeder;

class WebsettingTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('websetting')->insert([
            'logo' => '/images/logo.jpg',
            'name_office' =>  'Get Course',
            'tabheader_text' =>  'Get Course',
            'text_footer_left' => '',
            'text_footer_right' => '',
            'title' => 'Get Course',
            'keyword' => 'Get Course',
            'description' => 'Get Course',
            'address' => '123/333 กรุงเทพมหานคร',
            'tel' => '00-000-000-0',
            'fax' => '00-000-000-0',
            'facebook_page' => 'facebook.com/GetCourse',
            'ig_page' => 'GetCourse',
            'line_id' => 'GetCourse',
            'line_qrcode' => '',
            'email' => 'GetCourse@email.com',
        ]);
    }
}