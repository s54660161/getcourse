<?php

use Illuminate\Database\Seeder;
use Faker\Factory as Faker;
class ContactTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker::create('App\ContactUs');

        for($i = 1 ; $i <= 10 ; $i++){
            DB::table('contact_us')->insert([
                'name' => $faker->name,
                'tel' => null,
                'email' => $faker->unique()->safeEmail,
                'title' => $faker->title,
                'description' => $faker->paragraph,
            ]);
        }
    }
}
