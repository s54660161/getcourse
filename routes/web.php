<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Auth::routes(['register' => true,'login' => true]);

Route::get('policy', 'frontend\HomeController@policy');

Route::get('/','frontend\HomeController@index');
Route::get('/home','frontend\HomeController@index')->name('home');

Route::get('/contact', 'frontend\ContactController@index');
Route::resource('/contactus', 'MsgcontactController');

Route::get('/register','frontend\AuthController@register')->name('register');
Route::get('/login','frontend\AuthController@login')->name('login');


Route::get('/profile','frontend\MemberController@profile')->name('profile');
Route::post('/editProfile','frontend\MemberController@editProfile')->name('editProfile');
Route::get('/courseme','frontend\MemberController@courseme');
Route::get('/myorder','frontend\MemberController@myorder');

//Route::get('/notice-payment/{order_id}', 'frontend\NoticePaymentController@index');
Route::post('/createNewNotice','frontend\NoticePaymentController@createNewNotice');

Route::post('/checkCourseMember','frontend\CheckCourseController@checkCourseMember');

Route::get('/course','frontend\ProductController@courseAll');


Route::post('/addCourseFree','frontend\AddCourseController@addCourseFree');
Route::post('/buyCourse','frontend\AddCourseController@buyCourse')->name('buyCourse');


//Socail login
Route::get('auth/{provider}', 'Auth\LoginController@redirectToProvider');
Route::get('auth/{provider}/callback', 'Auth\LoginController@handleProviderCallback');

Route::get('/courses/section/{product_id}/{id?}/','frontend\CourseVideoController@seeVideo');

Route::post('/callback/payment','frontend\CallBackApiController@callbackApiPayment');


//Guzzle From Getbiz.co
Route::get('/getcourse','frontend\CallBackApiController@guzzleGetCourses');
Route::get('/getcoursePromotion','frontend\CallBackApiController@guzzleGetCoursesPromotion');
Route::get('/getcourseFree','frontend\CallBackApiController@guzzleGetCoursesFree');
Route::get('/getcourseReccomment','frontend\CallBackApiController@guzzleGetCoursesReccomment');

Route::get('/getteachers','frontend\CallBackApiController@guzzleGetTeachers');

Route::prefix('admin')->name('admin.')->group(function()
{
    /** Admin **/
    Route::get('/', function () {
        if(session('adminlogin')){
            return redirect('admin/products');
        }else{
            return view('admin.login');
        }
    });
    
    Route::get('/blank','admin\AdminController@blank');

    
    
    Route::post('/checklogin', 'admin\AdminController@checkLogin');

    Route::get('/logout', 'admin\AdminController@logout');
    
    Route::middleware(['chkadmin'])->group(function () {   
        
        Route::resource('/setting','admin\WebSettingController');
        Route::get('/contact','admin\ContactController@index');
        
         //Aboutus 
         Route::get('/aboutus','admin\AboutusController@index');
         Route::post('/aboutus/updateabout','admin\AboutusController@updateaboutus');
         
        //Slide and banner   
        Route::get('/slideshow','admin\SlidebannerController@slideshow');
        Route::post('/addslideshow','admin\SlidebannerController@addSlideshow');
        Route::post('/updateslideshow','admin\SlidebannerController@updateSlideshow');
        Route::post('/deleteSlideshow','admin\SlidebannerController@deleteSlideshow');


        //Product
        Route::resource('/products','admin\ProductsController');
        Route::post('/products/destroy','admin\ProductsController@destroy');
        Route::post('/products/deletetags', 'admin\ProductsController@deletetags');
        Route::get('/seeMemberCourse/{id}', 'admin\ProductsController@seeMemberCourse');
        Route::get('/seeMemberCourseAll/{id}', 'admin\ProductsController@seeMemberCourseAll');
        
        //category
        Route::get('/categoryproductall','admin\CategoryProductController@categoryProductAll');
        Route::post('/create-categoryproduct','admin\CategoryProductController@createCategory');
        Route::post('/edit-categoryproduct','admin\CategoryProductController@editCategory');
        Route::post('/delete-categoryproduct','admin\CategoryProductController@deleteCategory');



        Route::resource('/teacher','admin\TeacherController');
        Route::post('/teacher/destroy','admin\TeacherController@destroy');
        Route::post('/teacher/deleteexperience', 'admin\TeacherController@deleteExperience');
        Route::post('/teacher/deleteportfolio', 'admin\TeacherController@deletePortfolio');


        // Section and Lecture Course
        Route::get('/curriculum/{id}','admin\SectionController@curriculum')->name('curriculum');


        Route::resource('/section','admin\SectionController');
        Route::post('/section/destroy','admin\SectionController@destroy');
        Route::post('/section/sortSection','admin\SectionController@sortSection');

        Route::resource('/lecture','admin\LectureController');
        Route::post('/lecture/destroy','admin\LectureController@destroy');
        Route::post('/lecture/sortLecture','admin\LectureController@sortLecture');

        // Bank
        Route::resource('bank', 'admin\BankController');
        Route::post('bank/delete', 'admin\BankController@destroy');
    

        Route::get('/curriculum', function () {
            return view('admin.curriculum.curriculum');
        });

        
        Route::resource('memberall','admin\MemberallController');

        Route::get('orderall', 'admin\OrderController@index');
        Route::post('order/delete', 'admin\OrderController@delete');
        Route::get('confirmpayment/{status}/{id}', 'admin\OrderController@confirmpayment');
        Route::get('showorderlist/{id}', 'admin\OrderController@showorderlist')->name('showorderlist');


    });
});

Route::get('/{slug}', 'frontend\ProductController@detail')->name('product_detail.show');